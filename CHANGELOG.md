## Bleeding Edge

## Version 0.0.0.4

*   Libraries:
    *   `stdlib.asm`:
        *   Detect memory available in the system for `malloc` to use.
    *   `ring_buffer.asm`:
        *   Add a ring\_buffer library, which underlies most FILE\* streams.
    *   `term.asm`:
        *   Add hotkeys ALT-arrows to resize the window
*   Start of multitasking support:
    *   Created structures for `lib/term.asm`, `lib/termios.asm`, and `lib/readkey.asm`
    *   Hook up I/O flows from PS2 driver to `stdio.asm`, `unistd.asm`, and `termios.asm`
    *   Implement notion of file descriptors
    *   Work on scheduler
*   Tools:
    *   Added a static analyser for assembly written in rust: `asmcheck.rs`
    *   Fixes for issues found with the above tool were applied

A video introducing this release is available at: https://www.youtube.com/watch?v=nhFL0zv17xw

## Version 0.0.0.3

*   Bug fixes:
    *   Fix various bugs to do with shell history
*   Debugging support:
    *   Add a tool to generate symbols to use in gdb
    *   Add support for `backtrace`
*   Libraries:
    *   `term.asm`:
        *   Start of a terminal window concept
        *   Allows moving and resizing
        *   Allows scrolling a viewport
        *   Support for some ANSI codes
        *   Some parts refactored from the old `vga` driver
    *   `readkey.asm`:
        *   Library to process ANSI escape codes
*   Drivers:
    *   Refactor `vga` into `vga_text` and new `term` library

A video introducing this release is available at: https://www.youtube.com/watch?v=UBSHITg\_NlA

## Version 0.0.0.2

*   Bug fixes:
    *   `strnrev` now returns NULL on errors
    *   VGA driver now clears characters on backspace (as well as moving cursor back)
    *   Input tests interoperating with termios
*   Drivers:
    *   Programmable Interval Timer (PIT), supporting:
        *   Timer, used for `sleep` (buggy on BOCHS debugger, seems to hang on https://copy.sh/v86)
        *   PC Speaker with some music capabilities
        *   PS2 Keyboard supports sending CSI escape codes on special characters (ie, cursor up)
    *   VGA Text mode ASCII\_BELL, now makes a beep
    *   PS2 and VGA driver refactored out common CSI escapes into include file
*   Libraries:
    *   `unistd.asm`:
        *   Implemented functions `sleep` and `usleep`
    *   `music.asm`:
        *   Ability to play musical notes, scales and arpeggio chords
    *   `termios.asm`:
        *   Deals with canonical and raw mode inputs
        *   Implemented functions `tcgetattr` and `tcsetattr`
    *   `stdlib.asm`:
        *   Basic `malloc` and `free` support using a static heap
    *   `pthread.asm`:
        *   Added spin locks
*   Utilities:
    *   `shell.asm`
        *   `sleep` builtin
        *   `beep` builtin
        *   `history` builtin
        *   Detects cursor up key to load history
        *   Basic support for reading commands from init file at assemble time
    *   `music.asm`
        *   `play` command
        *   `scale` command
        *   `chord` command
*   Task scheduler:
    *   Basic task switching support for multitasking

A video introducing this release is available at: https://www.youtube.com/watch?v=HITwZxI98gY

## Version 0.0.0.1

This release includes the following features:

*   x86 16 bit real mode bootloader
*   `printf` function supporting:
    *   String output: `%s`
    *   Integer outputs: `%d`, `%i`, `%o`, `%u`, `%x`, `%X`
    *   Character output: `%c`
    *   Pointer output: `%p`
    *   Literal % output: `%%`
    *   Support for length modifiers: `hh`, `h`
    *   Support for precision
    *   Support for field width
    *   Support for flags: `#`, `0`, `-`, ` `, `+`
    *   Star format for width and precision: `%*`, `%.*`
    *   Non POSIX binary integer output: `%t`
*   Text mode VGA driver, with support for ANSI escape codes:
    *   Select Graphic Rendition (SGR) modes:
        *   Reset
        *   Bright/Bold
        *   Blink (support varies based on VGA hardware, may just be bright background)
        *   16 colour (3 bit colours)
    *   Cursor movement: relative up/down/forward/back, absolute positioning
    *   Scrolling up/down
    *   Device Status Report
*   Interrupt handler, supporting:
    *   `int 0`: DIVIDE\_BY\_ZERO
    *   `int 3`: DEBUG
    *   `int 8`: DOUBLE\_FAULT
*   PIC8259 support
*   PS/2 Keyboard driver:
    *   Supports locks:
        *   Num lock: Detected, but no effect
        *   Scroll lock: Detected, but no effect
        *   Caps lock: Detected, changes lower case to upper case when reading input
    *   Supports scrolling:
        *   Cursor up/down moves a line at a time
        *   Page up/down moves a screen at a time
        *   No respect for Scroll lock
*   Basic shell
    *   Implements the following commands:
        *   `:` - No Operation
        *   `echo`
        *   `printf`
        *   `help`
    *   Also allows for running test suite with `run_tests` command
*   Test suite covering a decent amount
*   Source code is written in x86 `fasm` assembly
*   Source code for tests is written in C.

A video introducing this release is available at: https://www.youtube.com/watch?v=Hb37FcfdfJc
