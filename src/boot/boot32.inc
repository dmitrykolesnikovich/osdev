	;; 32 bit protected mode
	use32

	extrn  kmain
	extrn  printf
	;      TODO better way to call driver init's
	extrn  vga_init
	extrn  pic8259_init
	extrn  ps2_keyboard_init
	extrn  pit_init
	extrn  memory_init
	extrn  initialise_multitasking
	extrn  backtrace
	public panic
	public halt

	include '../includes/kernel.inc'

	MAGIC = 0x1ABCDEF9
	IDT_SIZE = 32 + 16

boot32:
	;     Now in 32 bit protected mode
	;     Limitations on protected mode is no access to BIOS interrupts
	;     While in protected mode, can write to VGA text buffer
	mov   esp, kernel_stack_top
	xor   ebp, ebp
	ccall memory_init
	;     TODO there is a chance that vga_init will fail if not all image is read
	ccall vga_init

	cmp   dword [magic], MAGIC
	je    .magic_correct
	ccall printf, magic_str, magic, MAGIC, [magic]
	add   esp, 4
	jmp   panic

.magic_correct:
	;     TODO better way to call driver init's
	ccall ps2_keyboard_init
	ccall idt_init
	ccall pit_init
	ccall pic8259_init
	ccall initialise_multitasking
	sti

	ccall debug_functions
	ccall kmain
	ccall printf, kmain_fallout_str
	;     fall through to panic if we return from kmain

panic:
panic32:
	enter 0, 0
	;     TODO should reset scroll on vga?
	pusha
	ccall printf, panic32_str
	;     this printf pulls from the pusha
	ccall printf, dump_regs_fmt_str
	popa
	;     TODO: print out call stack
	;     TODO: print out memory?
	;     fall through to halt

	ccall backtrace

halt:
halt32:
	cli
	hlt
	jmp halt

include 'includes/idt.inc'

panic32_str:
	db "PANIC", 10, 0

kmain_fallout_str:
	db "UNREACHABLE: unexpected return from kmain", 10, 0

magic_str:
	db "ERROR: could not read all source code, magic is at %p, expected %#x, got %#x", 10, 0

dump_regs_fmt_str:
	db "EDI: %#010x, ESI: %#010x, EBP: %#010x, ESP: %#010x", 10
	db "EBX: %#010x, EDX: %#010x, ECX: %#010x, EAX: %#010x", 10
	db 0

	section '.debug_functions_prologue'

debug_functions:
	enter 0, 0
	jmp   @f
.boot32_str db "boot32", 0

@@:
	ccall debug_add_function, boot32, .boot32_str
	jmp   @f
.panic32_str db "panic32", 0

@@:
	ccall debug_add_function, panic32, .panic32_str

	; all code put in debug_functions will run on start, the ret is in the next section

	public debug_functions

	section '.debug_functions_epilogue'
	;       follows on from debug_functions in linker.ld
	leave
	ret

section '.magic'
public  magic

magic:
	dd MAGIC
