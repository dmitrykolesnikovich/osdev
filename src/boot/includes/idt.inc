include '../../includes/debug_int_handler.inc'

	; FIXME, currently we are in the bootloader, which I'm not sure is the right place for this

	;      TODO this is only used by irq's
	public idt_add_trap_entry

	public int_add_handler

	NUM_INTS = 32

	;        FIXME, as this is included in boot32.inc, printf is defined there
	;        it would be preferable to use an include as have this as a seperate unit?
	;include "../../lib/includes/stdio.inc"
	extrn    puts

	; Interrupt Descriptor Table

	;; Handlers

	; based off "http://www.osdever.net/bkerndev/Docs/isrs.htm"

	;; void int_handler(struct regs *r)

	;           These are all pushed by .int_common_handler and the interrupt itself
	struc       regs {
	.gs         dd ?
	.fs         dd ?
	.es         dd ?
	.ds         dd ?
	.edi        dd ?
	.esi        dd ?
	.ebp        dd ?
	.esp        dd ?
	.ebx        dd ?
	.edx        dd ?
	.ecx        dd ?
	.eax        dd ?
	.int_no  dd ?
	.err_code dd ?
	.frame_ebp dd ?
	.eip        dd ?
	.cs         dd ?
	.eflags     dd ?
	.useresp    dd ?
	.ss         dd ?
	}
	virtual     at eax
	regs        regs
	sizeof.regs = $ - regs
	end         virtual

	debug_int_handler int_handler
	;; void int_handler(struct regs *r)
	r  equ arg1

int_handler:
	enter 0, 0

	mov eax, r
	cmp [regs.int_no], 0
	jl  .error
	cmp [regs.int_no], NUM_INTS
	jge .error

	mov edx, [regs.int_no]
	mov ecx, [int_handlers + 4*edx]
	cmp ecx, NULL
	je  .unhandled

	ccall ecx, eax, [regs.err_code]

.return:
	leave
	ret

.error:
	ccall puts, .error_str
	;     fall through to unhandled

.unhandled:
	mov eax, r
	xor ecx, ecx
	cmp [regs.int_no], 0
	jl  .print
	cmp [regs.int_no], NUM_INTS
	jge .print
	mov edx, [regs.int_no]
	mov ecx, [int_handler_strs + 4*edx]

.print:
	ccall printf, .unhandled_str, [regs.int_no], ecx, [regs.err_code], [regs.err_code], [regs.err_code]
	;     TODO dump regs?
	ccall panic
	;     UNREACHABLE
	jmp   .return

.error_str:
	db "ERROR: Interrupt number out of bounds", 13, 10, 0

.unhandled_str:
	db "ERROR: Unhandled Interrupt %d (%s), code %d/0x%x/0b%t", 13, 10, 0

int_handler_strs:
	dd .int0_str
	dd .int1_str
	dd .int2_str
	dd .int3_str
	dd .int4_str
	dd .int5_str
	dd .int6_str
	dd .int7_str
	dd .int8_str
	dd .int9_str
	dd .int10_str
	dd .int11_str
	dd .int12_str
	dd .int13_str
	dd .int14_str
	dd .int15_str
	dd .int16_str
	dd .int17_str
	dd .int18_str
	dd .int19_str
	dd .int20_str
	dd .int21_str
	dd .int22_str
	dd .int23_str
	dd .int24_str
	dd .int25_str
	dd .int26_str
	dd .int27_str
	dd .int28_str
	dd .int29_str
	dd .int30_str
	dd .int31_str

.int0_str:
	db "Divide-by-zero Error", 0

.int1_str:
	db "Debug", 0

.int2_str:
	db "Non-maskable Interrupt", 0

.int3_str:
	db "Breakpoint", 0

.int4_str:
	db "Overflow", 0

.int5_str:
	db "Bound Range Exceeded", 0

.int6_str:
	db "Invalid Opcode", 0

.int7_str:
	db "Device Not Available", 0

.int8_str:
	db "Double Fault", 0

.int9_str:
	db "Coprocessor Segment Overrun", 0

.int10_str:
	db "Invalid TSS", 0

.int11_str:
	db "Segment Not Present", 0

.int12_str:
	db "Stack-Segment Fault", 0

.int13_str:
	db "General Protection Fault", 0

.int14_str:
	db "Page Fault", 0

.int15_str:
	db "Reserved", 0

.int16_str:
	db "x87 Floating-Point Exception", 0

.int17_str:
	db "Alignment Check", 0

.int18_str:
	db "Machine Check", 0

.int19_str:
	db "SIMD Floating-Point Exception", 0

.int20_str:
	db "Virtualization Exception", 0

.int21_str:
	db "Control Protection Exception", 0

.int22_str:
	db "Reserved", 0

.int23_str:
	db "Reserved", 0

.int24_str:
	db "Reserved", 0

.int25_str:
	db "Reserved", 0

.int26_str:
	db "Reserved", 0

.int27_str:
	db "Reserved", 0

.int28_str:
	db "Hypervidos Injection Exception", 0

.int29_str:
	db "VMM Communication Exception", 0

.int30_str:
	db "Security Exception", 0

.int31_str:
	db "Reserved", 0

	;; int int_add_handler(int num, void(*func)(int))

	num  equ arg1
	func equ arg2

int_add_handler:
	enter 0, 0

	mov eax, num
	cmp eax, 0
	jl  .error
	cmp eax, NUM_INTS
	jge .error

	mov ecx, func
	mov [int_handlers + 4*eax], ecx
	jmp .return

.error:
	mov eax, -1

.return:
	leave
	ret

int_handlers:
	times 32 dd 0

	debug_int_handler int_common_handler

int_common_handler:
	pusha
	push ds
	push es
	push fs
	push gs
	mov  ax, DATA_SEG
	mov  ds, ax
	mov  es, ax
	mov  fs, ax
	mov  gs, ax
	mov  eax, esp
	push eax
	mov  eax, int_handler
	call eax
	pop  eax
	pop  gs
	pop  fs
	pop  es
	pop  ds
	popa
	leave
	sti
	iret

int0_handler:
	cli
	enter 0, 0
	push  0
	push  0
	jmp   int_common_handler

int1_handler:
	cli
	enter 0, 0
	push  0
	push  1
	jmp   int_common_handler

int2_handler:
	cli
	enter 0, 0
	push  0
	push  2
	jmp   int_common_handler

int3_handler:
	;    shortcircuit the common handler for debugging ease
	xchg bx, bx; trap for bochs
	iret

	cli
	enter 0, 0
	push  0
	push  3
	jmp   int_common_handler

int4_handler:
	cli
	enter 0, 0
	push  0
	push  4
	jmp   int_common_handler

int5_handler:
	cli
	enter 0, 0
	push  0
	push  5
	jmp   int_common_handler

int6_handler:
	cli
	enter 0, 0
	push  0
	push  6
	jmp   int_common_handler

int7_handler:
	cli
	enter 0, 0
	push  0
	push  7
	jmp   int_common_handler

int8_handler:
	cli
	enter 0, 0
	;     this interrupt pushed an error code for us
	push  8
	jmp   int_common_handler

int9_handler:
	cli
	enter 0, 0
	push  0
	push  9
	jmp   int_common_handler

int10_handler:
	cli
	enter 0, 0
	;     this interrupt pushed an error code for us
	push  10
	jmp   int_common_handler

int11_handler:
	cli
	enter 0, 0
	;     this interrupt pushed an error code for us
	push  11
	jmp   int_common_handler

int12_handler:
	cli
	enter 0, 0
	;     this interrupt pushed an error code for us
	push  12
	jmp   int_common_handler

int13_handler:
	cli
	enter 0, 0
	;     this interrupt pushed an error code for us
	push  13
	jmp   int_common_handler

int14_handler:
	cli
	enter 0, 0
	;     this interrupt pushed an error code for us
	push  14
	jmp   int_common_handler

int15_handler:
	cli
	enter 0, 0
	push  0
	push  15
	jmp   int_common_handler

int16_handler:
	cli
	enter 0, 0
	push  0
	push  16
	jmp   int_common_handler

int17_handler:
	cli
	enter 0, 0
	;     this interrupt pushed an error code for us
	push  17
	jmp   int_common_handler

int18_handler:
	cli
	enter 0, 0
	push  0
	push  18
	jmp   int_common_handler

int19_handler:
	cli
	enter 0, 0
	push  0
	push  19
	jmp   int_common_handler

int20_handler:
	cli
	enter 0, 0
	push  0
	push  20
	jmp   int_common_handler

int21_handler:
	cli
	enter 0, 0
	;     this interrupt pushed an error code for us
	push  21
	jmp   int_common_handler

int22_handler:
	cli
	enter 0, 0
	push  0
	push  22
	jmp   int_common_handler

int23_handler:
	cli
	enter 0, 0
	push  0
	push  23
	jmp   int_common_handler

int24_handler:
	cli
	enter 0, 0
	push  0
	push  24
	jmp   int_common_handler

int25_handler:
	cli
	enter 0, 0
	push  0
	push  25
	jmp   int_common_handler

int26_handler:
	cli
	enter 0, 0
	push  0
	push  26
	jmp   int_common_handler

int27_handler:
	cli
	enter 0, 0
	push  0
	push  27
	jmp   int_common_handler

int28_handler:
	cli
	enter 0, 0
	push  0
	push  28
	jmp   int_common_handler

int29_handler:
	cli
	enter 0, 0
	;     this interrupt pushed an error code for us
	push  29
	jmp   int_common_handler

int30_handler:
	cli
	enter 0, 0
	;     this interrupt pushed an error code for us
	push  30
	jmp   int_common_handler

int31_handler:
	cli
	enter 0, 0
	push  0
	push  31
	jmp   int_common_handler

	;; void div_by_0(struct regs *r)
	r  equ arg1

div_by_0:
	mov eax, r

	;;    TODO we should be able to call backtrace, and it should know we are in a int_handler
	mov   ecx, [regs.eip]
	extrn debug_get_symbol
	ccall debug_get_symbol, ecx
	mov   ecx, [regs.eip]

	ccall printf, div_by_0_str, ecx, eax
	jmp   panic

	;; void breakpoint(struct regs *r)
	r  equ arg1

breakpoint:
	xchg bx, bx; trap for bochs
	ret

	;; void double_fault(struct regs *r)
	r  equ arg1

double_fault:
	ccall printf, double_fault_str
	jmp   panic

	;; Initialize IDT

idt_init:
	lidt [idt_pointer]

	ccall idt_add_trap_entry, 0, int0_handler
	ccall idt_add_trap_entry, 1, int1_handler
	ccall idt_add_trap_entry, 2, int2_handler
	ccall idt_add_trap_entry, 3, int3_handler
	ccall idt_add_trap_entry, 4, int4_handler
	ccall idt_add_trap_entry, 5, int5_handler
	ccall idt_add_trap_entry, 6, int6_handler
	ccall idt_add_trap_entry, 7, int7_handler
	ccall idt_add_trap_entry, 8, int8_handler
	ccall idt_add_trap_entry, 9, int9_handler
	ccall idt_add_trap_entry, 10, int10_handler
	ccall idt_add_trap_entry, 11, int11_handler
	ccall idt_add_trap_entry, 12, int12_handler
	ccall idt_add_trap_entry, 13, int13_handler
	ccall idt_add_trap_entry, 14, int14_handler
	ccall idt_add_trap_entry, 15, int15_handler
	ccall idt_add_trap_entry, 16, int16_handler
	ccall idt_add_trap_entry, 17, int17_handler
	ccall idt_add_trap_entry, 18, int18_handler
	ccall idt_add_trap_entry, 19, int19_handler
	ccall idt_add_trap_entry, 20, int20_handler
	ccall idt_add_trap_entry, 21, int21_handler
	ccall idt_add_trap_entry, 22, int22_handler
	ccall idt_add_trap_entry, 23, int23_handler
	ccall idt_add_trap_entry, 24, int24_handler
	ccall idt_add_trap_entry, 25, int25_handler
	ccall idt_add_trap_entry, 26, int26_handler
	ccall idt_add_trap_entry, 27, int27_handler
	ccall idt_add_trap_entry, 28, int28_handler
	ccall idt_add_trap_entry, 29, int29_handler
	ccall idt_add_trap_entry, 30, int30_handler
	ccall idt_add_trap_entry, 31, int31_handler

	ccall int_add_handler, 0, div_by_0
	ccall int_add_handler, 3, breakpoint
	ccall int_add_handler, 8, double_fault

	ret

irq     equ arg1
handler equ arg2

idt_add_trap_entry:
	enter 0, 0
	mov   ecx, irq
	shl   ecx, 3; mul 8
	add   ecx, idt_start
	cmp   ecx, idt_end
	jge   .error
	cmp   ecx, idt_start
	jl    .error
	mov   eax, handler
	mov   [ecx], ax; address lower 16 bits
	mov   word [ecx + 2], CODE_SEG; code segment
	mov   byte [ecx + 4], 0; reserved
	mov   byte [ecx + 5], 0x8E; 32bit trap gate, and present flag
	shr   eax, 16
	mov   word [ecx + 6], ax; address upper 16 bits
	leave
	ret

.error:
	mov   eax, irq
	ccall printf, idt_oob_str, eax
	jmp   panic

idt_start:
	times IDT_SIZE dq 0

idt_end:
idt_pointer:
	dw idt_end - idt_start
	dd idt_start

div_by_0_str:
	db 10, "EXCEPTION: Division By 0 - %p:%s", 10, 0

double_fault_str:
	db 10, "EXCEPTION: Double Fault", 10, 0

idt_oob_str:
	db "ERROR: idt_add_trap_entry called with out of bounds index %d", 10, 0
