	include 'vga_text_constants.inc'
	;;      void vga_putchar_at(char c, struct font_attr attr, int x, int y)
	extrn   vga_putchar_at
	;;      void vga_scroll_view_down(int x, int y, int width, int height)
	extrn   vga_scroll_view_down
	;;      void vga_scroll_view_up(int x, int y, int width, int height)
	extrn   vga_scroll_view_up
	;;      void vga_set_cursor(int x, int y)
	extrn   vga_set_cursor
	;;      void vga_enable_cursor()
	extrn   vga_enable_cursor
	;;      void vga_disable_cursor()
	extrn   vga_disable_cursor
