	format  elf
	include "../includes/debug_symbols.inc"
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  pit_init
	public  pit_tick_handler
	public  pit_ms
	public  pit_fractions
	public  pit_play_sound
	public  pit_no_sound

	include '../includes/kernel.inc'
	include '../lib/includes/stdio.inc'

	PIT_PORT_CHANNEL_0 = 0x40
	PIT_PORT_CHANNEL_1 = 0x41
	PIT_PORT_CHANNEL_2 = 0x42
	PIT_PORT_MODE      = 0x43
	PIT_PORT_SPEAKER   = 0x61

	; Channel select, `or` with others to send to PIT_PORT_MODE
	PIT_CHANNEL_0        = 00000000b
	PIT_CHANNEL_1        = 01000000b
	PIT_CHANNEL_2        = 10000000b
	PIT_CHANNEL_READBACK = 11000000b

	; Access modes, `or` with others to send to PIT_PORT_MODE
	PIT_ACCESS_MODE_LATCH       = 00000000b
	PIT_ACCESS_MODE_LO_BYTE     = 00010000b
	PIT_ACCESS_MODE_HIGH_BYTE   = 00100000b
	PIT_ACCESS_MODE_LOHIGH_BYTE = 00110000b

	; Operating modes, `or` with others to send to PIT_PORT_MODE
	PIT_OPERATING_MODE_0 = 00000000b
	PIT_OPERATING_MODE_1 = 00000010b
	PIT_OPERATING_MODE_2 = 00000100b
	PIT_OPERATING_MODE_3 = 00000110b
	PIT_OPERATING_MODE_4 = 00001000b
	PIT_OPERATING_MODE_5 = 00001010b
	; Mode 6 is same as mode 2
	; Mode 7 is same as mode 3

	PIT_OPERATING_MODE_INTERRUPT = PIT_OPERATING_MODE_0
	PIT_OPERATING_MODE_HARDWARE_ONE_SHOT = PIT_OPERATING_MODE_1
	PIT_OPERATING_MODE_RATE = PIT_OPERATING_MODE_2
	PIT_OPERATING_MODE_SQUARE_WAVE = PIT_OPERATING_MODE_3
	PIT_OPERATING_MODE_SOFT_STROBE = PIT_OPERATING_MODE_4
	PIT_OPERATING_MODE_HARD_STROBE = PIT_OPERATING_MODE_5

	; BCD/Binary mode, `or` with others to send to PIT_PORT_MODE
	PIT_MODE_BINARY = 00000000b
	PIT_MODE_BCD    = 00000001b

	; 2 (0x0010) is the fastest, 65536 (0x10000) is the slowest, 1 is not meant to be used
	; 4 (0x0100) seems to work well for me. any faster and printing speed is impacted
	; 8 (0x1000) seems to works on copy.sh/v86, any faster and it hangs
	PIT_RELOAD = 0x0100
	PIT_RELOAD = 0x1000

	; frequency = 1193182 / reload, but 1193182 is actually more accurately 3579545 / 3
	; this magic value is 3000 * (2^42) / 3579545 (expanded from 1 / (3579545 / 3) * 1000)
	; TODO FIXME: the quotes are round here due to bug in asmfmt
	; See "https://wiki.osdev.org/PIT"
	PIT_FREQ_MAGIC = 0xDBB3A062

pit_init:
	enter 0, 0
	push  ebx

	mov  ebx, PIT_RELOAD
	mov  eax, PIT_FREQ_MAGIC
	mul  ebx
	shrd eax, edx, 10
	shr  edx, 10

	mov [pit_tick_ms], edx
	mov [pit_tick_fractions], eax

	pushfd
	cli

	mov al, PIT_CHANNEL_0 or PIT_ACCESS_MODE_LOHIGH_BYTE or PIT_OPERATING_MODE_RATE
	out PIT_PORT_MODE, al

	mov ax, PIT_RELOAD
	out PIT_PORT_CHANNEL_0, al
	mov al, ah
	out PIT_PORT_CHANNEL_0, al

	popfd

.return:
	pop ebx
	leave
	ret

pit_tick_handler:
	enter 0, 0
	;     todo, some concurrency locks needed here if also reading from sleep?

	mov  eax, [pit_ms]
	test eax, eax
	jz   .nosched

	extrn  schedule
	;ccall schedule

.nosched:

	mov eax, [pit_tick_fractions]
	mov ecx, [pit_tick_ms]
	add [pit_fractions], eax
	adc [pit_ms], ecx
	adc dword [pit_ms+WORD_SIZE], 0
	jo  .overflow
	leave
	ret

.overflow:
	ccall printf, .overflow_str
	extrn panic
	ccall panic
	;     unreachable
	ret

.overflow_str:
	db "ERROR: Overflow in timer_tick_handler", 13, 10

	;;        void pit_play_sound(uint32_t frequency)
	frequency equ arg1

pit_play_sound:
	enter 0, 0
	push  ebx
	xor   edx, edx
	pushfd
	cli

	mov  eax, 1193180
	mov  ecx, frequency
	test ecx, ecx
	je   .return
	div  ecx
	mov  ebx, eax

	in   al, PIT_PORT_SPEAKER
	test al, 3
	jnz  .output
	;    turn on speaker
	or   al, 0x3
	out  PIT_PORT_SPEAKER, al

	mov al, PIT_CHANNEL_2 or PIT_ACCESS_MODE_LOHIGH_BYTE or PIT_OPERATING_MODE_SQUARE_WAVE
	out PIT_PORT_MODE, al

.output:
	mov al, bl
	out PIT_PORT_CHANNEL_2, al
	mov al, bh
	out PIT_PORT_CHANNEL_2, al

.return:
	popfd
	pop ebx
	leave
	ret

pit_no_sound:
	enter 0, 0
	in    al, PIT_PORT_SPEAKER
	and   al, 0xFC
	out   PIT_PORT_SPEAKER, al

.return:
	leave
	ret

pit_tick_ms:
	dd 0

pit_tick_fractions:
	dd 0

pit_ms:
	;  make this 64 bit
	dd 0
	dd 0

pit_fractions:
	dd 0
