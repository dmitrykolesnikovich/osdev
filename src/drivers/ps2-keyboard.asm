	format  elf
	include "../includes/debug_symbols.inc"
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  ps2_keyboard_init
	public  ps2_keyboard_handler
	public  ps2_keyboard_getc

	include '../lib/includes/stdio.inc'
	include '../lib/includes/string.inc'
	include '../lib/includes/termios.inc'
	include '../lib/includes/unistd.inc'
	include '../lib/includes/stdlib.inc'
	include '../lib/includes/readkey.inc'
	include '../lib/includes/ring_buffer.inc'
	include '../includes/ascii.inc'
	include '../includes/csi.inc'
	include '../includes/kernel.inc'

	;     todo should this go in a include somewhere
	extrn panic

	PS2_KEYBOARD_PORT = 0x60
	PS2_STATUS_PORT = 0x64
	PS2_RESEND_LIMIT = 3

	PS2_STATUS_OUTPUT = 0x1
	PS2_STATUS_INPUT = 0x2

	PS2_COMMAND_SET_LEDS = 0xED
	PS2_RESEND = 0xFE
	PS2_ACK = 0xFA

	PS2_BUF_CAP = 4095

	MODIFIERS_LEFT_GUI = MODIFIERS_GUI shl 4
	MODIFIERS_LEFT_CTRL = MODIFIERS_CTRL shl 4
	MODIFIERS_LEFT_ALT = MODIFIERS_ALT shl 4
	MODIFIERS_LEFT_SHIFT = MODIFIERS_SHIFT
	MODIFIERS_RIGHT_GUI = MODIFIERS_GUI
	MODIFIERS_RIGHT_CTRL = MODIFIERS_CTRL
	MODIFIERS_RIGHT_ALT = MODIFIERS_ALT
	MODIFIERS_RIGHT_SHIFT = MODIFIERS_SHIFT

	LOCKS_CAPS_LOCK = 0x4
	LOCKS_NUM_LOCK = 0x2
	LOCKS_SCROLL_LOCK = 0x1

ps2_keyboard_init:
	enter 0, 0

	; TODO
	; disable numlock
	; check current scancode set (panic if not what we support?)

	ccall ring_buffer_new
	mov   [scan_codes], eax

	leave
	ret

ps2_keyboard_handler:
	enter 0, 0

	xor   eax, eax
	in    al, PS2_KEYBOARD_PORT
	mov   ecx, [scan_codes]
	ccall ring_buffer_putc, eax, ecx

	;     TODO notify the right task
	;     this may cause delays in new codes coming in?
	extrn schedule
	ccall schedule

	leave
	ret

	;;   void ps2_handle_scan_code(int code)
	code equ arg1

ps2_handle_scan_code:
	enter 0, 0
	push  ebx

	mov eax, code
	mov ebx, eax
	;   Response codes, ignore
	;   TODO we could prob ignore these if we disable keyboard scans when doing commands
	cmp bl, PS2_ACK
	je  .return
	cmp bl, PS2_RESEND
	je  .return

	;    Extended scan code
	cmp  bl, SCAN_CODE_EXTENDED
	je   .extended_mode
	;    TODO, update current pressed (both extended and not)
	;    TODO make this a function
	test bl, 0x80
	je   .pressed
	;    todo, released
	jmp  .consume_code

.pressed:
	;   todo, pressed
	jmp .consume_code

.consume_code:
	;     Update modifiers and locks
	ccall ps2_keyboard_update_modifiers, ebx
	cmp   eax, 1
	je    .normal_mode
	ccall ps2_keyboard_update_locks, ebx
	cmp   eax, 1
	je    .normal_mode

	ccall ps2_keyboard_update_stdin, ebx
	cmp   eax, 1
	je    .normal_mode

	ccall ps2_keyboard_check_scroll, ebx
	cmp   eax, 1
	je    .normal_mode

	; TODO more stuff, F keys
	; some odd behaviour, shift F1-3 give a code, and shift F12
	; F1-11 without shift don't give a code, and shift F3-11 don't either

	test bl, 0x80
	je   .print_pressed
	mov  al, 0x7F
	and  bl, al
	xor  eax, eax
	mov  al, [mode]
	;    ccall printf, released_str, ebx, eax
	jmp  .normal_mode

.print_pressed:
	xor eax, eax
	mov al, [mode]
	;   ccall printf, unknown_str, ebx, eax
	jmp .normal_mode

.extended_mode:
	mov byte [mode], SCAN_CODE_EXTENDED
	jmp .return

.normal_mode:
	mov byte [mode], 0

.return:
	pop ebx
	leave
	ret

	; TODO remove once implemented

unknown_str:
	db "[Unknown Scan code %#x, mode %#x]", 10, 0

released_str:
	db "[Released %#x, mode %#x]", 10, 0

scan_codes:
	dd 0

	;;  int ps2_keyboard_getc(struct ring_buffer *buf)
	buf equ arg1

ps2_keyboard_getc:
	enter 0, 0

	; See if buf has any chars, if so return that
	; If not, then loop
	; check if any new scan codes have come in, and process them
	; after each scan code, check if buf has any chars
	; if no scan codes (and nothing in buf), then block task

.start:
	mov   eax, buf
	ccall ring_buffer_getc, eax
	cmp   eax, -1
	jne   .return

.get_scan_codes:
	mov   eax, [scan_codes]
	ccall ring_buffer_getc, eax
	cmp   eax, -1
	je    .return

	ccall ps2_handle_scan_code, eax
	jmp   .start

.return:
	leave
	ret

	;    bool ps2_keyboard_update_locks(int code)
	code equ arg1

ps2_keyboard_update_locks:
	enter 0, 0
	push  ebx
	mov   cl, 0
	mov   ebx, code
	test  bl, 0x80
	jz    .not_released
	mov   cl, 0x7F
	and   bl, cl
	mov   cl, 1

.not_released:
	cmp bl, SCAN_CODE_CAPS_LOCK
	je  .caps_lock
	cmp bl, SCAN_CODE_NUM_LOCK
	je  .num_lock
	cmp bl, SCAN_CODE_SCROLL_LOCK
	je  .scroll_lock
	jmp .unused

.caps_lock:
	mov al, LOCKS_CAPS_LOCK
	jmp .set

.num_lock:
	mov al, LOCKS_NUM_LOCK
	jmp .set

.scroll_lock:
	mov al, LOCKS_SCROLL_LOCK
	jmp .set

.set:
	cmp cl, 1
	je  .ignore; Ignore if released, but still "consume" the code
	mov bl, [locks]
	xor al, bl; toggle the bit
	mov [locks], al

	xor eax, eax
	mov al, [locks]

	;     update the lights
	ccall ps2_keyboard_send_with_resend, PS2_COMMAND_SET_LEDS
	cmp   al, PS2_ACK
	jne   .ignore; Silently ignore error and return
	xor   eax, eax
	mov   al, [locks]
	ccall ps2_keyboard_send_with_resend, eax
	cmp   al, PS2_ACK
	jne   .ignore; Silently ignore error and return

.ignore:
	mov eax, 1
	jmp .return

.unused:
	mov eax, 0

.return:
	pop ebx
	leave
	ret

	;    void ps2_keyboard_update_modifiers(int code)
	code equ arg1

ps2_keyboard_update_modifiers:
	enter 0, 0
	push  ebx
	mov   cl, 0xFF
	mov   ebx, code
	test  bl, 0x80
	jz    .not_released
	mov   cl, 0x7F
	and   bl, cl
	xor   cl, cl

.not_released:
	cmp byte [mode], SCAN_CODE_EXTENDED
	je  .extended
	cmp bl, SCAN_CODE_LEFT_CONTROL
	je  .left_ctrl
	cmp bl, SCAN_CODE_LEFT_ALT
	je  .left_alt
	cmp bl, SCAN_CODE_LEFT_SHIFT
	je  .left_shift
	cmp bl, SCAN_CODE_RIGHT_SHIFT
	je  .right_shift
	jmp .unused

.extended:
	cmp bl, SCAN_CODE_RIGHT_CONTROL
	je  .right_ctrl
	cmp bl, SCAN_CODE_RIGHT_ALT
	je  .right_alt
	cmp bl, SCAN_CODE_LEFT_GUI
	je  .left_gui
	cmp bl, SCAN_CODE_RIGHT_GUI
	je  .right_gui
	jmp .unused

.left_gui:
	mov al, MODIFIERS_LEFT_GUI
	jmp .set

.left_ctrl:
	mov al, MODIFIERS_LEFT_CTRL
	jmp .set

.left_alt:
	mov al, MODIFIERS_LEFT_ALT
	jmp .set

.left_shift:
	mov al, MODIFIERS_LEFT_SHIFT
	jmp .set

.right_gui:
	mov al, MODIFIERS_RIGHT_GUI
	jmp .set

.right_ctrl:
	mov al, MODIFIERS_RIGHT_CTRL
	jmp .set

.right_alt:
	mov al, MODIFIERS_RIGHT_ALT
	jmp .set

.right_shift:
	mov al, MODIFIERS_RIGHT_SHIFT
	jmp .set

.set:
	mov bl, [modifiers]
	not al
	and bl, al; clear the bit
	not al
	and al, cl; see if released
	or  al, bl; set the bit again
	mov [modifiers], al
	mov eax, 1
	jmp .return

.unused:
	mov eax, 0

.return:
	pop ebx
	leave
	ret

	;    bool ps2_keyboard_update_stdin(int code)
	code equ arg1

ps2_keyboard_update_stdin:
	enter 0, 0
	push  ebx
	push  esi

	mov  bl, 0
	mov  eax, code
	;    TODO, should only really do this if ISTRIP is set on c_iflag in termios?
	test al, 0x80
	jz   .get_ascii
	mov  bl, 0x7F
	and  al, bl
	mov  bl, 1

.get_ascii:

	mov   esi, eax
	ccall ps2_keyboard_get_ascii_from_code, esi
	test  eax, eax
	jnz   .is_ascii

	ccall ps2_keyboard_get_csi_from_code, esi
	test  eax, eax
	jnz   .is_csi_sequence

	jmp .unused

.is_csi_sequence:
	cmp bl, 1
	je  .ignore; Ignore if released, but still "consume" the code

	mov ebx, eax

	;   output \e [ csi
	;   if modifiers are present, then send \e [ 27; modr+1; modl+1; csi~
	xor eax, eax
	mov al, [modifiers]
	shl eax, 4
	shr al, 4
	or  al, ah

	test al, al
	jz   .send_csi

	;     better suited with fprintf
	;     Need to send two chars at once, otherwise readkey will think just esc was pressed
	ccall fputs, .csi_start_str, [stdin]
	ccall fputc, '2', [stdin]; 27 is dec for \e
	ccall fputc, '7', [stdin]
	ccall fputc, CSI_ARG_DELIM, [stdin]
	xor   ecx, ecx
	mov   cl, [modifiers]
	shl   ecx, 4
	shr   cl, 4
	xor   eax, eax
	mov   al, cl
	inc   al
	add   al, '0'
	push  ecx
	ccall fputc, eax, [stdin]
	ccall fputc, CSI_ARG_DELIM, [stdin]
	pop   ecx
	xor   eax, eax
	mov   al, ch
	inc   al
	add   al, '0'
	ccall fputc, eax, [stdin]
	ccall fputc, CSI_ARG_DELIM, [stdin]
	ccall itoa, ebx, .itoa_buf, 10
	ccall fputs, .itoa_buf, [stdin]
	ccall fputc, CSI_CUSTOM, [stdin]

	jmp .ignore

.send_csi:
	;     Need to send two chars at once, otherwise readkey will think just esc was pressed
	ccall fputs, .csi_start_str, [stdin]

	; TODO modifiers, for now ignore

	ccall fputc, ebx, [stdin]

	;   go to ignore, which just exits saying we consumed this code
	jmp .ignore

.is_ascii:
	cmp bl, 1
	je  .ignore; Ignore if released, but still "consume" the code

	mov ebx, eax

	xor eax, eax
	mov al, [modifiers]
	shl eax, 4
	shr al, 4
	or  al, ah

	test al, al
	jz   .update_stdin
	cmp  al, MODIFIERS_SHIFT
	je   .only_shift

	; send \e[<key>; <modr+1>; <modl+1>~
	; note, this won't take into account locks (like CAPS)

	;     better suited with fprintf
	;     Need to send two chars at once, otherwise readkey will think just esc was pressed
	ccall fputs, .csi_start_str, [stdin]
	ccall itoa, ebx, .itoa_buf, 10
	ccall fputs, .itoa_buf, [stdin]
	ccall fputc, CSI_ARG_DELIM, [stdin]
	xor   ecx, ecx
	mov   cl, [modifiers]
	shl   ecx, 4
	shr   cl, 4
	xor   eax, eax
	mov   al, cl
	inc   al
	add   al, '0'
	push  ecx
	ccall fputc, eax, [stdin]
	ccall fputc, CSI_ARG_DELIM, [stdin]
	pop   ecx
	xor   eax, eax
	mov   al, ch
	inc   al
	add   al, '0'
	ccall fputc, eax, [stdin]
	ccall fputc, CSI_CUSTOM, [stdin]

	jmp .ignore

.itoa_buf:
	times 33 db 0; Enough for 32 bit platforms

.csi_start_str:
	db ASCII_ESCAPE, CSI_DELIM_CHAR, 0

.only_shift:

	cmp bl, 'a'
	jl  .shift_non_letter
	cmp bl, 'z'
	jg  .shift_non_letter

	sub bl, 32; difference between A and a

	jmp .check_caps

.shift_non_letter:

	; if not letters, then use go to mapping

	; todo, what to do when shift and numpad ascii?

	mov   edx, '~'
	cmp   bl, '`'
	cmove ebx, edx
	je    .check_caps
	mov   edx, '!'
	cmp   bl, '1'
	cmove ebx, edx
	je    .check_caps
	mov   edx, '@'
	cmp   bl, '2'
	cmove ebx, edx
	je    .check_caps
	mov   edx, '#'
	cmp   bl, '3'
	cmove ebx, edx
	je    .check_caps
	mov   edx, '$'
	cmp   bl, '4'
	cmove ebx, edx
	je    .check_caps
	mov   edx, '%'
	cmp   bl, '5'
	cmove ebx, edx
	je    .check_caps
	mov   edx, '^'
	cmp   bl, '6'
	cmove ebx, edx
	je    .check_caps
	mov   edx, '&'
	cmp   bl, '7'
	cmove ebx, edx
	je    .check_caps
	mov   edx, '*'
	cmp   bl, '8'
	cmove ebx, edx
	je    .check_caps
	mov   edx, '('
	cmp   bl, '9'
	cmove ebx, edx
	je    .check_caps
	mov   edx, ')'
	cmp   bl, '0'
	cmove ebx, edx
	je    .check_caps
	mov   edx, '_'
	cmp   bl, '-'
	cmove ebx, edx
	je    .check_caps
	mov   edx, '+'
	cmp   bl, '='
	cmove ebx, edx
	je    .check_caps
	mov   edx, '{'
	cmp   bl, '['
	cmove ebx, edx
	je    .check_caps
	mov   edx, '}'
	cmp   bl, ']'
	cmove ebx, edx
	je    .check_caps
	mov   edx, '|'
	cmp   bl, '\'
	cmove ebx, edx
	je    .check_caps
	mov   edx, ':'
	cmp   bl, ';'
	cmove ebx, edx
	je    .check_caps
	mov   edx, '"'
	cmp   bl, "'"
	cmove ebx, edx
	je    .check_caps
	mov   edx, '<'
	cmp   bl, ','
	cmove ebx, edx
	je    .check_caps
	mov   edx, '>'
	cmp   bl, '.'
	cmove ebx, edx
	je    .check_caps
	mov   edx, '?'
	cmp   bl, '/'
	cmove ebx, edx
	je    .check_caps

.check_caps:

	test byte [locks], LOCKS_CAPS_LOCK
	jz   .update_stdin

	; if caps lock is on, then do upper case, but not special chars for digits

	cmp bl, 'a'
	jl  .update_stdin
	cmp bl, 'z'
	jg  .update_stdin

	sub bl, 32; difference between A and a

	; note, num lock will be detected in ps2_keyboard_get_ascii_from_code, and above .is_ascii
	; todo should we care if scroll lock is on here?

	; todo if super/gui is pressed, ???

	jmp .update_stdin

.update_stdin:
	ccall fputc, ebx, [stdin]
	;     fall through to "ignore" after updating

.ignore:
	mov eax, 1
	jmp .return

.unused:
	mov eax, 0

.return:
	pop esi
	pop ebx
	leave
	ret

	;    char ps2_keyboard_get_ascii_from_code(int code)
	code equ arg1

ps2_keyboard_get_ascii_from_code:
	enter 0, 0
	mov   ecx, code

	cmp byte [mode], SCAN_CODE_EXTENDED
	je  .extended

	mov eax, ASCII_ESCAPE
	cmp cl, SCAN_CODE_ESCAPE
	je  .return
	mov eax, ASCII_BACKSPACE
	cmp cl, SCAN_CODE_BACKSPACE
	je  .return
	mov eax, ASCII_NEWLINE
	cmp cl, SCAN_CODE_ENTER
	je  .return

	mov eax, ASCII_TAB
	cmp cl, SCAN_CODE_TAB
	je  .return
	mov eax, '1'
	cmp cl, SCAN_CODE_ONE
	je  .return
	mov eax, '2'
	cmp cl, SCAN_CODE_TWO
	je  .return
	mov eax, '3'
	cmp cl, SCAN_CODE_THREE
	je  .return
	mov eax, '4'
	cmp cl, SCAN_CODE_FOUR
	je  .return
	mov eax, '5'
	cmp cl, SCAN_CODE_FIVE
	je  .return
	mov eax, '6'
	cmp cl, SCAN_CODE_SIX
	je  .return
	mov eax, '7'
	cmp cl, SCAN_CODE_SEVEN
	je  .return
	mov eax, '8'
	cmp cl, SCAN_CODE_EIGHT
	je  .return
	mov eax, '9'
	cmp cl, SCAN_CODE_NINE
	je  .return
	mov eax, '0'
	cmp cl, SCAN_CODE_ZERO
	je  .return
	mov eax, '-'
	cmp cl, SCAN_CODE_MINUS
	je  .return
	mov eax, '='
	cmp cl, SCAN_CODE_EQUALS
	je  .return
	mov eax, 'q'
	cmp cl, SCAN_CODE_Q
	je  .return
	mov eax, 'w'
	cmp cl, SCAN_CODE_W
	je  .return
	mov eax, 'e'
	cmp cl, SCAN_CODE_E
	je  .return
	mov eax, 'r'
	cmp cl, SCAN_CODE_R
	je  .return
	mov eax, 't'
	cmp cl, SCAN_CODE_T
	je  .return
	mov eax, 'y'
	cmp cl, SCAN_CODE_Y
	je  .return
	mov eax, 'u'
	cmp cl, SCAN_CODE_U
	je  .return
	mov eax, 'i'
	cmp cl, SCAN_CODE_I
	je  .return
	mov eax, 'o'
	cmp cl, SCAN_CODE_O
	je  .return
	mov eax, 'p'
	cmp cl, SCAN_CODE_P
	je  .return
	mov eax, '['
	cmp cl, SCAN_CODE_LEFT_SQUARE_BRACKET
	je  .return
	mov eax, ']'
	cmp cl, SCAN_CODE_RIGHT_SQUARE_BRACKET
	je  .return
	mov eax, 'a'
	cmp cl, SCAN_CODE_A
	je  .return
	mov eax, 's'
	cmp cl, SCAN_CODE_S
	je  .return
	mov eax, 'd'
	cmp cl, SCAN_CODE_D
	je  .return
	mov eax, 'f'
	cmp cl, SCAN_CODE_F
	je  .return
	mov eax, 'g'
	cmp cl, SCAN_CODE_G
	je  .return
	mov eax, 'h'
	cmp cl, SCAN_CODE_H
	je  .return
	mov eax, 'j'
	cmp cl, SCAN_CODE_J
	je  .return
	mov eax, 'k'
	cmp cl, SCAN_CODE_K
	je  .return
	mov eax, 'l'
	cmp cl, SCAN_CODE_L
	je  .return
	mov eax, ';'
	cmp cl, SCAN_CODE_SEMI_COLON
	je  .return
	mov eax, "'"
	cmp cl, SCAN_CODE_SINGLE_QUOTE
	je  .return
	mov eax, '`'
	cmp cl, SCAN_CODE_BACK_TICK
	je  .return
	mov eax, '\\'
	cmp cl, SCAN_CODE_BACKSLASH
	je  .return
	mov eax, 'z'
	cmp cl, SCAN_CODE_Z
	je  .return
	mov eax, 'x'
	cmp cl, SCAN_CODE_X
	je  .return
	mov eax, 'c'
	cmp cl, SCAN_CODE_C
	je  .return
	mov eax, 'v'
	cmp cl, SCAN_CODE_V
	je  .return
	mov eax, 'b'
	cmp cl, SCAN_CODE_B
	je  .return
	mov eax, 'n'
	cmp cl, SCAN_CODE_N
	je  .return
	mov eax, 'm'
	cmp cl, SCAN_CODE_M
	je  .return
	mov eax, ', '
	cmp cl, SCAN_CODE_COMMA
	je  .return
	mov eax, '.'
	cmp cl, SCAN_CODE_FULL_STOP
	je  .return
	mov eax, '/'
	cmp cl, SCAN_CODE_FORWARD_SLASH
	je  .return
	mov eax, ' '
	cmp cl, SCAN_CODE_SPACE
	je  .return

	mov eax, '*'
	cmp cl, SCAN_CODE_KEYPAD_STAR
	je  .return
	mov eax, '-'
	cmp cl, SCAN_CODE_KEYPAD_MINUS
	je  .return
	mov eax, '+'
	cmp cl, SCAN_CODE_KEYPAD_PLUS
	je  .return

	xor  eax, eax
	test byte [locks], LOCKS_NUM_LOCK
	jz   .return

	mov eax, '1'
	cmp cl, SCAN_CODE_KEYPAD_ONE
	je  .return
	mov eax, '2'
	cmp cl, SCAN_CODE_KEYPAD_TWO
	je  .return
	mov eax, '3'
	cmp cl, SCAN_CODE_KEYPAD_THREE
	je  .return
	mov eax, '4'
	cmp cl, SCAN_CODE_KEYPAD_FOUR
	je  .return
	mov eax, '5'
	cmp cl, SCAN_CODE_KEYPAD_FIVE
	je  .return
	mov eax, '6'
	cmp cl, SCAN_CODE_KEYPAD_SIX
	je  .return
	mov eax, '7'
	cmp cl, SCAN_CODE_KEYPAD_SEVEN
	je  .return
	mov eax, '8'
	cmp cl, SCAN_CODE_KEYPAD_EIGHT
	je  .return
	mov eax, '9'
	cmp cl, SCAN_CODE_KEYPAD_NINE
	je  .return
	mov eax, '0'
	cmp cl, SCAN_CODE_KEYPAD_ZERO
	je  .return
	mov eax, '.'
	cmp cl, SCAN_CODE_KEYPAD_FULL_STOP
	je  .return

	xor eax, eax
	jmp .return

.extended:

	mov eax, ASCII_DELETE
	cmp cl, SCAN_CODE_DELETE
	je  .return

	mov eax, ASCII_NEWLINE
	cmp cl, SCAN_CODE_KEYPAD_ENTER
	je  .return
	mov eax, '/'
	cmp cl, SCAN_CODE_KEYPAD_FORWARD_SLASH
	je  .return

	xor eax, eax

.return:
	leave
	ret

	;    char ps2_keyboard_get_csi_from_code(int code)
	code equ arg1

ps2_keyboard_get_csi_from_code:
	enter 0, 0

	mov ecx, code

	; TODO if num lock is off, then do arrows etc as ANSI codes

	mov eax, CSI_CURSOR_UP
	cmp cl, SCAN_CODE_CURSOR_UP
	je  .return

	mov eax, CSI_CURSOR_DOWN
	cmp cl, SCAN_CODE_CURSOR_DOWN
	je  .return

	mov eax, CSI_CURSOR_LEFT
	cmp cl, SCAN_CODE_CURSOR_LEFT
	je  .return

	mov eax, CSI_CURSOR_RIGHT
	cmp cl, SCAN_CODE_CURSOR_RIGHT
	je  .return

	mov eax, CSI_SCROLL_UP
	cmp cl, SCAN_CODE_PAGE_UP
	je  .return

	mov eax, CSI_SCROLL_DOWN
	cmp cl, SCAN_CODE_PAGE_DOWN
	je  .return

	; TODO instead of sending it to input stream, send just the code to *_readkey_handle

.unused:
	mov eax, 0

.return:
	leave
	ret

	;    void ps2_keyboard_check_scroll(int code)
	code equ arg1

ps2_keyboard_check_scroll:
	enter 0, 0
	push  ebx
	push  esi
	mov   cl, 0
	mov   ebx, code
	test  bl, 0x80
	jz    .check_scroll
	mov   cl, 0x7F
	and   bl, cl
	mov   cl, 1

.check_scroll:
	;   TODO, implement scroll lock
	mov edx, .ansi_scroll_up
	cmp bl, SCAN_CODE_CURSOR_UP
	je  .is_scroll_key
	mov edx, .ansi_scroll_down
	cmp bl, SCAN_CODE_CURSOR_DOWN
	je  .is_scroll_key
	mov edx, .ansi_page_up
	cmp bl, SCAN_CODE_PAGE_UP
	je  .is_scroll_key
	mov edx, .ansi_page_down
	cmp bl, SCAN_CODE_PAGE_DOWN
	je  .is_scroll_key
	jmp .unused

.is_scroll_key:
	cmp   cl, 1
	je    .ignore; Ignore if released, but still "consume" the code
	ccall fputs, edx, [stdin]

	; fall through to ignore which says we consumed the key

.ignore:
	mov eax, 1
	jmp .return

.unused:
	mov eax, 0

.return:
	pop esi
	pop ebx
	leave
	ret

.ansi_scroll_up:
	db ASCII_ESCAPE, "[S", 0

.ansi_scroll_down:
	db ASCII_ESCAPE, "[T", 0

	; FIXME: hard coding vga_height

.ansi_page_up:
	db ASCII_ESCAPE, "[25S", 0

.ansi_page_down:
	db ASCII_ESCAPE, "[25T", 0

	;; int ps2_keyboard_send_with_resend(char data)

data equ arg1

ps2_keyboard_send_with_resend:
	enter 0, 0
	xor   eax, eax
	mov   ecx, PS2_RESEND_LIMIT

.resend:
	cmp ecx, 0
	je  .error
	dec ecx

.check_input_buffer:
	in   al, PS2_STATUS_PORT
	test al, PS2_STATUS_INPUT
	jnz  .check_input_buffer
	mov  al, data
	out  PS2_KEYBOARD_PORT, al
	xor  al, al

.check_output_buffer:
	in   al, PS2_STATUS_PORT
	test al, PS2_STATUS_OUTPUT
	jz   .check_output_buffer
	in   al, PS2_KEYBOARD_PORT
	cmp  al, PS2_RESEND; resend
	je   .resend
	;    return with response in eax (al)
	jmp  .return

.error:
	mov eax, -1

.return:
	leave
	ret

locks:
	db 0; bits caps num scroll

modifiers:
	db 0; bits gui, ctrl, alt, shift (left is high, right in low)

mode:
	db 0

	include "includes/scan-codes.inc"
