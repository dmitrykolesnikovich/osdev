	format  elf
	include "../includes/debug_symbols.inc"
	section '.text' executable
	;;      32 bit protected mode
	use32

	;      FIXME remove this when fixed tests
	public vga_escape_str

vga_escape_str:
	ret
public vga_disable_last_printed_str

vga_disable_last_printed_str:
	ret

public vga_enable_last_printed_str

vga_enable_last_printed_str:
	ret
public vga_last_printed_str

vga_last_printed_str:
	db 0
	;  FIXME remove this when fixed tests

	public vga_init
	public vga_putchar_at
	public vga_scroll_view_down
	public vga_scroll_view_up
	public vga_set_cursor
	public vga_enable_cursor
	public vga_disable_cursor

	include "includes/vga_text_constants.inc"
	include '../includes/font.inc'
	include '../includes/kernel.inc'
	include '../lib/includes/string.inc'

	VGA_BUFFER = 0xb8000
	VGA_CELL_WIDTH = 2

	VGA_PORT_REGISTER_C_COMMAND = 0x3D4
	VGA_PORT_REGISTER_C_DATA = 0x3D5
	VGA_PORT_CURSOR_START_INDEX = 0x0A
	VGA_PORT_CURSOR_END_INDEX = 0x0B
	VGA_PORT_MAX_SCANLINE_INDEX = 0x09
	VGA_PORT_CURSOR_LOC_HIGH_INDEX = 0x0E
	VGA_PORT_CURSOR_LOC_LOW_INDEX = 0x0F

	VGA_PORT_REGISTER_MISC_OUTPUT_WRITE = 0x3C2
	VGA_PORT_REGISTER_MISC_OUTPUT_READ = 0x3CC

	;; void vga_init()

vga_init:
	enter 0, 0
	ccall vga_set_io_address_select
	ccall vga_set_max_scanline, 0x0F
	ccall vga_set_cursor_size, 0x00, 0x0F
	leave
	ret

	;;   byte vga_get_colour_from_attr(struct font_attr attr)
	attr equ arg1

vga_get_colour_from_attr:
	enter 0, 0
	mov   eax, attr

	xor ecx, ecx
	mov cl, byte [font_attr.foreground_colour]
	mov ch, byte [font_attr.background_colour]

	mov dl, [font_attr.bright]
	and dl, 0x1
	shl dl, 3
	or  cl, dl

	mov dl, [font_attr.blink]
	and dl, 0x1
	shl dl, 3
	or  ch, dl

	shl cl, 4
	shr cx, 4
	mov eax, ecx

	leave
	ret

	;;   void vga_putchar_at(char c, struct attr attr, int x, int y)
	c    equ arg1
	attr equ arg2
	x    equ arg3
	y    equ arg4

vga_putchar_at:
	enter 0, 0

	; TODO locking?

	mov   eax, attr
	ccall vga_get_colour_from_attr, eax

	mov  edx, y
	imul edx, VGA_WIDTH
	add  edx, x
	shl  edx, 1
	add  edx, VGA_BUFFER

	mov cl, c
	mov ch, al

	mov [edx], cx

	leave
	ret

	;; void vga_set_cursor(int x, int y)
	x  equ arg1
	y  equ arg2

vga_set_cursor:
	enter 0, 0

	;   sending port stuff
	mov eax, y
	mov ecx, VGA_WIDTH
	mul ecx
	mov ecx, x
	add eax, ecx
	mov ecx, eax
	mov dx, VGA_PORT_REGISTER_C_COMMAND
	mov al, VGA_PORT_CURSOR_LOC_LOW_INDEX
	out dx, al
	mov dx, VGA_PORT_REGISTER_C_DATA
	mov al, cl
	out dx, al
	mov dx, VGA_PORT_REGISTER_C_COMMAND
	mov al, VGA_PORT_CURSOR_LOC_HIGH_INDEX
	out dx, al
	mov dx, VGA_PORT_REGISTER_C_DATA
	mov al, ch
	out dx, al

	leave
	ret

vga_set_io_address_select:
	enter 0, 0
	mov   dx, VGA_PORT_REGISTER_MISC_OUTPUT_READ
	in    al, dx
	or    al, 0x1
	mov   dx, VGA_PORT_REGISTER_MISC_OUTPUT_WRITE
	out   dx, al
	leave
	ret

	;;  void vga_set_max_scanline(int val)
	val equ arg1

vga_set_max_scanline:
	enter 0, 0
	mov   dx, VGA_PORT_REGISTER_C_COMMAND
	mov   al, VGA_PORT_MAX_SCANLINE_INDEX
	out   dx, al
	mov   dx, VGA_PORT_REGISTER_C_DATA
	in    al, dx
	or    al, val
	out   dx, al
	leave
	ret

	;;    void vga_set_cursor_size(int start, int end)
	start equ arg1
	fin   equ arg2

vga_set_cursor_size:
	enter 0, 0
	mov   dx, VGA_PORT_REGISTER_C_COMMAND
	mov   al, VGA_PORT_CURSOR_START_INDEX
	out   dx, al
	mov   dx, VGA_PORT_REGISTER_C_DATA
	in    al, dx
	and   al, 0xC0
	or    al, start
	out   dx, al
	mov   dx, VGA_PORT_REGISTER_C_COMMAND
	mov   al, VGA_PORT_CURSOR_END_INDEX
	out   dx, al
	mov   dx, VGA_PORT_REGISTER_C_DATA
	in    al, dx
	and   al, 0xE0
	or    al, fin
	out   dx, al
	leave
	ret

	;; void vga_enable_cursor()

vga_enable_cursor:
	enter 0, 0
	mov   dx, VGA_PORT_REGISTER_C_COMMAND
	mov   al, VGA_PORT_CURSOR_START_INDEX
	out   dx, al
	mov   dx, VGA_PORT_REGISTER_C_DATA
	in    al, dx
	and   al, not(0x20)
	out   dx, al
	leave
	ret

	;; void vga_disable_cursor()

vga_disable_cursor:
	enter 0, 0
	mov   dx, VGA_PORT_REGISTER_C_COMMAND
	mov   al, VGA_PORT_CURSOR_START_INDEX
	out   dx, al
	mov   dx, VGA_PORT_REGISTER_C_DATA
	in    al, dx
	or    al, 0x20
	out   dx, al
	leave
	ret

	;;     void vga_scroll_view_down(int x, int y, int width, int height)
	x      equ arg1
	y      equ arg2
	width  equ arg3
	height equ arg4

vga_scroll_view_down:
	enter 0, 0

	push ebx
	push esi
	push edi

	mov eax, x
	cmp eax, 0
	jl  .return
	mov ebx, y
	cmp ebx, 0
	jl  .return
	mov ecx, width
	add eax, ecx
	cmp eax, VGA_WIDTH
	jg  .return
	mov edx, height
	add ebx, edx
	cmp ebx, VGA_HEIGHT
	jg  .return

	mov  esi, y
	imul esi, VGA_WIDTH
	add  esi, x
	imul esi, VGA_CELL_WIDTH
	;    TODO in future, should have a double buffer front so reads are efficient
	add  esi, VGA_BUFFER
	mov  ebx, height
	dec  ebx

.loop:
	test ebx, ebx
	jz   .return
	;    TODO scroll by more than one line at a time
	mov  edi, esi
	add  esi, VGA_WIDTH * VGA_CELL_WIDTH

	mov   ecx, width
	imul  ecx, VGA_CELL_WIDTH
	ccall memcpy, edi, esi, ecx

	dec ebx
	jmp .loop

.return:
	pop edi
	pop esi
	pop ebx
	leave
	ret

	;;     void vga_scroll_view_up(int x, int y, int width, int height)
	x      equ arg1
	y      equ arg2
	width  equ arg3
	height equ arg4

vga_scroll_view_up:
	enter 0, 0

	push ebx
	push esi
	push edi

	mov eax, x
	cmp eax, 0
	jl  .return
	mov ebx, y
	cmp ebx, 0
	jl  .return
	mov ecx, width
	add eax, ecx
	cmp eax, VGA_WIDTH
	jg  .return
	mov edx, height
	add ebx, edx
	cmp ebx, VGA_HEIGHT
	jg  .return

	mov  esi, y
	add  esi, height
	dec  esi
	imul esi, VGA_WIDTH
	add  esi, x
	imul esi, VGA_CELL_WIDTH
	;    TODO in future, should have a double buffer front so reads are efficient
	add  esi, VGA_BUFFER
	mov  ebx, height
	dec  ebx

.loop:
	test ebx, ebx
	jz   .return
	;    TODO scroll by more than one line at a time
	mov  edi, esi
	sub  esi, VGA_WIDTH * VGA_CELL_WIDTH

	mov   ecx, width
	imul  ecx, VGA_CELL_WIDTH
	ccall memcpy, edi, esi, ecx

	dec ebx
	jmp .loop

.return:
	pop edi
	pop esi
	pop ebx
	leave
	ret
