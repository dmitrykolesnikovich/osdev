	macro ccall proc, [arg]
	{
	common
	local size
	size  = 0
	if    ~ arg eq
	reverse
	pushd arg
	size  = size + 4
	common
	end   if
	call  proc
	add   esp, size
	}
