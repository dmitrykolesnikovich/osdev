	struc   font_attr {
	.foreground_colour dd ?
	.background_colour dd ?
	.blink  db ?
	.bright db ?
	.used   db ?
	}

	virtual at eax
	font_attr font_attr
	sizeof.font_attr = $ - font_attr
	end     virtual
