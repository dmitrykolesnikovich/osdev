	; Some helpers for coding, should probably get renamed

	include "ccall.inc"

	macro todo
	{
	;     requires puts and panic to be loaded
	ccall puts, @f
	ccall panic

@@:
	db "TODO", 13, 10, 0
	}

	STACK_SIZE = 16384 ; 16 KiB

	;    TODO define these in sub files
	NULL = 0
	WORD_SIZE = 4
	STACK_ARGS_OFFSET = WORD_SIZE

	var1_offset equ -1 * 1 * WORD_SIZE
	var2_offset equ -1 * 2 * WORD_SIZE
	var3_offset equ -1 * 3 * WORD_SIZE
	var4_offset equ -1 * 4 * WORD_SIZE
	var5_offset equ -1 * 5 * WORD_SIZE
	var6_offset equ -1 * 6 * WORD_SIZE

	var1 equ dword [ebp + var1_offset]
	var2 equ dword [ebp + var2_offset]
	var3 equ dword [ebp + var3_offset]
	var4 equ dword [ebp + var4_offset]
	var5 equ dword [ebp + var5_offset]
	var6 equ dword [ebp + var6_offset]

	arg1_offset equ STACK_ARGS_OFFSET + 1 * WORD_SIZE
	arg2_offset equ STACK_ARGS_OFFSET + 2 * WORD_SIZE
	arg3_offset equ STACK_ARGS_OFFSET + 3 * WORD_SIZE
	arg4_offset equ STACK_ARGS_OFFSET + 4 * WORD_SIZE
	arg5_offset equ STACK_ARGS_OFFSET + 5 * WORD_SIZE
	arg6_offset equ STACK_ARGS_OFFSET + 6 * WORD_SIZE

	arg1 equ [ebp + arg1_offset]
	arg2 equ [ebp + arg2_offset]
	arg3 equ [ebp + arg3_offset]
	arg4 equ [ebp + arg4_offset]
	arg5 equ [ebp + arg5_offset]
	arg6 equ [ebp + arg6_offset]
