	format  elf
	include "includes/debug_symbols.inc"
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  kmain

	include 'lib/includes/stdio.inc'
	include 'lib/includes/unistd.inc'
	include 'lib/includes/string.inc'
	include 'lib/includes/ring_buffer.inc'
	include 'lib/includes/termios.inc'
	include 'drivers/includes/ps2_keyboard.inc'
	include 'includes/kernel.inc'

	extrn shell_main
	extrn term_new

kmain:
	enter 0, 0

	ccall ring_buffer_new
	mov   ecx, eax
	mov   eax, [stdin]
	mov   [stdio_file.object], ecx
	mov   [stdio_file.putc], ring_buffer_putc
	mov   [stdio_file.getc], ps2_keyboard_getc
	mov   [stdio_file.ungetc], ring_buffer_ungetc
	push  eax
	ccall termios_new, eax
	mov   ecx, eax
	pop   eax
	mov   [stdio_file.termios], ecx

	;     TODO remove this once preemptive scheduling going
	ccall fork
	test  eax, eax
	jz    .child

.parent:
	extrn schedule
	ccall schedule
	jmp   .parent

.child:
	ccall term_new, shell_main
	extrn stop_current_task
	ccall stop_current_task

.return:
	leave
	ret
