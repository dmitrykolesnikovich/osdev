	;     public music functions
	extrn music_parse_note
	extrn music_play_note
	extrn music_play_scale
	extrn music_play_chord

	include 'music_constants.inc'
