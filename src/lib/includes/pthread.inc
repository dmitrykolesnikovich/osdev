	;;    int pthread_spin_init(pthread_spinlock_t *lock, int pshared)
	extrn pthread_spin_init
	;;    int pthread_spin_destroy(pthread_spinlock_t *lock)
	extrn pthread_spin_destroy
	;;    int pthread_spin_lock(pthread_spinlock_t *lock)
	extrn pthread_spin_lock
	;;    int pthread_spin_trylock(pthread_spinlock_t *lock)
	extrn pthread_spin_trylock
	;;    int pthread_spin_unlock(pthread_spinlock_t *lock)
	extrn pthread_spin_unlock
