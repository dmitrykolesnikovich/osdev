	; test this on return value of readkey
	; Set when some flags are set
	READKEY_HAS_FLAG_MASK  = 1 shl 31

	READKEY_FLAGS_MASK         = 0x7F shl 24
	READKEY_FLAG_KEY_MASK      = 1 shl 30
	READKEY_FLAG_FUNC_MASK     = 1 shl 29
	READKEY_FLAG_AGAIN_MASK    = 1 shl 28
	READKEY_FLAG_MODIFIED_MASK = 1 shl 27
	; Make sure we don't go below 1 shl 24

	; Escaped keys
	READKEY_PROCESSING  = 0
	READKEY_UNKNOWN     = 1

	; Escaped keys, also uses the generic id's
	READKEY_KEY_CURSOR_UP    = 2
	READKEY_KEY_CURSOR_DOWN  = 3
	READKEY_KEY_CURSOR_LEFT  = 4
	READKEY_KEY_CURSOR_RIGHT = 5
	READKEY_KEY_PAGE_UP      = 6
	READKEY_KEY_PAGE_DOWN    = 7
	; Make sure we don't go over 256

	; Escaped functions
	; Some functions are doubled up as keys
	READKEY_FUNC_CURSOR_UP   = READKEY_KEY_CURSOR_UP
	READKEY_FUNC_CURSOR_DOWN = READKEY_KEY_CURSOR_DOWN
	READKEY_FUNC_PAGE_UP     = READKEY_KEY_PAGE_UP
	READKEY_FUNC_PAGE_DOWN   = READKEY_KEY_PAGE_DOWN

	; the rest of the functions have no overlap
	READKEY_FUNC_RESET                 = 16
	READKEY_FUNC_FONT_RESET            = 17
	READKEY_FUNC_SET_BRIGHT            = 18
	READKEY_FUNC_SET_BLINK             = 19
	READKEY_FUNC_SET_FOREGROUND_NIBBLE = 20
	READKEY_FUNC_SET_BACKGROUND_NIBBLE = 21
	; Make sure we don't go over 256

	MODIFIERS_GUI = 0x8
	MODIFIERS_CTRL = 0x4
	MODIFIERS_ALT = 0x2
	MODIFIERS_SHIFT = 0x1

	struc   readkey_struct {
	.escape_mode  db ?
	.csi_args_len dd ?
	.csi_args     dd ?
	}
	virtual at ebx
	readkey_struct        readkey_struct
	sizeof.readkey_struct = $ - readkey_struct
	end     virtual
