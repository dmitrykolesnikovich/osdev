	include 'ring_buffer_struc.inc'
	;;      struct ring_buffer ring_buffer_new()
	extrn   ring_buffer_new
	;;      int ring_buffer_putc(int c, struct ring_buffer buf)
	extrn   ring_buffer_putc
	;;      int ring_buffer_getc(struct ring_buffer buf)
	extrn   ring_buffer_getc
	;;      int ring_buffer_unputc(struct ring_buffer buf)
	extrn   ring_buffer_unputc
	;;      int ring_buffer_ungetc(int c, struct ring_buffer buf)
	extrn   ring_buffer_ungetc
