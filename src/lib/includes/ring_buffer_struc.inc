	struc     ring_buffer {
	.length   dd ?
	.start    dd ?
	.end      dd ?
	.capacity dd ?
	.buffer   dd ?
	}

	virtual at ebx
	ring_buffer ring_buffer
	sizeof.ring_buffer = $ - ring_buffer
	end     virtual
