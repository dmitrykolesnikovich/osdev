	include 'stdio_struct.inc'
	extrn   stdin
	extrn   stdout
	extrn   stderr
	;;      int printf(char *, ...)
	extrn   printf
	;;      int putc(char c, FILE *stream)
	extrn   putc
	;;      int fputc(char c, FILE *stream)
	extrn   fputc
	;;      int putchar(char)
	extrn   putchar
	;;      int fputs(char *, FILE *stream)
	extrn   fputs
	;;      int puts(char *)
	extrn   puts
	;;      void putnl(void)
	extrn   putnl
	;;      int getchar()
	extrn   getchar
	;;      int fgetc(FILE* stream)
	extrn   fgetc
	;;      int getc(FILE* stream)
	extrn   getc
	;;      int ungetc(int c, FILE* stream)
	extrn   ungetc
	;;      char *fgets(char *s, int size, FILE *stream)
	extrn   fgets
	;;      FILE* filestream(int fd)
	extrn   filestream
