	struc    stdio_file {
	.object  dd ?
	.putc    dd ?
	.getc    dd ?
	.ungetc  dd ?
	.termios dd ?
	.readkey dd ?
	}

	virtual at eax
	stdio_file        stdio_file
	sizeof.stdio_file = $ - stdio_file
	end     virtual
