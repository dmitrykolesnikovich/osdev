	;      todo check what these should be
	ICANON = 0x1
	ECHO   = 0x2

	VMIN = 0
	LEN_CC = 1

	;         optional_actions for tcsetattr
	TCSANOW   = 0
	TCSADRAIN = 1
	TCSAFLUSH = 2

	struc    termios_object {
	.termios dd ?
	.in_buffer  dd ?
	.out_buffer  dd ?
	.echo_stream  dd ?
	}
	virtual  at ebx
	termios_object        termios_object
	sizeof.termios_object = $ - termios_object
	end      virtual

	struc          termios {
	.c_iflag dd ?
	.c_oflag dd ?
	.c_cflag dd ?
	.c_lflag dd ?
	.c_cc dd ?
	;              TODO c_cc arrag
	}
	;              TODO make a struct/ends macro to hide the need for virtual
	;              see  "https://stackoverflow.com/questions/41843715/defining-a-structure-in-fasm-which-of-the-2-ways-is-better-in-what-situation#48058223"
	virtual        at edx
	termios        termios
	sizeof.termios = $ - termios
	end            virtual
