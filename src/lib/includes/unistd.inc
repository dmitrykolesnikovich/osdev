	;;    unsigned int sleep(unsigned int seconds)
	extrn sleep
	;;    int usleep(useconds_t usec)
	extrn usleep
	;;    ssize_t write(int fd, const void *buf, size_t count)
	extrn write
	;;    ssize_t read(int fd, const void *buf, size_t count)
	extrn read
	;;    pid_t fork()
	extrn fork
	;;    int close(int fd)
	extrn close
	;;    int dup2(int oldfd, int newfd)
	extrn dup2
	;;    int pipe(int[2] fds)
	extrn pipe

	STDIN_FILENO  equ 0
	STDOUT_FILENO equ 1
	STDERR_FILENO equ 2
