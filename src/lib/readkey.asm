	format  elf
	include "../includes/debug_symbols.inc"
	section '.text' executable
	;;      32 bit protected mode
	use32

	public readkey
	public readkey_new
	public readkey_process
	public writekey

	include '../includes/kernel.inc'
	include '../includes/ascii.inc'
	include '../includes/csi.inc'
	include 'includes/readkey_constants.inc'
	include 'includes/stdio.inc'
	include 'includes/stdlib.inc'
	include 'includes/unistd.inc'

	; Modes
	ESCAPE_MODE_NORMAL = 0
	ESCAPE_MODE_ESCAPE = 1
	ESCAPE_MODE_CSI    = 2
	ESCAPE_MODE_SGR    = 3

	; Fs escape codes
	FS_LOW = 0x60
	FS_HIGH = 0x7E
	FS_RESET = 'c'

	; Fe escape codes
	FE_LOW = 0x40
	FE_HIGH = 0x5F
	FE_CSI = '['

	; CSI escape codes
	CSI_PARAM_LOW = 0x30
	CSI_PARAM_HIGH = 0x3F
	CSI_INTERMEDIATE_LOW = 0x20
	CSI_INTERMEDIATE_HIGH = 0x2F
	CSI_TERMINATING_LOW = 0x40
	CSI_TERMINATING_HIGH = 0x7E

	SGR_RESET           = 0
	SGR_BRIGHT          = 1
	SGR_BLINK           = 5
	SGR_NOBRIGHT        = 22
	SGR_NOBLINK         = 25
	SGR_FOREGROUND_LOW  = 30
	SGR_FOREGROUND_HIGH = 37
	SGR_BACKGROUND_LOW  = 40
	SGR_BACKGROUND_HIGH = 47

	CSI_ARGS_CAP = 257

	;; int readkey_process_csi_param(struct readkey *r, char c)
	r  equ arg1
	c  equ arg2

readkey_process_csi_param:
	enter 0, 0
	push  ebx
	push  edi

	mov ebx, r
	mov edi, [readkey_struct.csi_args]
	add edi, [readkey_struct.csi_args_len]
	dec edi

	;   arguments separated by a semi-colon, missing arg is 0
	mov ecx, c
	cmp cl, CSI_ARG_DELIM
	je  .next_arg
	cmp cl, ':'
	jge .not_digit

	sub  cl, '0'
	xor  ax, ax
	mov  al, [edi]
	imul ax, 10
	add  al, cl
	mov  [edi], al
	jmp  .return

.next_arg:
	inc [readkey_struct.csi_args_len]

	cmp [readkey_struct.csi_args_len], CSI_ARGS_CAP
	jg  .invalid

	inc edi
	;   clear the next arg
	mov byte [edi], 0
	jmp .return

.not_digit:
	; TODO this may be a private range, technically the : isn't

.invalid:
	mov eax, READKEY_UNKNOWN
	jmp ._return

.return:
	;   READKEY_PROCESSING
	xor eax, eax

._return:
	pop edi
	pop ebx
	leave
	ret

	;; int readkey_process_csi_intermediate(struct readkey_struct *r, char c)
	r  equ arg1
	c  equ arg2

readkey_process_csi_intermediate:
	enter 0, 0

	; ignore silently

	;   READKEY_PROCESSING
	xor eax, eax
	leave
	ret

	;; void readkey_process_custom(struct readkey_struct *r)
	r  equ arg1

readkey_process_custom:
	enter 0, 0
	push  ebx
	push  edi

	mov ebx, r
	mov edx, [readkey_struct.csi_args_len]
	mov edi, [readkey_struct.csi_args]

	; \e[<keycode>; <modr+1>; <modl+1>~
	; if keycode is ESC, then should be followed by another CSI seq (after modifiers)
	; if keycode is NULL, then should be 4 bytes making up a readkey code from writekey

	xor eax, eax
	mov al, [edi]

	cmp al, NULL
	je  .readkey

	xor ecx, ecx
	mov ch, [edi+1]
	dec ch
	mov cl, [edi+2]
	dec cl
	shl ecx, 8
	or  eax, ecx
	or  eax, READKEY_HAS_FLAG_MASK or READKEY_FLAG_MODIFIED_MASK

	cmp al, ASCII_ESCAPE
	jne .return
	;   if keycode is ESC, then should be followed by another CSI seq (after modifiers)

	mov cl, [edi+3]

	mov edx, READKEY_KEY_CURSOR_UP or READKEY_FLAG_KEY_MASK or READKEY_FLAG_FUNC_MASK
	cmp cl, CSI_CURSOR_UP
	je  .return_key
	mov edx, READKEY_KEY_CURSOR_DOWN or READKEY_FLAG_KEY_MASK or READKEY_FLAG_FUNC_MASK
	cmp cl, CSI_CURSOR_DOWN
	je  .return_key
	mov edx, READKEY_KEY_CURSOR_LEFT or READKEY_FLAG_KEY_MASK or READKEY_FLAG_FUNC_MASK
	cmp cl, CSI_CURSOR_LEFT
	je  .return_key
	mov edx, READKEY_KEY_CURSOR_RIGHT or READKEY_FLAG_KEY_MASK or READKEY_FLAG_FUNC_MASK
	cmp cl, CSI_CURSOR_RIGHT
	je  .return_key
	mov edx, READKEY_KEY_PAGE_UP or READKEY_FLAG_KEY_MASK or READKEY_FLAG_FUNC_MASK
	cmp cl, CSI_SCROLL_UP
	je  .return_key
	mov edx, READKEY_KEY_PAGE_DOWN or READKEY_FLAG_KEY_MASK or READKEY_FLAG_FUNC_MASK
	cmp cl, CSI_SCROLL_DOWN
	je  .return_key
	jmp .return

.readkey:
	;   if keycode is NULL, then should be 4 bytes making up a readkey code from writekey
	xor eax, eax
	mov al, [edi+4]
	shl eax, 8
	mov al, [edi+3]
	shl eax, 8
	mov al, [edi+2]
	shl eax, 8
	mov al, [edi+1]
	jmp .return

.return_key:
	xor al, al
	or  eax, edx

.return:
	pop edi
	pop ebx
	leave
	ret

	;; void readkey_process_sgr(struct readkey_struct *r)
	r  equ arg1

readkey_process_sgr:
	enter 0, 0

	push ebx
	push edi

	mov ebx, r

	;   Do these in reverse order, flag the callee to call us again
	mov edx, [readkey_struct.csi_args_len]
	dec edx
	cmp edx, 0
	jl  .unknown

	mov [readkey_struct.csi_args_len], edx

	mov [readkey_struct.escape_mode], ESCAPE_MODE_NORMAL
	xor edi, edi

	test edx, edx
	jz   .sgr_process

	mov [readkey_struct.escape_mode], ESCAPE_MODE_SGR
	mov edi, READKEY_FLAG_AGAIN_MASK

.sgr_process:

	add edx, [readkey_struct.csi_args]
	mov cl, [edx]

	mov eax, READKEY_FUNC_FONT_RESET or READKEY_FLAG_FUNC_MASK
	or  eax, edi
	cmp cl, SGR_RESET
	jz  .return
	mov eax, (1 shl 8) or READKEY_FUNC_SET_BRIGHT or READKEY_FLAG_FUNC_MASK
	or  eax, edi
	cmp cl, SGR_BRIGHT
	jz  .return
	mov eax, (0 shl 8) or READKEY_FUNC_SET_BRIGHT or READKEY_FLAG_FUNC_MASK
	or  eax, edi
	cmp cl, SGR_NOBRIGHT
	jz  .return
	mov eax, (1 shl 8) or READKEY_FUNC_SET_BLINK or READKEY_FLAG_FUNC_MASK
	or  eax, edi
	cmp cl, SGR_BLINK
	jz  .return
	mov eax, (0 shl 8) or READKEY_FUNC_SET_BLINK or READKEY_FLAG_FUNC_MASK
	or  eax, edi
	cmp cl, SGR_NOBLINK
	jz  .return

	cmp cl, SGR_FOREGROUND_LOW
	jl  .not_foreground
	cmp cl, SGR_FOREGROUND_HIGH
	jg  .not_foreground

	sub cl, SGR_FOREGROUND_LOW
	mov eax, READKEY_FUNC_SET_FOREGROUND_NIBBLE or READKEY_FLAG_FUNC_MASK
	or  eax, edi
	mov ah, cl
	jmp .return

.not_foreground:
	cmp cl, SGR_BACKGROUND_LOW
	jl  .not_background
	cmp cl, SGR_BACKGROUND_HIGH
	jg  .not_background

	sub cl, SGR_BACKGROUND_LOW
	mov eax, READKEY_FUNC_SET_BACKGROUND_NIBBLE or READKEY_FLAG_FUNC_MASK
	or  eax, edi
	mov ah, cl
	jmp .return

.not_background:
	; TODO more codes

.unknown:
	mov eax, READKEY_UNKNOWN
	jmp .return

.return:
	pop edi
	pop ebx
	leave
	ret

	;; int readkey_process_csi_terminating(struct readkey_struct *r, char c)
	r  equ arg1
	c  equ arg2

readkey_process_csi_terminating:
	enter 0, 0
	push  ebx

	mov ebx, r

	; todo test which char it is
	; return that exit code, with args in higher bytes

	mov ecx, c

	cmp cl, CSI_SGR
	je  .sgr

	cmp cl, CSI_CUSTOM
	je  .custom

	mov edx, READKEY_KEY_CURSOR_UP or READKEY_FLAG_KEY_MASK or READKEY_FLAG_FUNC_MASK
	cmp cl, CSI_CURSOR_UP
	je  .return_args
	mov edx, READKEY_KEY_CURSOR_DOWN or READKEY_FLAG_KEY_MASK or READKEY_FLAG_FUNC_MASK
	cmp cl, CSI_CURSOR_DOWN
	je  .return_args
	mov edx, READKEY_KEY_PAGE_UP or READKEY_FLAG_KEY_MASK or READKEY_FLAG_FUNC_MASK
	cmp cl, CSI_SCROLL_UP
	je  .return_args
	mov edx, READKEY_KEY_PAGE_DOWN or READKEY_FLAG_KEY_MASK or READKEY_FLAG_FUNC_MASK
	cmp cl, CSI_SCROLL_DOWN
	je  .return_args

.sgr:
	ccall readkey_process_sgr, ebx
	jmp   .return

.custom:
	ccall readkey_process_custom, ebx
	jmp   .return

.return_args:
	mov eax, edx
	and eax, READKEY_FLAGS_MASK
	shr eax, 8
	mov ecx, [readkey_struct.csi_args]
	mov al, [ecx]
	mov ah, [ecx+1]
	shl eax, 8
	mov al, dl

.return:
	pop ebx
	leave
	ret

	;; int readkey_process_csi(struct readkey_struct *r, char c)
	r  equ arg1
	c  equ arg2

readkey_process_csi:
	enter 0, 0
	push  ebx

	mov ebx, r

	mov ecx, c

	cmp   cl, CSI_TERMINATING_LOW
	jl    .not_terminating
	cmp   cl, CSI_TERMINATING_HIGH
	jg    .not_terminating
	ccall readkey_process_csi_terminating, ebx, ecx
	jmp   .return

.not_terminating:
	mov   ecx, c
	cmp   cl, CSI_PARAM_LOW
	jl    .not_param
	cmp   cl, CSI_PARAM_HIGH
	jg    .not_param
	ccall readkey_process_csi_param, ebx, ecx
	jmp   .return

.not_param:
	cmp   cl, CSI_INTERMEDIATE_LOW
	jl    .not_intermediate
	cmp   cl, CSI_INTERMEDIATE_HIGH
	jg    .not_intermediate
	ccall readkey_process_csi_intermediate, ebx, ecx
	jmp   .return

.not_intermediate:
	; UNREACHABLE

.unknown:
	mov eax, READKEY_UNKNOWN

.return:
	pop ebx
	leave
	ret

	;; int readkey_process_fs(struct readkey_struct *r, char c)
	r  equ arg1
	c  equ arg2

readkey_process_fs:
	enter 0, 0
	push  ebx

	mov ebx, r

	mov ecx, c

	cmp cl, FS_RESET
	je  .reset
	jmp .unknown

.reset:
	mov eax, READKEY_FUNC_RESET or READKEY_FLAG_FUNC_MASK
	jmp .return

.unknown:
	mov eax, READKEY_UNKNOWN

.return:
	pop ebx
	leave
	ret

	;; int readkey_process_fe(struct readkey_struct *r, char c)
	r  equ arg1
	c  equ arg2

readkey_process_fe:
	enter 0, 0
	push  ebx

	mov ebx, r

	mov ecx, c

	cmp cl, FE_CSI
	je  .csi
	jmp .unknown

.csi:
	mov [readkey_struct.escape_mode], ESCAPE_MODE_CSI
	;   Clear args, defaults to 1 arg of 0 (which gets added to)
	mov [readkey_struct.csi_args_len], 1
	mov eax, [readkey_struct.csi_args]
	mov byte [eax], 0
	;   READKEY_PROCESSING
	xor eax, eax
	jmp .return

.unknown:
	mov eax, READKEY_UNKNOWN

.return:
	pop ebx
	leave
	ret

	;; int readkey_process_escape(struct readkey_struct *r, char c)
	r  equ arg1
	c  equ arg2

readkey_process_escape:
	enter 0, 0
	push  ebx

	mov ebx, r

	mov ecx, c

	cmp   cl, FS_LOW
	jl    .not_fs
	cmp   cl, FS_HIGH
	jg    .not_fs
	ccall readkey_process_fs, ebx, ecx
	jmp   .return

.not_fs:
	cmp   cl, FE_LOW
	jl    .not_fe
	cmp   cl, FE_HIGH
	jg    .not_fe
	ccall readkey_process_fe, ebx, ecx
	jmp   .return

.not_fe:
	;   TODO more codes
	jmp .unknown

.unknown:
	mov eax, READKEY_UNKNOWN

.return:
	pop ebx
	leave
	ret

	;; int readkey_process(struct readkey *r, char c)
	r  equ arg1
	c  equ arg2

	; high byte is used as a bitfield, if top bit is set, then it is an escape code
	; if it is clear, then an ascii char is in the low byte
	; if it is an escape code, then 2 args are put in the middle 2 bytes, and the low byte is the code
	; TODO support for more than 3 args, or more than 512 vals

	; TODO need to have one per task?

readkey_process:
	enter 0, 0
	push  ebx

	mov ebx, r
	mov eax, c
	cmp eax, -1
	je  .again

	cmp al, ASCII_ESCAPE
	je  .escape

	cmp [readkey_struct.escape_mode], ESCAPE_MODE_NORMAL
	je  .return

	cmp [readkey_struct.escape_mode], ESCAPE_MODE_ESCAPE
	je  .readkey_process_escape

	cmp [readkey_struct.escape_mode], ESCAPE_MODE_CSI
	je  .readkey_process_csi

	;     UNREACHABLE, these are the only modes set
	extrn panic
	ccall panic
	mov   eax, READKEY_UNKNOWN
	jmp   .return_escaped_key

.again:
	mov cl, [readkey_struct.escape_mode]
	mov [readkey_struct.escape_mode], ESCAPE_MODE_NORMAL
	cmp cl, ESCAPE_MODE_SGR
	je  .readkey_process_sgr

	;     UNREACHABLE, these are the only modes set that we expect AGAIN
	ccall panic
	mov   eax, READKEY_UNKNOWN
	jmp   .return_escaped_key

.readkey_process_escape:
	ccall readkey_process_escape, ebx, eax
	jmp   .return_escaped_key

.readkey_process_csi:
	ccall readkey_process_csi, ebx, eax
	jmp   .return_escaped_key

.readkey_process_sgr:
	ccall readkey_process_sgr, ebx, eax
	jmp   .return_escaped_key

.escape:
	mov [readkey_struct.escape_mode], ESCAPE_MODE_ESCAPE
	;   READKEY_PROCESSING
	xor eax, eax

	; fall through to set the mask

.return_escaped_key:
	or eax, READKEY_HAS_FLAG_MASK

	;    READKEY_PROCESSING
	test al, al
	je   .return

	test eax, READKEY_FLAG_AGAIN_MASK
	jnz  .return

	mov [readkey_struct.escape_mode], ESCAPE_MODE_NORMAL

.return:
	pop ebx
	leave
	ret

	;; int readkey(int fd)
	fd equ arg1

readkey:
	enter 0, 0
	push  ebx

	;     first test whether there is an AGAIN flag set
	mov   eax, fd
	ccall filestream, eax
	cmp   [stdio_file.readkey], NULL
	jnz   .initialized
	push  eax
	ccall readkey_new
	mov   ebx, eax
	pop   eax
	test  ebx, ebx
	jz    .error
	mov   [stdio_file.readkey], ebx

.initialized:
	mov ebx, [stdio_file.readkey]

	cmp [readkey_struct.escape_mode], ESCAPE_MODE_NORMAL
	je  .read

	;     TODO process AGAIN
	ccall readkey_process, ebx, -1
	jmp   .return

.read:
	mov   eax, fd
	ccall read, eax, .readkey_buf, 2
	cmp   eax, 0
	je    .error
	cmp   eax, 1
	;     Could just be a single ESC, in which case it is the key, not a sequence
	je    .return_buf

	;     process until back to normal mode
	xor   eax, eax
	mov   al, [.readkey_buf]
	ccall readkey_process, ebx, eax
	cmp   eax, READKEY_HAS_FLAG_MASK or READKEY_PROCESSING
	jne   .ungetc

	xor   eax, eax
	mov   al, [.readkey_buf+1 ]
	ccall readkey_process, ebx, eax
	cmp   eax, READKEY_HAS_FLAG_MASK or READKEY_PROCESSING
	jne   .return

.read1:
	mov   eax, fd
	ccall read, eax, .readkey_buf, 1
	cmp   eax, 0
	je    .error

	xor   eax, eax
	mov   al, [.readkey_buf]
	ccall readkey_process, ebx, eax
	cmp   eax, READKEY_HAS_FLAG_MASK or READKEY_PROCESSING
	je    .read1

	jmp .return

.ungetc:
	push  eax
	mov   eax, fd
	ccall filestream, eax
	xor   ecx, ecx
	mov   cl, [.readkey_buf+1]
	ccall ungetc, ecx, eax
	;     TODO check error
	pop   eax
	jmp   .return

.error:
	mov eax, -1
	jmp .return

.return_buf:
	xor eax, eax
	mov al, [.readkey_buf]

.return:
	pop ebx
	leave
	ret

.readkey_buf:
	times 2 db 0

	;;  void writekey(int fd, int key)
	fd  equ arg1
	key equ arg2

	; 0x88000465

writekey:
	enter 0, 0
	push  ebx
	push  edi

	;     send \e[0; a; b; c; d~, where a, b, c, d is the bytes making up key
	mov   eax, fd
	ccall filestream, eax
	mov   edi, eax

	mov  ebx, key
	test ebx, READKEY_HAS_FLAG_MASK
	jz   .putc
	;    better suited for fprintf, to avoid itoa

	ccall fputs, .csi_start_str, edi
	ccall putc, CSI_ARG_DELIM, edi
	xor   ecx, ecx
	mov   cl, bl
	ccall itoa, ecx, .itoa_buf, 10
	ccall fputs, .itoa_buf, edi
	ccall putc, CSI_ARG_DELIM, edi
	shr   ebx, 8
	xor   ecx, ecx
	mov   cl, bl
	ccall itoa, ecx, .itoa_buf, 10
	ccall fputs, .itoa_buf, edi
	ccall putc, CSI_ARG_DELIM, edi
	shr   ebx, 8
	xor   ecx, ecx
	mov   cl, bl
	ccall itoa, ecx, .itoa_buf, 10
	ccall fputs, .itoa_buf, edi
	ccall putc, CSI_ARG_DELIM, edi
	shr   ebx, 8
	xor   ecx, ecx
	mov   cl, bl
	ccall itoa, ecx, .itoa_buf, 10
	ccall fputs, .itoa_buf, edi
	ccall putc, CSI_CUSTOM, edi
	jmp   .return

.putc:
	ccall putc, ebx, edi

.return:
	pop edi
	pop ebx
	leave
	ret

.itoa_buf:
	times 33 db 0; Enough for 32 bit platforms

.csi_start_str:
	db ASCII_ESCAPE, CSI_DELIM_CHAR, 0

	;; struct readkey *readkey_new()

readkey_new:
	enter 0, 0
	push  ebx

	ccall calloc, sizeof.readkey_struct, 1
	test  eax, eax
	je    .return
	mov   ebx, eax

	ccall calloc, 1, CSI_ARGS_CAP
	test  eax, eax
	je    .return
	mov   [readkey_struct.csi_args], eax

	mov eax, ebx

.return:
	pop ebx
	leave
	ret
