	format  elf
	include "../includes/debug_symbols.inc"
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  strnrev
	public  strncpy
	public  memcpy
	public  memset
	public  strlen
	public  strcmp
	public  strtok
	public  strdup
	public  strndup

	include '../includes/kernel.inc'
	include '../lib/includes/stdlib.inc'

	;;  char *strnrev(char *str, int n)
	;   FIXME: currently this is inline, rather than returning
	;   args
	str equ arg1
	n   equ arg2

strnrev:
	enter 0, 0
	push  ebx
	push  esi
	push  edi

	mov   eax, str
	cmp   eax, NULL
	jz    .error
	ccall strlen, eax
	cmp   eax, n
	jl    .error

	; todo, malloc a new string and use that as edi
	; in which case, it could just be strrev (no n), and also get changed in itoa

	; ccall malloc, eax
	; test eax, eax
	; jz .error
	; mov edi, eax
	; mov esi, str
	; mov ecx, n
	; add esi, ecx
	; dec esi

;.loop:
	; test ecx, ecx
	; je   .success
	; std
	; lodsb
	; cld
	; stosb
	; dec  ecx
	; jmp  .loop

	; for now, do this longer inplace strnrev

	;   for (int i = 0; i < n/2; i ++)
	xor eax, eax
	mov ecx, n
	shr ecx, 1

.loop:
	;   char c = str[i]
	mov edx, str
	add edx, eax
	mov esi, edx
	xor edx, edx
	mov dl, [esi]
	;   str[i] = str[n-i-1]
	mov ebx, str
	add ebx, n
	sub ebx, eax
	dec ebx
	mov edi, ebx
	xor ebx, ebx
	mov bl, [edi]
	mov [esi], bl
	;   str[n-i-1] = str[i]
	mov [edi], dl
	inc eax
	cmp eax, ecx
	jge .success
	jmp .loop

.error:
	xor eax, eax
	jmp .return

.success:
	;   todo, malloc a new string and return that instead
	mov eax, str

.return:
	pop edi
	pop esi
	pop ebx
	leave
	ret

	;; char *strncpy(char *dest, const char *src, size_t n)

	;    TODO stop when src terminates with NULL byte if less than n
	;    for now, fall through to memcpy
	;;   void *memcpy(void *dest, void *src, size_t n)
	dest equ arg1
	src  equ arg2
	n    equ arg3

strncpy:
memcpy:
	enter 0, 0
	push  esi
	push  edi
	mov   edi, dest
	mov   esi, src
	mov   ecx, n

	; TODO: check for overlap, and call memmove instead
	; TODO: check for NULL

.loop:
	test ecx, ecx
	jz   .return
	lodsb
	stosb
	dec  ecx
	jmp  .loop

.return:
	mov eax, dest
	pop edi
	pop esi
	leave
	ret

	;; void *memset(void *s, inc c, size_t n)
	s  equ arg1
	c  equ arg2
	n  equ arg3

memset:
	enter 0, 0
	push  edi
	xor   eax, eax
	mov   al, c
	mov   edi, s
	mov   ecx, n

	; TODO: check for NULL

.loop:
	test ecx, ecx
	jz   .return
	stosb
	dec  ecx
	jmp  .loop

.return:
	mov eax, s
	pop edi
	leave
	ret

	;; size_t strlen(const char *s)
	s  equ arg1

strlen:
	enter 0, 0
	xor   eax, eax
	mov   ecx, s
	test  ecx, ecx
	jz    .return; s is NULL, return 0 length

.loop:
	cmp byte [ecx], 0
	jz  .return
	inc ecx
	inc eax
	jmp .loop

.return:
	leave
	ret

	;; int strcmp(const char *s1, const char *s2)
	s1 equ arg1
	s2 equ arg2

strcmp:
	enter 0, 0
	mov   ecx, s1
	mov   edx, s2
	xor   eax, eax
	test  ecx, ecx
	jz    .s1_null
	test  edx, edx
	jz    .more; s2 is NULL, but s1 wasn't
	jmp   .loop

.s1_null:
	test edx, edx
	jz   .return; both s1 and s2 are NULL
	jmp  .less; s1 is NULL, but s2 isn't

.loop:
	mov  al, [ecx]
	cmp  al, [edx]
	jl   .less
	jg   .more
	test al, al
	jz   .return
	inc  ecx
	inc  edx
	jmp  .loop

.less:
	mov eax, -1
	jmp .return

.more:
	mov eax, 1
	jmp .return

.return:
	leave
	ret

	;;    char *strtok(char *str, const char *delim)
	str   equ arg1
	delim equ arg2

strtok:
	enter 0, 0
	push  ebx
	push  esi
	push  edi
	cmp   dword delim, NULL
	je    .not_found
	mov   esi, str
	cmp   esi, NULL
	jnz   .str_not_null
	mov   esi, [.last_pointer]
	jmp   .skip_delim

.str_not_null:
	mov [.last_pointer], esi

.skip_delim:
	lodsb
	test al, al
	je   .not_found
	mov  ecx, delim

.skip_delim_loop:
	cmp byte [ecx], NULL
	je  .save_start
	cmp al, [ecx]
	je  .skip_delim
	inc ecx
	jmp .skip_delim_loop

.save_start:
	mov ebx, esi
	dec ebx

.process_str:
	lodsb
	test al, al
	je   .eof
	mov  ecx, delim

.process_str_loop:
	cmp byte [ecx], NULL
	je  .process_str
	cmp al, [ecx]
	je  .match
	inc ecx
	jmp .process_str_loop

.eof:
	dec  esi
	test esi, ebx
	inc  esi
	jnz  .match
	;    fall through

.not_found:
	mov eax, NULL
	jmp .return

.match:
	mov edi, esi
	dec edi
	mov al, NULL
	stosb
	mov eax, ebx
	mov [.last_pointer], esi

.return:
	pop edi
	pop esi
	pop ebx
	leave
	ret

.last_pointer:
	dd 0

	;; char *strdup(const char* s)

	s equ arg1

strdup:
	enter 0, 0
	push  ebx
	mov   ebx, s

	ccall strlen, ebx
	ccall strndup, ebx, eax

	pop ebx
	leave
	ret

	;; char *strndup(const char* s, size_t n)

	s equ arg1
	n equ arg2

strndup:
	enter 0, 0
	push  esi
	push  ebx

	mov esi, s
	mov ebx, n

	inc   ebx
	ccall malloc, ebx
	dec   ebx
	test  eax, eax
	jz    .error
	ccall memcpy, eax, esi, ebx
	mov   byte [eax + ebx], NULL
	jmp   .return

.error:
	mov eax, -1

.return:
	pop ebx
	pop esi
	leave
	ret
