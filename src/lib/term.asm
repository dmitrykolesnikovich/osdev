	format  elf
	include "../includes/debug_symbols.inc"
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  term_putc

	include '../includes/kernel.inc'
	include 'includes/music.inc'
	include 'includes/unistd.inc'
	include '../drivers/includes/vga_text.inc'
	include '../includes/font.inc'
	include '../includes/ascii.inc'
	include '../includes/scheduler.inc'
	include 'includes/stdlib.inc'
	include 'includes/string.inc'
	include 'includes/readkey.inc'
	include 'includes/stdio.inc'
	include 'includes/termios.inc'
	include 'includes/ring_buffer.inc'

	SCROLL_BUF_INIT_SIZE = 10 * 1024 ; 100KiB

	public term_new
	public term_init
	public term_free

	public term_readkey_handle

	;      TODO these are probably for the window manager to do?
	public term_move
	public term_resize

	include 'term_struc.inc'

	; TODO in future this will get dimensions from window manager
	VGA_WIDTH = 80
	VGA_HEIGHT = 25

	; Escape code uses 16 bit table, map it through memory to lookup later

vga_csi_sgr_colours:
	db VGA_COLOUR_BLACK
	db VGA_COLOUR_RED
	db VGA_COLOUR_GREEN
	db VGA_COLOUR_BROWN
	db VGA_COLOUR_BLUE
	db VGA_COLOUR_MAGENTA
	db VGA_COLOUR_CYAN
	db VGA_COLOUR_LIGHT_GREY; can make this white with the bold (fg), or blink (bg on some vga's) bits

	;;   void term_readkey_handle(struct term *t, int code)
	t    equ arg1
	code equ arg2

term_readkey_handle:
	enter 0, 0
	push  ebx

	mov ebx, t

	mov eax, code
	;   see what type of code it is
	cmp al, READKEY_UNKNOWN
	je  .unknown_code
	cmp al, READKEY_FUNC_RESET
	je  .reset
	cmp al, READKEY_FUNC_PAGE_UP
	je  .scroll_up
	cmp al, READKEY_FUNC_PAGE_DOWN
	je  .scroll_down
	cmp al, READKEY_FUNC_FONT_RESET
	je  .font_reset
	cmp al, READKEY_FUNC_SET_BRIGHT
	je  .set_bright
	cmp al, READKEY_FUNC_SET_BLINK
	je  .set_blink
	cmp al, READKEY_FUNC_SET_FOREGROUND_NIBBLE
	je  .set_foreground_nibble
	cmp al, READKEY_FUNC_SET_BACKGROUND_NIBBLE
	je  .set_background_nibble

.unknown_code:
	;   Unknown function
	;   TODO some warning?
	jmp .return

.reset:
	ccall term_reset, ebx
	jmp   .return

.scroll_up:
	ccall term_scroll_up_line, ebx
	cmp   dword [term.scroll_offset], 0
	je    .return
	ccall term_scroll_up_line, ebx
	cmp   dword [term.scroll_offset], 0
	je    .return
	ccall term_scroll_up_line, ebx
	cmp   dword [term.scroll_offset], 0
	je    .return
	ccall term_scroll_up_line, ebx
	cmp   dword [term.scroll_offset], 0
	je    .return
	ccall term_scroll_up_line, ebx
	jmp   .return

.scroll_down:
	ccall term_scroll_down_line, ebx
	mov   eax, [term.cursor_offset]
	cmp   [term.scroll_bottom], eax
	je    .return
	ccall term_scroll_down_line, ebx
	mov   eax, [term.cursor_offset]
	cmp   [term.scroll_bottom], eax
	je    .return
	ccall term_scroll_down_line, ebx
	mov   eax, [term.cursor_offset]
	cmp   [term.scroll_bottom], eax
	je    .return
	ccall term_scroll_down_line, ebx
	mov   eax, [term.cursor_offset]
	cmp   [term.scroll_bottom], eax
	je    .return
	ccall term_scroll_down_line, ebx
	jmp   .return

.font_reset:
	ccall term_font_reset, ebx
	jmp   .return

.set_bright:
	xor   ecx, ecx
	mov   cl, ah
	ccall term_font_set_bright, ebx, ecx
	jmp   .return

.set_blink:
	xor   ecx, ecx
	mov   cl, ah
	ccall term_font_set_blink, ebx, ecx
	jmp   .return

.set_foreground_nibble:
	xor   ecx, ecx
	mov   cl, ah
	mov   cl, [vga_csi_sgr_colours+ecx]
	ccall term_font_set_foreground, ebx, ecx
	jmp   .return

.set_background_nibble:
	xor   ecx, ecx
	mov   cl, ah
	mov   cl, [vga_csi_sgr_colours+ecx]
	ccall term_font_set_background, ebx, ecx
	jmp   .return

.return:
	pop ebx
	leave
	ret

	;; struct term *term_new(void *start_function)
	start_function equ arg1

term_new:
	enter 0, 0
	push  ebx
	push  esi
	push  edi

	ccall malloc, sizeof.term
	test  eax, eax
	jz    .error
	mov   ebx, eax

	ccall term_init, ebx

	mov [hack_term_ptr], ebx

	ccall malloc, sizeof.stdio_file
	test  eax, eax
	jz    .error

	mov   [stdio_file.object], ebx
	mov   [stdio_file.putc], term_putc
	extrn task_add_file
	ccall task_add_file, eax
	mov   edi, eax

	ccall malloc, WORD_SIZE*2
	mov   esi, eax
	ccall pipe, esi
	test  eax, eax
	jnz   .error

	ccall termios_new, edi
	test  eax, eax
	jz    .error
	mov   ecx, eax
	push  ecx
	;     read end of pipe
	ccall filestream, [esi]
	pop   ecx
	test  eax, eax
	jz    .error
	mov   [stdio_file.termios], ecx
	push  ecx
	;     write end of pipe
	ccall filestream, [esi+WORD_SIZE]
	pop   ecx
	test  eax, eax
	jz    .error
	mov   [stdio_file.termios], ecx

	ccall fork
	test  eax, eax
	jz    .child
	cmp   eax, -1
	je    .error

	; parent
	; esi is a pipe for stdin forwarding
	; edi is the stdout of the term

	;     read end of pipe
	ccall close, [esi]
	;     write end of pipe
	ccall dup2, [esi+WORD_SIZE], STDOUT_FILENO
	ccall close, [esi+WORD_SIZE]
	;     stdin should be kernel stdin (ps2), stdout should be our pipe write end

	ccall term_process_input, ebx
	jmp   .return

.child:
	; esi is a pipe for stdin forwarding
	; edi is the stdout of the term

	ccall dup2, edi, STDOUT_FILENO
	ccall close, edi

	;     write end of pipe
	ccall close, [esi+WORD_SIZE]
	;     read end of pipe
	ccall dup2, [esi], STDIN_FILENO
	ccall close, [esi]
	;     stdin should be read end of pipe, stdout should the term stdout

	mov   eax, start_function
	ccall eax
	jmp   .return

.error:
	ccall panic

.return:
	pop edi
	pop esi
	pop ebx
	leave
	ret

	;; void term_init(struct term *t)
	t  equ arg1

term_init:
	enter 0, 0
	push  ebx

	mov ebx, t

	mov   dword [term.width], 50
	mov   dword [term.height], 15
	mov   dword [term.offset_x], 10
	mov   dword [term.offset_y], 5
	ccall malloc, SCROLL_BUF_INIT_SIZE
	;     TODO error check
	mov   dword [term.scroll_buf], eax
	mov   dword [term.scroll_cap], SCROLL_BUF_INIT_SIZE

	ccall malloc, WORD_SIZE*SCROLL_BUF_INIT_SIZE
	;     TODO error check
	mov   dword [term.font_buf], eax

	;     TODO this should be done somewhere else?
	ccall term_font_setup_default

	ccall term_font_reset, ebx
	ccall term_clear, ebx, term_font_default

	ccall readkey_new
	mov   [term.readkey], eax

	; TODO set up a hook to stdout and stderr when we have hooks
	; a hook would allow us to have interceptors which would be handy for tests
	; it also allows different drivers to register as output

	; TODO it may run on different tasks at some point...

	pop ebx
	leave
	ret

	;; void term_free(struct term *t)
	t  equ arg1

term_free:
	enter 0, 0

	mov   eax, t
	ccall free, eax

	leave
	ret

	;; void term_reset(struct term *t)
	t  equ arg1

term_reset:
	enter 0, 0
	push  ebx

	mov ebx, t

	ccall term_font_reset, ebx
	ccall term_clear, ebx, term_font_default
	mov   eax, [term.cursor_offset]
	mov   [term.scroll_offset], eax
	mov   [term.scroll_bottom], eax

	pop ebx
	leave
	ret

	;; void term_add_to_scroll_buf(struct term *t, char c)
	t  equ arg1
	c  equ arg2

term_add_to_scroll_buf:
	enter 0, 0
	push  ebx

	mov ebx, t
	mov eax, c

	cmp dword [term.cursor_offset], -1
	je  .return; redrawing, UNREACHABLE
	mov edx, [term.cursor_offset]
	cmp edx, [term.scroll_cap]
	jge .overflow
	mov ecx, edx

	add edx, [term.scroll_buf]
	mov [edx], al

	imul ecx, WORD_SIZE
	add  ecx, [term.font_buf]
	mov  eax, [term.font]
	mov  [ecx], eax

	inc dword [term.cursor_offset]

	jmp .return

.overflow:
	push esi

	mov   esi, [term.scroll_cap]
	shl   esi, 1; double
	ccall realloc, [term.scroll_buf], esi
	test  eax, eax
	jz    .error
	mov   dword [term.scroll_buf], eax
	mov   [term.scroll_cap], esi

	imul  esi, WORD_SIZE
	ccall realloc, [term.font_buf], esi
	test  eax, eax
	jz    .error
	mov   dword [term.font_buf], eax

	pop esi

	mov   eax, c
	ccall term_add_to_scroll_buf, ebx, eax

	jmp .return

.error:
	mov   dword [term.cursor_offset], -1
	ccall puts, .overflow_str
	extrn panic
	ccall panic

.overflow_str:
	db "Overflow in term_add_to_scroll_buf, couldn't realloc", 13, 10, 0

.return:
	pop ebx
	leave
	ret

	;; term_bell(struct term *t)
	t  equ arg1

term_bell:
	enter 0, 0

	; TODO do something with the term (might store config?

	; TODO visual bell
	; TODO configurable sound and duration
	; TODO configurable type

	ccall music_play_note, MUSIC_NOTE_C_4, 15

	leave
	ret

	;; void term_output_newline(struct term *t)
	t  equ arg1

term_output_newline:
	enter 0, 0
	push  ebx

	mov ebx, t

	inc dword [term.output_cursor_y]
	mov eax, [term.output_cursor_y]
	cmp eax, [term.height]
	jl  .return

.scroll_down:
	mov   ecx, [term.scroll_offset]
	cmp   ecx, -1
	je    .return; redrawing
	;     TODO if scrolled up, don't do this
	ccall term_scroll_down_line, ebx

.return:
	pop ebx
	leave
	ret

	;; bool term_is_tabstop(struct term *t)
	t  equ arg1

term_is_tabstop:
	enter 0, 0
	push  ebx
	xor   eax, eax
	xor   edx, edx

	mov ebx, t

	;     TODO configurable tab stops
	mov   eax, [term.output_cursor_x]
	mov   ecx, 8
	div   ecx
	test  edx, edx
	setnz al

	pop ebx
	leave
	ret

	;; void term_output_tab(struct term *t)
	t  equ arg1

term_output_tab:
	enter 0, 0
	push  ebx
	push  esi

	mov ebx, t

	;    Set flag that we are redrawing
	;    The char \t is what is stored in scrollback, and we don't want the spaces
	push dword [term.scroll_offset]
	mov  dword [term.scroll_offset], -1

.loop:
	ccall term_putc, " ", ebx
	ccall term_is_tabstop, ebx
	test  eax, eax
	jz    .end_loop
	jmp   .loop

.end_loop:
	pop dword [term.scroll_offset]

	pop esi
	pop ebx
	leave
	ret

	;; void term_output_vertical_tab(struct term *t)
	t  equ arg1

term_output_vertical_tab:
	enter 0, 0
	push  ebx

	mov ebx, t

	;     basically newline without carriage return, save x
	push  dword [term.output_cursor_x]
	ccall term_output_newline, ebx
	pop   dword [term.output_cursor_x]

	pop ebx
	leave
	ret

	;; void term_output_backspace(struct term *t)
	t  equ arg1

term_output_backspace:
	enter 0, 0
	push  ebx

	mov ebx, t

	cmp dword [term.output_cursor_x], 0
	je  .last_line
	dec dword [term.output_cursor_x]

.clear:
	;     Set flag that we are redrawing
	;     The char \b is what is stored in scrollback, and we don't want the space
	push  dword [term.scroll_offset]
	mov   dword [term.scroll_offset], -1
	ccall term_putc, " ", ebx
	pop   dword [term.scroll_offset]
	dec   dword [term.output_cursor_x]

	; Note, if the char before is a tab, it won't get dealt with here.
	; This function simply moves the cursor back one char

	;     The putc above moved the cursor, so move it back
	ccall term_update_cursor, ebx, [term.output_cursor_x], [term.output_cursor_y]

	jmp .return

.last_line:
	; TODO go through scroll buf to find last char before \n (or last char)
	; save x and y, then dec y and set x approp
	; call this func recursively, then restore x and y
	; this would have to work with scrolling

.return:
	pop ebx
	leave
	ret

	;;   void term_output(struct term *t, char c, struct font_attr attr)
	t    equ arg1
	c    equ arg2
	attr equ arg3

term_output:
	enter 0, 0
	push  ebx

	mov ebx, t
	mov eax, c

	; FIXME: because we handle these here, there isn't an easy way to print the
	; extra vga font chars that they would otherwise
	; really this should get handled on stdin, and append to scroll_buf
	; then stdout can put these chars fine

	cmp al, ASCII_NEWLINE
	je  .newline
	cmp al, ASCII_CARRIAGE_RETURN
	je  .carriage_return
	cmp al, ASCII_TAB
	je  .tab
	cmp al, ASCII_BELL
	je  .bell
	cmp al, ASCII_BACKSPACE
	je  .backspace
	cmp al, ASCII_DELETE
	je  .delete
	cmp al, ASCII_FORM_FEED
	je  .form_feed
	cmp al, ASCII_VERTICAL_TAB
	je  .vertical_tab

	mov   ecx, attr
	ccall term_putchar_at, ebx, eax, ecx, [term.output_cursor_x], [term.output_cursor_y]
	inc   dword [term.output_cursor_x]
	mov   eax, [term.width]
	cmp   [term.output_cursor_x], eax
	jl    .return

	; fall through to newline

.newline:
	ccall term_output_newline, ebx

	; fall through to carriage_return (so \n === \r\n === \n\r)

.carriage_return:
	mov dword [term.output_cursor_x], 0
	jmp .return

.tab:
	ccall term_output_tab, ebx
	jmp   .return

.bell:
	ccall term_bell, ebx
	jmp   .return

.backspace:
	ccall term_output_backspace, ebx
	jmp   .return

.delete:
	;   TODO delete char at x+1 without moving cursor
	;   only makes sense if we can move the cursor
	jmp .return

.form_feed:
	ccall term_clear, ebx, term_font_default
	mov   eax, [term.cursor_offset]
	mov   [term.scroll_offset], eax
	mov   [term.scroll_bottom], eax
	jmp   .return

.vertical_tab:
	ccall term_output_vertical_tab, ebx
	jmp   .return

.return:
	pop ebx
	leave
	ret

	;; void term_putc(char c, struct term *t)
	c  equ arg1
	t  equ arg2

term_putc:
	enter 0, 0
	push  ebx

	mov ebx, t
	mov eax, c

	ccall readkey_process, [term.readkey], eax
	test  eax, READKEY_HAS_FLAG_MASK
	jnz   .readkey_flag

	test al, al
	jz   .return

	mov   ecx, [term.scroll_offset]
	cmp   ecx, -1
	je    .inc_scroll; redrawing
	cmp   al, ASCII_BELL
	je    .inc_scroll; don't add the bell to the buffer
	ccall term_add_to_scroll_buf, ebx, eax

.inc_scroll:
	mov eax, [term.output_cursor_y]
	cmp eax, [term.height]
	jge .return

	mov ecx, [term.scroll_offset]
	cmp ecx, -1
	je  .output; redrawing
	inc dword [term.scroll_bottom]

.output:
	mov   eax, c
	ccall term_output, ebx, eax, [term.font]
	jmp   .return

.readkey_flag:

	test eax, READKEY_FLAG_FUNC_MASK
	jne  .readkey_func

	jmp .return

.readkey_func:
	push  eax
	ccall term_readkey_handle, ebx, eax
	pop   eax

	test eax, READKEY_FLAG_AGAIN_MASK
	jz   .return

	ccall readkey_process, [term.readkey], -1
	jmp   .readkey_func

.return:
	mov eax, c
	pop ebx
	leave
	ret

	;;   void term_putchar_at(struct term *t, char c, struct attr attr, int x, int y)
	t    equ arg1
	c    equ arg2
	attr equ arg3
	x    equ arg4
	y    equ arg5

term_putchar_at:
	enter 0, 0
	push  esi
	push  ebx

	mov ebx, t

	mov eax, attr
	mov byte [font_attr.used], 1

	mov   eax, c
	mov   esi, attr
	mov   ecx, x
	mov   edx, y
	add   ecx, [term.offset_x]
	add   edx, [term.offset_y]
	;     TODO eventually we should detect what output there is and choose that, might not always be vga_text mode
	ccall vga_putchar_at, eax, esi, ecx, edx

	mov ecx, x
	mov edx, y

	inc ecx
	cmp ecx, [term.width]
	jl  .check_height
	xor ecx, ecx

	mov ecx, 0
	inc edx

.check_height:
	cmp edx, [term.height]
	jge .return

	ccall term_update_cursor, ebx, ecx, edx

.return:
	pop ebx
	pop esi
	leave
	ret

	;;   void term_clear_line(struct term *t, int line, struct font_attr font)
	t    equ arg1
	line equ arg2
	font equ arg3

term_clear_line:
	enter 0, 0
	push  ebx
	push  esi

	mov  ebx, t
	;    set redrawing flag, so not added to buffer
	push dword [term.scroll_offset]
	mov  dword [term.scroll_offset], -1

	mov dword [term.output_cursor_x], 0
	mov eax, line
	mov dword [term.output_cursor_y], eax

	mov esi, [term.width]

.loop:
	test esi, esi
	jz   .return

	mov   eax, font
	ccall term_output, ebx, ' ', font

	dec esi
	jmp .loop

.return:
	mov dword [term.output_cursor_x], 0
	mov eax, line
	mov dword [term.output_cursor_y], eax
	pop dword [term.scroll_offset]

	pop esi
	pop ebx
	leave
	ret

	;;   void term_clear(struct term *t, struct font_attr font)
	t    equ arg1
	font equ arg2

term_clear:
	enter 0, 0
	push  ebx
	push  esi

	mov ebx, t
	mov esi, [term.height]
	dec esi

.loop:
	mov   eax, font
	ccall term_clear_line, ebx, esi, eax

	test esi, esi
	jz   .return
	dec  esi
	jmp  .loop

.return:
	mov dword [term.output_cursor_x], 0
	mov dword [term.output_cursor_y], 0
	pop esi
	pop ebx
	leave
	ret

	;;     int term_redraw_line(struct term *t, int line, int x, int offset)
	t      equ arg1
	line   equ arg2
	x      equ arg3
	offset equ arg4
	;      offset is offset into scroll_buf, returns new offset

term_redraw_line:
	enter 0, 0
	push  ebx
	push  esi
	push  edi

	mov ebx, t

	;    set redrawing flag, so not added to buffer
	push dword [term.scroll_offset]
	mov  dword [term.scroll_offset], -1

	mov eax, line
	mov [term.output_cursor_y], eax
	mov eax, x
	mov [term.output_cursor_x], eax

	mov eax, offset
	mov esi, [term.scroll_buf]
	add esi, eax

	mov  edi, [term.font_buf]
	dec  eax
	imul eax, WORD_SIZE
	add  edi, eax

	mov ecx, offset

.loop:
	xor eax, eax
	lodsb
	add edi, WORD_SIZE

	; FIXME need to think about free'ing the fonts...

	test eax, eax
	je   .return

	inc ecx

	cmp al, ASCII_FORM_FEED
	je  .return

	cmp ecx, [term.cursor_offset]
	jg  .return

	push  ecx
	ccall term_output, ebx, eax, [edi]
	pop   ecx

	mov eax, [term.output_cursor_y]
	cmp eax, line
	jne .return
	jmp .loop

.return:

	mov eax, ecx

	;   TODO enable cursor only if we are at bottom
	pop dword [term.scroll_offset]
	pop edi
	pop esi
	pop ebx
	leave
	ret

	;; void term_redraw(struct term *t)
	t  equ arg1

term_redraw:
	enter 0, 0
	push  esi
	push  edi
	push  ebx

	mov ebx, t

	; TODO disable cursor
	; TODO double buffering

	ccall term_clear, ebx, term_font_default

	mov esi, [term.scroll_offset]
	xor edi, edi

.loop:
	ccall term_redraw_line, ebx, edi, 0, esi
	mov   esi, eax
	inc   edi
	cmp   esi, [term.cursor_offset]
	jge   .eof
	cmp   edi, [term.height]
	jge   .scrolled
	jmp   .loop

.eof:
	mov esi, [term.cursor_offset]
	mov dword [term.output_cursor_x], 0
	mov [term.output_cursor_y], edi

	jmp .return

.scrolled:
	mov edi, [term.height]
	mov [term.output_cursor_y], edi

.return:
	mov [term.scroll_bottom], esi

	; TODO double buffering
	; TODO enable cursor only if we are at bottom

	pop ebx
	pop edi
	pop esi
	leave
	ret

	;;     int term_find_last_line(struct term *t, int offset)
	t      equ arg1
	offset equ arg2

term_find_last_line:
	enter 0, 0
	push  esi
	push  ebx

	mov ebx, t

	std

	mov esi, [term.scroll_buf]
	;   TODO check offset is in range
	add esi, offset
	dec esi; start at char before offset
	mov al, [esi]
	cmp al, ASCII_NEWLINE
	je  .trim
	cmp al, ASCII_VERTICAL_TAB
	je  .trim
	cmp al, ASCII_FORM_FEED
	je  .trim
	jmp .loop

.trim:
	dec esi

.loop:
	cmp esi, [term.scroll_buf]
	jl  .return

	xor eax, eax
	lodsb
	cmp al, ASCII_NEWLINE
	je  .return_inc
	cmp al, ASCII_VERTICAL_TAB
	je  .return_inc; TODO in this case, we want x to start in the middle?
	cmp al, ASCII_FORM_FEED
	je  .return_inc

	jmp .loop

.return_inc:

	inc esi

.return:
	mov eax, esi
	sub eax, [term.scroll_buf]
	inc eax

	cld

	pop ebx
	pop esi
	leave
	ret

	;; void term_scroll_up_line(struct term *t)
	t  equ arg1

term_scroll_up_line:
	enter 0, 0
	push  esi
	push  ebx
	push  edi

	mov ebx, t

	cmp dword [term.scroll_offset], 0
	jle .error

	push dword [term.output_cursor_y]
	push dword [term.output_cursor_x]

	;     TODO in future, we should have a double buffer
	ccall vga_scroll_view_up, [term.offset_x], [term.offset_y], [term.width], [term.height]
	ccall term_find_last_line, ebx, [term.scroll_offset]

.redraw_loop:
	mov   ecx, eax
	push  ecx
	ccall term_redraw_line, ebx, 0, 0, ecx
	pop   ecx
	cmp   eax, [term.scroll_offset]
	jl    .redraw_loop

	push  ecx
	ccall term_clear_line, ebx, 0, term_font_default
	pop   ecx
	push  ecx

	ccall term_redraw_line, ebx, 0, 0, ecx
	pop   ecx

	mov [term.scroll_offset], ecx

	mov ecx, eax

	mov esi, [term.scroll_buf]
	add esi, eax
	dec esi
	lodsb

	cmp al, ASCII_VERTICAL_TAB
	jne .update_cursor

	push  ecx
	push  dword [term.output_cursor_x]
	ccall term_clear_line, ebx, 1, term_font_default
	pop   dword [term.output_cursor_x]
	pop   ecx
	ccall term_redraw_line, ebx, 1, [term.output_cursor_x], ecx

.update_cursor:
	pop   dword [term.output_cursor_x]
	pop   dword [term.output_cursor_y]
	inc   dword [term.output_cursor_y]
	ccall term_update_cursor, ebx, [term.output_cursor_x], [term.output_cursor_y]
	mov   eax, [term.height]
	cmp   [term.output_cursor_y], eax
	jl    .return

	ccall term_find_last_line, ebx, [term.scroll_bottom]
	mov   ecx, eax
	push  ecx
	ccall term_find_last_line, ebx, eax

	mov edi, [term.height]
	dec edi

.scroll_bottom_loop:
	pop  ecx
	mov  esi, ecx
	mov  ecx, eax
	push ecx

	ccall term_redraw_line, ebx, edi, 0, ecx

	cmp eax, [term.scroll_bottom]
	jl  .scroll_bottom_loop

	ccall term_clear_line, ebx, edi, term_font_default
	ccall term_redraw_line, ebx, edi, 0, esi

	pop ecx
	mov [term.scroll_bottom], ecx

	ccall vga_disable_cursor

	jmp .return

.error:
	ccall term_bell, ebx

.return:
	pop edi
	pop ebx
	pop esi
	leave
	ret

	;; void term_scroll_down_line(struct term *t)
	t  equ arg1

term_scroll_down_line:
	enter 0, 0
	push  esi
	push  ebx

	mov ebx, t

	mov eax, [term.scroll_bottom]
	cmp eax, [term.cursor_offset]
	jl  .scroll

	mov eax, [term.output_cursor_y]
	cmp eax, [term.height]
	jl  .error

.scroll:

	mov edx, 0
	mov esi, [term.scroll_buf]
	add esi, [term.scroll_offset]
	mov ecx, [term.width]

.loop:
	test ecx, ecx
	jle  .redraw

	xor eax, eax
	lodsb
	inc edx
	cmp al, ASCII_NEWLINE
	je  .redraw
	cmp al, ASCII_VERTICAL_TAB
	je  .redraw
	cmp al, ASCII_FORM_FEED
	je  .redraw
	cmp al, ASCII_BELL
	je  .loop
	cmp al, ASCII_CARRIAGE_RETURN
	je  .loop
	cmp al, ASCII_DELETE
	je  .loop; TODO this doesn't make sense in the scroll_buf, as it reqs moving cursor, also it isn't implemented yet
	cmp al, ASCII_TAB
	je  .tab
	cmp al, ASCII_BACKSPACE
	je  .backspace

	; TODO some assert that we checked all control chars?

	dec ecx
	jmp .loop

.tab:
	;   TODO add tabwidth up to tabstop
	;   edx will be count since 0 which can be used to do tabstop
	;   for now, assume it takes 4 chars
	sub ecx, 4
	jmp .loop

.backspace:
	inc ecx
	jmp .loop

.redraw:
	add [term.scroll_offset], edx

	;     TODO in future, we should have a double buffer
	ccall vga_scroll_view_down, [term.offset_x], [term.offset_y], [term.width], [term.height]

	mov esi, [term.scroll_buf]
	add esi, [term.scroll_bottom]
	dec esi
	lodsb

	cmp al, ASCII_VERTICAL_TAB
	je  .redraw_last_line

	mov dword [term.output_cursor_x], 0

.redraw_last_line:
	mov   esi, [term.height]
	dec   esi
	push  dword [term.output_cursor_x]
	ccall term_clear_line, ebx, esi, term_font_default
	pop   dword [term.output_cursor_x]

	mov   eax, [term.scroll_bottom]
	ccall term_redraw_line, ebx, esi, [term.output_cursor_x], eax
	mov   [term.scroll_bottom], eax

	cmp [term.cursor_offset], eax
	jne .return

	ccall term_update_cursor, ebx, [term.output_cursor_x], [term.output_cursor_y]
	ccall vga_enable_cursor

	jmp .return

.error:
	ccall term_bell, ebx

.return:
	pop ebx
	pop esi
	leave
	ret

term_font_setup_default:
	enter 0, 0

	;   TODO make this a config option
	mov eax, term_font_default
	mov [font_attr.foreground_colour], VGA_COLOUR_WHITE
	mov [font_attr.background_colour], VGA_COLOUR_GREEN
	mov [font_attr.bright], 0
	mov [font_attr.blink], 0

	mov eax, term_font_black
	mov [font_attr.foreground_colour], VGA_COLOUR_BLACK
	mov [font_attr.background_colour], VGA_COLOUR_BLACK
	mov [font_attr.bright], 0
	mov [font_attr.blink], 0

	leave
	ret

	;; void term_font_reset(struct term *t)
	t  equ arg1

term_font_reset:
	enter 0, 0
	push  ebx

	mov ebx, t
	mov dword [term.font], term_font_default

	pop ebx
	leave
	ret

	;; *(struct attr) term_font_copy(struct term *t)
	t  equ arg1

term_font_copy:
	enter 0, 0
	push  ebx

	mov ebx, t

	; FIXME need to think about free'ing the fonts...

	mov eax, [term.font]
	cmp byte [font_attr.used], 1
	jne .return

	ccall malloc, sizeof.font_attr
	test  eax, eax
	jz    .error

	ccall memcpy, eax, [term.font], sizeof.font_attr
	mov   byte [font_attr.used], 0

	jmp .return

.error:
	ccall puts, .malloc_str
	call  panic

.malloc_str:
	db "term_font_copy: Could not malloc new font", 13, 10, 0

.return:
	pop ebx
	leave
	ret

	;;    void term_font_set_bright(struct term *t, int state)
	t     equ arg1
	state equ arg2

term_font_set_bright:
	enter 0, 0
	push  ebx

	mov ebx, t

	ccall term_font_copy, ebx

	mov ecx, state
	mov [font_attr.bright], cl

	mov [term.font], eax

	pop ebx
	leave
	ret

	;;    void term_font_set_blink(struct term *t, int state)
	t     equ arg1
	state equ arg2

term_font_set_blink:
	enter 0, 0
	push  ebx

	mov ebx, t

	ccall term_font_copy, ebx

	mov ecx, state
	mov [font_attr.blink], cl

	mov [term.font], eax

	pop ebx
	leave
	ret

	;;     void term_font_set_foreground(struct term *t, int colour)
	t      equ arg1
	colour equ arg2

term_font_set_foreground:
	enter 0, 0
	push  ebx

	mov ebx, t

	ccall term_font_copy, ebx

	mov ecx, colour
	mov [font_attr.foreground_colour], ecx

	mov [term.font], eax

	pop ebx
	leave
	ret

	;;     void term_font_set_background(struct term *t, int colour)
	t      equ arg1
	colour equ arg2

term_font_set_background:
	enter 0, 0
	push  ebx

	mov ebx, t

	ccall term_font_copy, ebx

	mov ecx, colour
	mov [font_attr.background_colour], ecx

	mov [term.font], eax

	pop ebx
	leave
	ret

	;; void term_move(struct term *t, int x, int y)
	t  equ arg1
	x  equ arg2
	y  equ arg3

term_move:
	enter 0, 0
	push  ebx

	mov ebx, t

	; TODO this could be more efficient with a double buffer
	; can clear the old spot on back buffer
	; then copy old spot from front buffer to new spot on back buffer
	; then swap buffers

	mov ecx, x
	mov edx, y

	cmp ecx, 0
	jl  .error
	mov eax, [term.width]
	add eax, ecx
	cmp eax, VGA_WIDTH
	jg  .error

	cmp edx, 0
	jl  .error
	mov eax, [term.height]
	add eax, edx
	cmp eax, VGA_HEIGHT
	jg  .error

	;     TODO this should clean up where we were...
	;     for now, just clear it, but it should get done higher up by wm
	ccall term_clear, ebx, term_font_black

	mov eax, x
	mov [term.offset_x], eax
	mov eax, y
	mov [term.offset_y], eax

	ccall term_redraw, ebx

	xor eax, eax
	jmp .return

.error:
	mov eax, -1

.return:
	pop ebx
	leave
	ret

	;;     int term_resize(struct term *t, int width, int height)
	t      equ arg1
	width  equ arg2
	height equ arg3

term_resize:
	enter 0, 0
	push  ebx

	mov ebx, t

	mov ecx, width
	mov edx, height

	cmp ecx, 0
	jl  .error
	mov eax, [term.offset_x]
	add eax, ecx
	cmp eax, VGA_WIDTH
	jg  .error

	cmp edx, 0
	jl  .error
	mov eax, [term.offset_y]
	add eax, edx
	cmp eax, VGA_HEIGHT
	jg  .error

	;     TODO this should clean up where we were...
	;     for now, just clear it, but it should get done higher up by wm
	ccall term_clear, ebx, term_font_black

	mov eax, width
	mov [term.width], eax
	mov eax, height
	mov [term.height], eax

	;     TODO if smaller, need to work out the new scroll_offset
	;     if we are scrolled to the bottom (scroll_bottom==cursor_offset) and smaller, then work backwards so we can see the bottom still
	;     if we are not scrolled to the bottom, or getting bigger, keep same scroll_offset so the scroll view doesn't change
	ccall term_redraw, ebx

	xor eax, eax
	jmp .return

.error:
	mov eax, -1

.return:
	pop ebx
	leave
	ret

	;; void term_update_cursor(struct term *t, int x, int y)
	t  equ arg1
	x  equ arg2
	y  equ arg3

term_update_cursor:
	enter 0, 0
	push  ebx

	mov ebx, t
	;   TODO only do this when not scrolling
	;   TODO also only do this when the active term?
	;   we could have a cursor for the scroll
	mov eax, [term.scroll_offset]
	cmp eax, -1
	;   TODO hide the cursor
	je  .return

	mov ecx, x
	mov edx, y

	add   ecx, [term.offset_x]
	add   edx, [term.offset_y]
	ccall vga_set_cursor, ecx, edx

.return:
	pop ebx
	leave
	ret

	;; void term_process_input(struct term t)
	t  equ arg1

term_process_input:
	enter 0, 0
	push  ebx

	mov ebx, t

	ccall tcgetattr, STDIN_FILENO, .termios_p
	test  eax, eax
	;     TODO should this jump to loop, as it means that the stdin
	;     stream doesn't have termios, which is fine for this as
	;     it wants it in raw mode, and no termios is the same
	jnz   .return

	ccall cfmakeraw, .termios_p
	ccall tcsetattr, STDIN_FILENO, TCSANOW, .termios_p
	test  eax, eax
	jnz   .return

.loop:
	ccall readkey, STDIN_FILENO
	cmp   eax, -1
	je    .return

	ccall term_handle_readkey_input, ebx, eax
	test  eax, eax
	jz    .loop

	ccall writekey, STDOUT_FILENO, eax

	jmp .loop

.return:
	pop ebx
	leave
	ret

.termios_p:
	times sizeof.termios db 0

	;;  int term_handle_readkey_input(struct term *t, int key)
	t   equ arg1
	key equ arg2

term_handle_readkey_input:
	enter 0, 0
	push  ebx

	mov ebx, t
	mov eax, key

	test eax, READKEY_HAS_FLAG_MASK
	jnz  .readkey_flag

	;   Any single chars to handle?
	jmp .unused

	; \e[27; 1; 3; A

.readkey_flag:
	test eax, READKEY_FLAG_MODIFIED_MASK
	jnz  .modified_flag

	jmp .unused

.modified_flag:
	;   get modifier
	mov ecx, eax
	shr ecx, 8
	;   ch is rhs, cl is lhs
	;   don't care about sides
	or  cl, ch
	cmp cl, MODIFIERS_ALT
	je  .alt_modifier

	jmp .unused

.alt_modifier:
	test eax, READKEY_FLAG_KEY_MASK
	jnz  .alt_with_key

	; TODO any alt+chars to handle?

	jmp .unused

.alt_with_key:
	cmp al, READKEY_KEY_CURSOR_UP
	je  .move_up
	cmp al, READKEY_KEY_CURSOR_DOWN
	je  .move_down
	cmp al, READKEY_KEY_CURSOR_LEFT
	je  .move_left
	cmp al, READKEY_KEY_CURSOR_RIGHT
	je  .move_right

	jmp .unused

.move_up:
	mov   eax, [term.offset_y]
	dec   eax
	ccall term_move, ebx, [term.offset_x], eax
	jmp   .used

.move_down:
	mov   eax, [term.offset_y]
	inc   eax
	ccall term_move, ebx, [term.offset_x], eax
	jmp   .used

.move_left:
	mov   eax, [term.offset_x]
	dec   eax
	ccall term_move, ebx, eax, [term.offset_y]
	jmp   .used

.move_right:
	mov   eax, [term.offset_x]
	inc   eax
	ccall term_move, ebx, eax, [term.offset_y]
	jmp   .used

.used:
	xor eax, eax
	jmp .return

.unused:
	mov eax, key

.return:
	pop ebx
	leave
	ret

term_font_default:
	times sizeof.font_attr db 0

term_font_black:
	times sizeof.font_attr db 0

	;      TODO remove this when we deal with stdout for diff processes
	public hack_term_ptr

hack_term_ptr:
	dd 0
