	struc    term {
	.task    dd ?
	.subtask dd ?
	.width   dd ?
	.height  dd ?
	.offset_x dd ?
	.offset_y dd ?
	;        TODO this should be a circular buffer type thing
	;        when overflowing, should skip forward one line (til \n) at the top
	.scroll_buf dd ?
	.scroll_cap dd ?
	.font_buf dd ?
	;        TODO name some things viewport or something
	;        where is the start of the scroll_view
	.scroll_offset dd ?
	.scroll_bottom dd ?
	;        where is our cursor in the scroll_buf
	.cursor_offset dd ?
	;        this may differ from cursor_offset due to where the line wraps
	;        based 0, 0, should get transformed using term_width/height and term_offset

	.output_cursor_x dd ?
	.output_cursor_y dd ?
	.font       dd ?
	.readkey    dd ?
	}
	virtual     at ebx
	term        term
	sizeof.term = $ - term
	end         virtual
