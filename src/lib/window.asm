	format  elf
	include "../includes/debug_symbols.inc"
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  window_new
	public  window_init
	public  window_free

	extrn panic

	include '../includes/kernel.inc'
	include 'includes/stdlib.inc'
	include 'window_struc.inc'

	; TODO write a window manager
	; it will take some stuff from term (such as resizing keybindings)
	; it will do decorating (compositing?) for borders etc
	; it will deal with overlap
	; it will have hotkeys for switching focus, and creating new windows
	; will need a term command to do the same
	; it will have to redirect the stdin to the active window

	; will need to have file descriptors opened by pipe for each window
	; so the window opening needs some setup in the win task (good case for syscalls..)

	;; struct window *window_new()

window_new:
	enter 0, 0
	push  ebx

	ccall malloc, sizeof.window
	test  eax, eax
	jz    .error

	mov ebx, eax

	ccall window_init, ebx

	mov eax, ebx
	jmp .return

.error:
	ccall panic

.return:
	pop ebx
	leave
	ret

	;;     void window_init(struct window *window)
	window equ arg1

window_init:
	enter 0, 0

	; TODO need initialise the window structure

	leave
	ret

	;;     void window_free(struct window *window)
	window equ arg1

window_free:
	enter 0, 0

	mov   eax, window
	ccall free, eax

	leave
	ret
