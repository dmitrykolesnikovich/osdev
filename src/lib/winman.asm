	format  elf
	include "../includes/debug_symbols.inc"
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  winman_new
	public  winman_init
	public  winman_free

	extrn panic

	include '../includes/kernel.inc'
	include 'includes/stdio.inc'
	include 'includes/stdlib.inc'
	include 'includes/unistd.inc'
	include 'includes/termios.inc'
	include 'includes/readkey.inc'
	include 'window_struc.inc'
	include 'winman_struc.inc'

	; TODO write a winman manager
	; it will take some stuff from term (such as resizing keybindings)
	; it will do decorating (compositing?) for borders etc
	; it will deal with overlap
	; it will have hotkeys for switching focus, and creating new winmans
	; will need a term command to do the same
	; it will have to redirect the stdin to the active winman

	; will need to have file descriptors opened by pipe for each winman
	; so the winman opening needs some setup in the win task (good case for syscalls..)

	;; void winman_new()

winman_new:
	enter 0, 0
	push  ebx
	push  esi

	ccall malloc, sizeof.winman
	test  eax, eax
	jz    .error

	mov ebx, eax

	ccall winman_init, ebx

	ccall winman_process_input, ebx
	;     unreachable

	jmp .return

.error:
	ccall panic

.return:
	pop esi
	pop ebx
	leave
	ret

	;;     void winman_init(struct winman *winman)
	winman equ arg1

winman_init:
	enter 0, 0
	push  ebx

	mov   ebx, winman
	mov   [winman.active_window], NULL
	ccall malloc, sizeof.window * 10
	mov   [winman.windows], eax

	pop ebx
	leave
	ret

	;;     void winman_free(struct winman *winman)
	winman equ arg1

winman_free:
	enter 0, 0

	mov   eax, winman
	ccall free, eax

	leave
	ret

	;;     void winman_process_input(struct winman *winman)
	winman equ arg1

winman_process_input:
	enter 0, 0
	push  ebx

	mov ebx, winman

	ccall tcgetattr, STDIN_FILENO, .termios_p
	test  eax, eax
	;     TODO should this jump to loop, as it means that the stdin
	;     stream doesn't have termios, which is fine for this as
	;     it wants it in raw mode, and no termios is the same
	jnz   .return

	ccall cfmakeraw, .termios_p
	ccall tcsetattr, STDIN_FILENO, TCSANOW, .termios_p
	test  eax, eax
	jnz   .return

.loop:
	ccall readkey, STDIN_FILENO
	cmp   eax, -1
	je    .return

	ccall winman_handle_readkey_input, ebx, eax
	test  eax, eax
	jz    .loop

	; TODO this needs to get the active window, and the correct file stream

	ccall writekey, STDOUT_FILENO, eax

	jmp .loop

.return:
	pop ebx
	leave
	ret

.termios_p:
	times sizeof.termios db 0

	;;     int winman_handle_readkey_input(struct winman *winman, int key)
	winman equ arg1
	key    equ arg2

winman_handle_readkey_input:
	enter 0, 0
	push  ebx

	mov ebx, winman
	mov eax, key

	test eax, READKEY_HAS_FLAG_MASK
	jnz  .readkey_flag

	;   Any single chars to handle?
	jmp .unused

	; \e[27; 1; 3; A

.readkey_flag:
	test eax, READKEY_FLAG_MODIFIED_MASK
	jnz  .modified_flag

	jmp .unused

.modified_flag:
	;   get modifier
	mov ecx, eax
	shr ecx, 8
	;   ch is rhs, cl is lhs
	;   don't care about sides
	or  cl, ch
	cmp cl, MODIFIERS_ALT
	je  .alt_modifier

	jmp .unused

.alt_modifier:
	test eax, READKEY_FLAG_KEY_MASK
	jnz  .alt_with_key

	; TODO any alt+chars to handle?

	jmp .unused

.alt_with_key:
	; TODO any alt+keys to handle?

	jmp .unused

.used:
	xor eax, eax
	jmp .return

.unused:
	mov eax, key

.return:
	pop ebx
	leave
	ret

winman_new_window:
	enter 0, 0
	push  esi

	; TODO this function isn't really done
	; it was copied from term_new
	; it still needs:
	; - create a window
	; - set new window as active (maybe?)

	ccall malloc, WORD_SIZE*2
	mov   esi, eax
	ccall pipe, esi
	test  eax, eax
	jnz   .error

	ccall termios_new, stdout
	test  eax, eax
	jz    .error
	mov   ecx, eax
	push  ecx
	;     read end of pipe
	ccall filestream, [esi]
	pop   ecx
	test  eax, eax
	jz    .error
	mov   [stdio_file.termios], ecx
	push  ecx
	;     write end of pipe
	ccall filestream, [esi+WORD_SIZE]
	pop   ecx
	test  eax, eax
	jz    .error
	mov   [stdio_file.termios], ecx

	ccall fork
	test  eax, eax
	jz    .child
	cmp   eax, -1
	je    .error

	jmp .return

.child:
	; esi is a pipe for stdin forwarding
	; edi is the stdout of the term

	; TODO  the parent should just return
	; the child should create the window object
	; the child should set up file descriptors for the winman
	; the file descriptors will probably be in window_struc.inc

	;ccall dup2, edi, STDOUT_FILENO
	;ccall close, edi

	;     write end of pipe
	ccall close, [esi+WORD_SIZE]
	;     read end of pipe
	ccall dup2, [esi], STDIN_FILENO
	ccall close, [esi]
	;     stdin should be read end of pipe, stdout should the term stdout

	;     TODO do something with the window
	;     mov   eax, start_function
	ccall eax
	jmp   .return

.error:
	ccall panic

.return:
	pop esi
	leave
	ret
