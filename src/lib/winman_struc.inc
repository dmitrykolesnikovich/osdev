	struc         winman {
	.active_window dd ? ; pointer to active window
	.windows      dd ?; pointer to list of windows
	}
	virtual       at ebx
	winman        winman
	sizeof.winman = $ - winman
	end           virtual
