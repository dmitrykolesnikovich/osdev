	format  elf
	include "includes/debug_symbols.inc"
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  dump_memory_map
	public  memory_init
	public  memory_get_chunk

	include 'includes/kernel.inc'
	include 'lib/includes/stdio.inc'

	MEMORY_DESCRIPTOR_BASE = 0x500
	EXTENDED_MEMORY_BASE = 0x00100000
	MEMORY_DESCRIPTOR_TYPE_FREE = 1
	MEMORY_DESCRIPTOR_TYPE_RESERVED = 2
	MEMORY_DESCRIPTOR_TYPE_ACPI_RECLAIM = 3
	MEMORY_DESCRIPTOR_TYPE_ACPI_NVS = 4
	MEMORY_DESCRIPTOR_TYPE_BAD = 5

	; custom types
	MEMORY_DESCRIPTOR_TYPE_LOW = 6
	MEMORY_DESCRIPTOR_TYPE_USED = 7

	include 'memory_struc.inc'

dump_memory_map:
	enter 0, 0
	push  ebx
	push  esi

	mov   ebx, [MEMORY_DESCRIPTOR_BASE]
	ccall printf, .size_str, ebx
	mov   esi, MEMORY_DESCRIPTOR_BASE+WORD_SIZE

.loop:
	test ebx, ebx
	jz   .return

	mov   eax, [memory_descriptor.base_high]
	mov   ecx, [memory_descriptor.base_low]
	ccall printf, .base_str, eax, ecx

	mov   eax, [memory_descriptor.length_high]
	mov   ecx, [memory_descriptor.length_low]
	ccall printf, .length_str, eax, ecx

	mov   eax, [memory_descriptor.type]
	ccall printf, .type_str, eax

	add esi, sizeof.memory_descriptor
	dec ebx
	jmp .loop

.return:
	pop esi
	pop ebx
	leave
	ret

.size_str:
	db "Size of memory descriptor table: %d", 13, 10, 0

.base_str:
	db "Base address %p%08x", 13, 10, 0

.length_str:
	db "Length %p%08x", 13, 10, 0

.type_str:
	db "Type %d", 13, 10, 0

memory_init:
	; todo in future, do some paging or something
	; this function atm just sets non extended memory to type 6
	; type 6 is a custom type saying unusable low memory

	enter 0, 0
	push  ebx
	push  esi

	mov ebx, [MEMORY_DESCRIPTOR_BASE]
	mov esi, MEMORY_DESCRIPTOR_BASE+WORD_SIZE

.loop:
	test ebx, ebx
	jz   .return

	mov  eax, [memory_descriptor.base_high]
	test eax, eax
	jnz  .continue

	mov eax, [memory_descriptor.base_low]
	cmp eax, EXTENDED_MEMORY_BASE
	jae .continue

	mov [memory_descriptor.type], MEMORY_DESCRIPTOR_TYPE_LOW

.continue:
	add esi, sizeof.memory_descriptor
	dec ebx
	jmp .loop

.return:
	pop esi
	pop ebx
	leave
	ret

	;; void *memory_get_chunk(size_t *size_ptr)
	size_ptr equ arg1

memory_get_chunk:
	enter 0, 0
	push  ebx
	push  esi

	xor eax, eax

	mov ebx, [MEMORY_DESCRIPTOR_BASE]
	mov esi, MEMORY_DESCRIPTOR_BASE+WORD_SIZE

.loop:
	test ebx, ebx
	jz   .return

	mov eax, [memory_descriptor.type]
	cmp eax, MEMORY_DESCRIPTOR_TYPE_FREE
	je  .alloc

.continue:
	add esi, sizeof.memory_descriptor
	dec ebx
	jmp .loop

.alloc:
	;   todo some way to get high memory, PAE?
	mov [memory_descriptor.type], MEMORY_DESCRIPTOR_TYPE_USED
	mov ecx, size_ptr
	;   todo some way to get high memory, PAE?
	mov edx, [memory_descriptor.length_low]
	mov [ecx], edx
	mov eax, [memory_descriptor.base_low]

	; fall through to return

.return:
	pop esi
	pop ebx
	leave
	ret
