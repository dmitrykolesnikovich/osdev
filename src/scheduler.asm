	format  elf
	include "includes/debug_symbols.inc"
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  sched_add_builtin

	public initialise_multitasking
	public new_task
	public start_task
	public stop_current_task
	public schedule
	public task_setup_switch_to_task_stack

	;      FIXME this is a bit gross
	public hack_current_task

	extrn shell_add_builtin

	include 'includes/kernel.inc'
	include 'lib/includes/stdlib.inc'
	include 'lib/includes/stdio.inc'
	include 'lib/includes/unistd.inc'
	include 'lib/includes/string.inc'

	STATE_UNKNOWN = 0
	STATE_STOPPED = 1
	STATE_PAUSED  = 2
	STATE_RUNNING = 3
	STATE_DEAD    = 4

	include 'includes/scheduler_struc.inc'

	; TODO more posix'y name?
	; pthread_init or something?

initialise_multitasking:
	enter 0, 0

	push edi

	ccall malloc, sizeof.thread_control_block
	test  eax, eax
	je    .error
	mov   edi, eax

	cmp dword [pid_count], 0
	jne .error

	mov   dword [pid_count], 1
	mov   [thread_control_block.pid], 1
	extrn kernel_stack_top; FIXME this is hacky
	mov   [thread_control_block.stack_top], kernel_stack_top
	mov   [thread_control_block.esp], NULL; this will get set in switch_to_task
	mov   eax, cr3
	mov   [thread_control_block.cr3], eax
	mov   [thread_control_block.next], edi
	mov   [thread_control_block.state], STATE_RUNNING

	ccall malloc, WORD_SIZE*3
	test  eax, eax
	je    .error
	mov   [thread_control_block.file_descriptors_len], 3
	mov   [thread_control_block.file_descriptors], eax

	mov [hack_current_task], edi

	mov edi, eax
	mov eax, edi
	add eax, STDIN_FILENO*WORD_SIZE
	mov edx, [stdin]
	mov [eax], edx
	mov eax, edi
	add eax, STDOUT_FILENO*WORD_SIZE
	mov edx, [stdout]
	mov [eax], edx
	mov eax, edi
	add eax, STDERR_FILENO*WORD_SIZE
	mov edx, [stderr]
	mov [eax], edx

	xor eax, eax
	jmp .return

.error:
	extrn panic
	ccall panic

.return:
	pop edi
	leave
	ret

	;; struct thread_control_block *new_task(void *start_routine, ...args)

	; TODO more posix'y name?
	; maybe pthread_create to make a new one? (and store id's in a hash?)
	start_routine equ arg1

new_task:
	enter 0, 0

	push ebx
	push edi
	push esi

	ccall malloc, STACK_SIZE
	test  eax, eax
	je    .error
	add   eax, STACK_SIZE
	mov   ebx, eax

	;   save old stack, then setup new stack and frame pointer
	mov esi, esp
	mov esp, ebx
	mov edi, ebx

	;   push args of start_routine
	mov ecx, [ebp]
	sub ecx, WORD_SIZE
	mov eax, ebp
	add eax, arg1_offset

.loop:
	cmp ecx, eax
	je  .add_stack_frame

	push dword [ecx]
	sub  edi, WORD_SIZE
	sub  ecx, WORD_SIZE
	jmp  .loop

.add_stack_frame:

	push task_exit_fallthrough
	sub  edi, WORD_SIZE
	mov  ecx, start_routine
	push ecx
	push edi; this will be the stack frame to "return" to task_exit_fallthrough
	sub  edi, 2*WORD_SIZE

	;    push ebp as expected in switch_to_task
	push edi
	sub  edi, WORD_SIZE

	;     reset stack to this current task
	mov   esp, esi
	;     push dummy edi, esi, ebx popped in switch_to_task
	ccall task_setup_switch_to_task_stack, edi

	mov esi, eax

	ccall malloc, sizeof.thread_control_block
	test  eax, eax
	je    .error
	mov   edi, eax

	mov ecx, [pid_count]
	inc ecx
	mov [pid_count], ecx
	mov [thread_control_block.pid], ecx
	mov [thread_control_block.stack_top], ebx
	mov [thread_control_block.esp], esi
	mov eax, cr3
	mov [thread_control_block.cr3], eax
	mov [thread_control_block.state], STATE_STOPPED
	mov eax, edi
	mov edi, [hack_current_task]
	mov ecx, [thread_control_block.next]
	mov [thread_control_block.next], eax
	mov edi, eax
	mov [thread_control_block.next], ecx

	ccall task_copy_fds, edi
	test  eax, eax
	jnz   .error

	mov eax, edi
	jmp .return

.error:
	mov eax, NULL

.return:
	pop esi
	pop edi
	pop ebx
	leave
	ret

	;; void task_copy_fds(struct thread_control_block *t)
	t  equ arg1

task_copy_fds:
	enter 0, 0
	push  ebx
	push  edi
	push  esi

	mov edi, [hack_current_task]
	mov esi, [thread_control_block.file_descriptors_len]
	mov ebx, [thread_control_block.file_descriptors]

	mov   edi, t
	mov   [thread_control_block.file_descriptors_len], esi
	mov   eax, esi
	imul  eax, WORD_SIZE
	ccall malloc, eax
	test  eax, eax
	jz    .error
	mov   [thread_control_block.file_descriptors], eax

	mov edi, eax

.fds_loop:
	test esi, esi
	jz   .success

	ccall malloc, sizeof.stdio_file
	test  eax, eax
	jz    .error
	mov   ecx, [ebx]
	ccall memcpy, eax, ecx, sizeof.stdio_file
	mov   [edi], eax

	add edi, WORD_SIZE
	add ebx, WORD_SIZE
	dec esi
	jmp .fds_loop

.success:
	xor eax, eax
	jmp .return

.error:
	mov eax, -1

.return:
	pop esi
	pop edi
	pop ebx
	leave
	ret

	;;  void* task_setup_switch_to_task_stack(void *stk)
	stk equ arg1

task_setup_switch_to_task_stack:
	enter 0, 0

	mov edx, esp
	mov eax, stk
	mov esp, eax

	;    ebp is also pushed, but this should be done before this
	;    as ebp will reference *this task's stack*, not the new one
	push edi
	push esi
	push ebx

	sub eax, 3*WORD_SIZE
	mov esp, edx

	; asmcheck won't like this

	leave
	ret

	;;   void start_task(struct thread_control_block *task)
	task equ arg1

start_task:
	enter 0, 0
	push  edi

	mov edi, task
	mov [thread_control_block.state], STATE_PAUSED

	ccall schedule

	pop edi
	leave
	ret

	;; void task_exit_fallthrough

	; this function is setup in new_task as a fallthrough of start_routine, if that exits, then we call pthread_exit with it's ret (eax)

task_exit_fallthrough:
	enter 0, 0

	;    eax is ret of what fell through to this
	push eax

	;    FIXME this should be pthread_exit or something
	;    or could just fallthrough?
	call stop_current_task
	;;   unreachable
	call panic
	pop  eax

	leave
	ret

	;; void stop_current_task

stop_current_task:
	enter 0, 0
	push  edi

	mov edi, [hack_current_task]
	mov [thread_control_block.state], STATE_DEAD

	ccall schedule

	;;   unreachable
	call panic

	pop edi
	leave
	ret

	;; void schedule()

schedule:
	enter 0, 0
	push  edi
	cli

	mov edi, [hack_current_task]
	mov edi, [thread_control_block.next]

.loop:
	test edi, edi
	jz   .return
	cmp  edi, [hack_current_task]
	je   .return
	cmp  [thread_control_block.state], STATE_RUNNING
	je   .return
	cmp  [thread_control_block.state], STATE_PAUSED
	je   .switch
	mov  edi, [thread_control_block.next]
	jmp  .loop

.switch:
	ccall switch_to_task, edi

.return:
	sti
	pop edi
	leave
	ret

	; TODO more posix'y name?
	; maybe sched_yield (man 7 sched)

	;;void switch_to_task(thread_control_block *next)
	;      Adapted from "https://wiki.osdev.org/Brendan%27s_Multi-tasking_Tutorial"
	next   equ arg1

switch_to_task:
	enter 0, 0
	push  ebp
	push  edi
	push  esi
	push  ebx

	mov edi, [hack_current_task]
	mov [thread_control_block.esp], esp
	cmp [thread_control_block.state], STATE_DEAD
	je  .start_next
	mov [thread_control_block.state], STATE_PAUSED

.start_next:

	mov edi, next
	mov [hack_current_task], edi
	mov [thread_control_block.state], STATE_RUNNING

	mov esp, [thread_control_block.esp]
	mov eax, [thread_control_block.cr3]
	;   TODO may need a TSS and have esp0
	mov ecx, cr3

	mov  edi, [thread_control_block.file_descriptors]
	test edi, edi
	je   .error

	mov edx, edi
	add edx, STDIN_FILENO*WORD_SIZE
	mov edx, [edx]
	mov [stdin], edx
	mov edx, edi
	add edx, STDOUT_FILENO*WORD_SIZE
	mov edx, [edx]
	mov [stdout], edx
	mov edx, edi
	add edx, STDERR_FILENO*WORD_SIZE
	mov edx, [edx]
	mov [stderr], edx

	cmp eax, ecx
	je  .return
	mov cr3, eax
	jmp .return

.error:
	ccall panic

.return:
	pop ebx
	pop esi
	pop edi
	pop ebp
	leave
	ret

public sched_test

sched_test:
	enter 0, 0

	ccall puts, .test_start_str

	ccall new_task, simple_task, 1, 2
	ccall start_task, eax
	ccall simple_task, 3, 4
	ccall schedule
	ccall puts, .test_end_str

	leave
	ret

.test_start_str:
	db "Starting sched test, creating another task to run", 13, 10, 0

.test_end_str:
	db "Ended sched test", 13, 10, 0

	;; void simple_task(int a, int b)
	a  equ arg1
	b  equ arg2

simple_task:
	enter 0, 0

	push edi
	mov  edi, [hack_current_task]

	mov ecx, 10

.loop:
	test  ecx, ecx
	jz    .return
	push  ecx
	mov   eax, a
	mov   edx, b
	ccall printf, .task_str, [thread_control_block.pid], ecx, eax, edx
	ccall switch_to_task, [thread_control_block.next]
	pop   ecx
	dec   ecx
	jmp   .loop

.return:
	pop edi
	leave
	ret

.task_str:
	db "In task %d, ecx = %d, a=%d, 5=%d", 13, 10, 0

	;      TODO this is a bit hacky, this is basically part of open()
	public task_add_file

	;;     int task_add_file(FILE *stream)
	stream equ arg1

task_add_file:
	enter 0, 0
	push  edi
	push  ebx

	mov   edi, [hack_current_task]
	mov   ebx, [thread_control_block.file_descriptors_len]
	inc   ebx
	mov   eax, ebx
	imul  eax, WORD_SIZE
	mov   edx, [thread_control_block.file_descriptors]
	ccall realloc, edx, eax
	test  eax, eax
	je    .error
	mov   [thread_control_block.file_descriptors], eax
	mov   [thread_control_block.file_descriptors_len], ebx

	dec  ebx
	mov  ecx, ebx
	imul ecx, WORD_SIZE
	add  ecx, eax
	mov  eax, stream
	mov  [ecx], eax
	mov  eax, ebx
	jmp  .return

.error:
	mov eax, -1

.return:
	pop ebx
	pop edi
	leave
	ret

	; TODO at some point this will be an ID pthread_self returns and can be looked up in a hash

sched_add_builtin:
	enter 0, 0
	ccall shell_add_builtin, sched_cmd_str, sched_shortdoc_str, sched_test
	leave
	ret

sched_cmd_str:
	db "sched_test", 0

sched_shortdoc_str:
	db "Simple test of multitasking scheduler", 0

hack_current_task:
	dd 0; pointer to current task structure

pid_count:
	dd 0
