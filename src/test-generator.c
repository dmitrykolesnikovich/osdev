// This is a script to on host to generate test cases based on host libraries

#include "tests/test-macros.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUF_CAP 1000
int main()
{
    char str[BUF_CAP];
    int start = snprintf(str, BUF_CAP, "\\e[0m");
    for (int bg = 0; bg < 8; bg++) {
        for (int fg = 0; fg < 8; fg++) {
            start += snprintf(
                str + start, BUF_CAP - start, "\\e[1;5;3%1$d;4%2$dm%1$d%2$d", fg, bg);
        }
        start += snprintf(str + start, BUF_CAP - start, "\\n");
    }
    start += snprintf(str + start, BUF_CAP - start, "\\e[0m");
    GEN_TEST(puts, s, str);

    return 0;
}
