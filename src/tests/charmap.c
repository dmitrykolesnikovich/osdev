#include "../lib/includes/stdio.h"

void test_charmap()
{
    puts("    0   1   2   3   4   5   6   7   8   9   a   b   c   d   e   f\n");
    int c;
    for (c = 0; c < 256; c++) {
        if (c % 16 == 0) {
            if (c == 0) {
                puts("\n0 NUL");
            } else {
                printf("\n%x   %c", c / 16, (char)c);
            }
        } else {
            if (c == '\a') {
                puts(" BEL");
            } else if (c == '\b') {
                puts("  BS");
            } else if (c == '\t') {
                puts(" TAB");
            } else if (c == '\n') {
                puts("  NL");
            } else if (c == '\v') {
                puts("  VT");
            } else if (c == '\f') {
                puts("  FF");
            } else if (c == '\r') {
                puts("  CR");
            } else if (c == '\e') {
                puts(" ESC");
            } else {
                printf("   %c", (char)c);
            }
        }
    }
    putnl();
    return;
}
