#include "../lib/includes/stdio.h"
#include "../lib/includes/string.h"
#include "test-macros.h"
#include <stddef.h>
// For size_t

// TODO: This is used to test what vga actually prints, feels hacky
// extern char* vga_last_printed_str();

typedef struct {
    char* s;
    int expected;
} test_args;

void test_puts()
{
    test_args test_cases[] = {
        { "Escape chars for CSI \\e\\[(\\d*( *; *)?)*[A-Za-z], m is for SGR mode\n", 67 },
        { "\e[30mBlack chars\e[0m\n", 21 },
        { "\e[31mRed chars\e[0m\n", 19 },
        { "\e[32mGreen chars\e[0m\n", 21 },
        { "\e[33mYellow/brown chars\e[0m\n", 28 },
        { "\e[34mBlue chars\e[0m\n", 20 },
        { "\e[35mMagenta chars\e[0m\n", 23 },
        { "\e[36mCyan chars\e[0m\n", 20 },
        { "\e[37mWhite chars\e[0m\n", 21 },
        { "\e[31mRed chars\e[0m\n", 19 },
        { "\e[1;31mBright red\e[0m\n", 22 },
        { "\e[33mYellow/brown chars\e[0m\n", 28 },
        { "\e[1;33mBright yellow\e[0m\n", 25 },
        { "Normal chars\n", 13 },
        { "\e[30;47mBlack on white\e[0m\n", 27 },
        { "\e[36;45mCyan on magenta\e[0m\n", 28 },
        { "\e[1;36;45mBright Cyan on magenta\e[0m\n", 37 },
        { "\e[1;5;36;45mBlinking (qemu has bright bg) Bright Cyan on magenta\e[0m\n", 69 },
        { "\e[5;36;45mBlinking Cyan on magenta and \e[25m not blinking same colours\e[0m\n", 75 },
        { "\e[1;36;45mBright Cyan on magenta and \e[22m not bright same colours\e[0m\n", 71 },
        // Don't reset
        { "\e[31m\n", 6 },
        { "Red chars\e[0m\n", 14 },
        // TODO: escape chars for more colours (256 + rgb)
        { "", 0 },
        { "a\n", 2 },
        { "ab\n", 3 },
        { "abc\n", 4 },
        { "dabc\n", 5 },
        { "dabcsahtashtsast\n", 17 },
        { "\ttab\n", 5 },
        { " space\n", 7 },
        { "cr\r", 3 },
        { "crnl\r\n", 6 },
        // TODO: better tests for the control chars, like compare to what the cursor actually does
        // Should probably clear the screen each time and compare the whole vga buf?
        { "Testing backspace.abc\b\n", 23 },

        { "Testing backspace.xxx\b\b\bab\n", 27 },

        { "tabs\tshould\tbe shorter when\tnot aligned to 8\n", 45 },
        { "\fform feed should clear the screen\n", 35 },
        { "Vertical\vtab\n", 13 },
        { "\ecShould reset term\n", 20 },
    };

    puts("Testing int puts(const char *s)\n");
    for (size_t i = 0; i < sizeof(test_cases) / sizeof(test_cases[0]); ++i) {
        test_args t = test_cases[i];
        TEST_OR_PANIC(puts, t.expected, int, s, t.s);
        // TODO some way to test whether the string is actually printed
        // char* printed = vga_last_printed_str();
        // TEST_OR_PANIC(strcmp, 0, int, s, t.s, printed);
    }
    puts("Testing NULL\n");
    TEST_OR_PANIC(puts, -1, int, s, NULL);
    puts("Finished testing puts\n");
}
