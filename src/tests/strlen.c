#include "../lib/includes/stdio.h"
#include "../lib/includes/string.h"
#include "test-macros.h"

typedef struct {
    const char* s;
    size_t expected;
} test_args;

void test_strlen()
{
    test_args test_cases[] = {
        { "", 0 },
        { "a", 1 },
        { "ab", 2 },
        { "abc", 3 },
        { "dabc", 4 },
        { "dabcsahtashtsast", 16 },
        { "newline\n", 8 },
        { "\ttab", 4 },
        { " space", 6 },
        { "cr\r", 3 },
        { "crnl\r\n", 6 },
    };

    puts("Testing int strcmp(const char *s1, const char *s2)\n");
    for (size_t f = 0; f < sizeof(test_cases) / sizeof(test_cases[0]); ++f) {
        test_args t = test_cases[f];
        TEST_OR_PANIC(strlen, t.expected, size_t, s, t.s);
    }
    puts("Testing NULL\n");
    // this is printing garbage from last string
    TEST_OR_PANIC(strlen, 0, size_t, s, NULL);
    puts("Finished testing strlen\n");
}
