	format  elf
	include "../includes/debug_symbols.inc"
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  music_main
	public  music_add_builtin

	extrn shell_add_builtin
	extrn shell_next_arg

	include '../lib/includes/music.inc'
	include '../lib/includes/string.inc'
	include '../lib/includes/stdio.inc'
	include '../lib/includes/unistd.inc'
	include '../includes/kernel.inc'
	include '../includes/ascii.inc'

music_main:
	enter 0, 0
	push  esi
	push  ebx

	ccall shell_next_arg
	test  eax, eax
	jz    .usage

	mov   esi, eax
	ccall strcmp, esi, .command_play_str
	test  eax, eax
	je    .command_play
	ccall strcmp, esi, .command_scale_str
	test  eax, eax
	je    .command_scale
	ccall strcmp, esi, .command_chord_str
	test  eax, eax
	je    .command_chord

	jmp .unknown_command

.error:
	mov eax, 1
	jmp .return

.success:
	mov eax, 0

.return:
	pop ebx
	pop esi
	leave
	ret

.usage:
	ccall puts, .usage_str
	jmp   .error

.usage_str:
	db "music: usage: music [command]", 13, 10
	db 13, 10
	db "Commands:", 13, 10
	db ASCII_TAB, "play <note>", 13, 10
	db ASCII_TAB, "scale <note> [major|minor|pentatonic [major|minor]]", 13, 10
	db ASCII_TAB, "chord <note> [major]", 13, 10
	;  TODO have dedicated help command for music to get help for each command
	;  for now, just have a big usage str
	db "Note specification is the note letter followed by octave, e.g. C4 for middle C", 13, 10
	db "Accidentals supported, sharp (#) and flat (b), e.g. Bb3, C#5", 13, 10
	db 0

.unknown_command:
	ccall printf, .unknown_command_str, esi
	jmp   .usage

.unknown_command_str:
	db "music: ERROR: unknown command `%s`", 13, 10, 0

.command_play_str:
	db "play", 0

.command_scale_str:
	db "scale", 0

.command_chord_str:
	db "chord", 0

.command_play:

	ccall shell_next_arg
	test  eax, eax
	jz    .no_note_given_error

	mov esi, eax

	ccall music_parse_note, eax
	cmp   eax, 0
	jl    .invalid_note_error

	ccall music_play_note, eax, 500

	jmp .success

.command_chord:
	ccall shell_next_arg
	test  eax, eax
	jz    .no_note_given_error

	mov esi, eax

	ccall music_parse_note, eax
	cmp   eax, 0
	jl    .invalid_note_error

	mov ebx, eax

	ccall shell_next_arg
	test  eax, eax
	jz    .default_chord
	mov   esi, eax
	ccall strcmp, esi, .major_str
	test  eax, eax
	je    .major_chord

	ccall printf, .unknown_chord_str, esi
	jmp   .default_chord

.command_scale:
	ccall shell_next_arg
	test  eax, eax
	jz    .no_note_given_error

	mov esi, eax

	ccall music_parse_note, eax
	cmp   eax, 0
	jl    .invalid_note_error

	mov ebx, eax

	ccall shell_next_arg
	test  eax, eax
	jz    .default_scale
	mov   esi, eax
	ccall strcmp, esi, .major_str
	test  eax, eax
	je    .major_scale
	ccall strcmp, esi, .minor_str
	test  eax, eax
	je    .minor_scale
	ccall strcmp, esi, .pentatonic_str
	test  eax, eax
	je    .pentatonic_scale

	ccall printf, .unknown_scale_str, esi
	jmp   .default_scale

.pentatonic_scale:
	ccall shell_next_arg
	test  eax, eax
	jz    .pentatonic_default_scale
	mov   esi, eax
	ccall strcmp, esi, .major_str
	test  eax, eax
	je    .pentatonic_major_scale
	ccall strcmp, esi, .minor_str
	test  eax, eax
	je    .pentatonic_minor_scale

.default_scale:
	; just do major scale

.major_scale:
	ccall music_play_scale, ebx, MUSIC_SCALE_MAJOR
	jmp   .success

.minor_scale:
	ccall music_play_scale, ebx, MUSIC_SCALE_MINOR
	jmp   .success

.pentatonic_default_scale:
	; just do major scale

.pentatonic_major_scale:
	ccall music_play_scale, ebx, MUSIC_SCALE_PENTATONIC_MAJOR
	jmp   .success

.pentatonic_minor_scale:
	ccall music_play_scale, ebx, MUSIC_SCALE_PENTATONIC_MINOR
	jmp   .success

.default_chord:
	; just do major chord

.major_chord:
	ccall music_play_chord, ebx, MUSIC_CHORD_MAJOR, 1
	jmp   .success

.unknown_scale_str:
	db "music: unknown scale type `%s`", 13, 10, 0

.unknown_chord_str:
	db "music: unknown chord type `%s`", 13, 10, 0

.major_str:
	db "major", 0

.minor_str:
	db "minor", 0

.pentatonic_str:
	db "pentatonic", 0

.no_note_given_error:
	ccall puts, .no_note_given_error_str
	;     TODO have a specific help for this command?
	jmp   .usage

.no_note_given_error_str:
	db "music: ERROR: no note given", 13, 10, 0

.invalid_note_error:
	ccall printf, .invalid_note_error_str, esi
	;     TODO have a specific help for this command?
	jmp   .usage

.invalid_note_error_str:
	db "music: ERROR: invalid note specification `%s`", 13, 10, 0

music_add_builtin:
	enter 0, 0
	ccall shell_add_builtin, music_cmd_str, music_shortdoc_str, music_main
	leave
	ret

music_cmd_str:
	db "music", 0

music_shortdoc_str:
	db "Music application", 0
