	format  elf
	include "../includes/debug_symbols.inc"
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  term_add_builtin

	include '../includes/ascii.inc'
	include '../lib/includes/string.inc'
	include '../lib/includes/stdio.inc'
	include '../lib/includes/stdlib.inc'
	include '../lib/includes/term.inc'
	include '../lib/includes/readkey.inc'
	extrn   shell_add_builtin
	extrn   shell_next_arg

	extrn hack_term_ptr

term_add_builtin:
	enter 0, 0
	ccall shell_add_builtin, .term_cmd_str, .term_shortdoc_str, term_main
	leave
	ret

.term_cmd_str:
	db "term", 0

.term_shortdoc_str:
	db "Term application", 0

term_main:
	enter 0, 0
	push  esi
	push  ebx

	ccall shell_next_arg
	test  eax, eax
	jz    .usage

	mov   esi, eax
	ccall strcmp, esi, .command_resize_str
	test  eax, eax
	je    .command_resize
	ccall strcmp, esi, .command_move_str
	test  eax, eax
	je    .command_move

	jmp .unknown_command

.error:
	mov eax, 1
	jmp .return

.success:
	mov eax, 0

.return:
	pop ebx
	pop esi
	leave
	ret

.usage:
	ccall puts, .usage_str
	jmp   .error

.usage_str:
	db "term: usage: term [command]", 13, 10
	db 13, 10
	db "Commands:", 13, 10
	db ASCII_TAB, "resize <width> <height>", 13, 10
	db ASCII_TAB, "move <x> <y>", 13, 10
	db 0

.unknown_command:
	ccall printf, .unknown_command_str, esi
	jmp   .usage

.unknown_command_str:
	db "term: ERROR: unknown command `%s`", 13, 10, 0

.command_resize_str:
	db "resize", 0

.command_move_str:
	db "move", 0

.command_move:

	ccall shell_next_arg
	test  eax, eax
	jz    .no_x

	;     TODO check if it is an int
	ccall atoi, eax
	mov   ebx, eax

	ccall shell_next_arg
	test  eax, eax
	jz    .no_y

	;     TODO check if it is an int
	ccall atoi, eax
	mov   esi, eax

	ccall term_move, [hack_term_ptr], ebx, esi
	test  eax, eax
	jne   .invalid

	jmp .success

.command_resize:

	ccall shell_next_arg
	test  eax, eax
	jz    .no_width

	;     TODO check if it is an int
	ccall atoi, eax
	mov   ebx, eax

	ccall shell_next_arg
	test  eax, eax
	jz    .no_height

	;     TODO check if it is an int
	ccall atoi, eax
	mov   esi, eax

	ccall term_resize, [hack_term_ptr], ebx, esi
	test  eax, eax
	jne   .invalid

	jmp .success

.no_x:
.no_y:
.no_width:
.no_height:
.missing_arg:
	ccall puts, .missing_str
	jmp   .usage

.invalid:
	ccall puts, .invalid_str
	jmp   .usage

.missing_str:
	db "Missing argument", 13, 10, 0

.invalid_str:
	db "Invalid argument", 13, 10, 0
