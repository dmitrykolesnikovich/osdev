use std::collections::VecDeque;
use std::env;
use std::fs::{self, File};
use std::io::{self, BufRead};
use std::path::Path;
use std::process::{exit, Command};

fn maybe_rebuild_self() -> io::Result<()> {
    let binary = env::args().nth(0).unwrap();
    let source = file!();
    let binary_metadata = fs::metadata(binary)?;
    let source_metadata = fs::metadata(source)?;
    if let Ok(binary_mtime) = binary_metadata.modified() {
        if let Ok(source_mtime) = source_metadata.modified() {
            if binary_mtime < source_mtime {
                println!("Source code is newer than binary, rebuilding");
                let status = Command::new("rustc")
                    .arg(source)
                    .status()
                    .expect("rustc failed");
                assert!(status.success());
                Command::new(env::args().nth(0).unwrap())
                    .args(env::args().skip(1))
                    .status()
                    .expect("running binary failed");
                exit(0)
            }
        }
    }
    Ok(())
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

fn main() -> io::Result<()> {
    maybe_rebuild_self()?;
    if env::args().count() > 1 {
        for i in 1..env::args().count() {
            let mut stmts: Vec<AsmStmt> = vec![];
            let file_name: &String = &env::args().nth(i).unwrap();
            println!("Reading from file `{}`", file_name);
            if let Ok(lines) = read_lines(file_name) {
                for (line_no, line) in lines.enumerate() {
                    if let Ok(s) = line {
                        stmts.push(parse_line(s, file_name.clone(), line_no + 1));
                    }
                }
            } else {
                assert!(false, "Reading from file `{}` failed", file_name);
            }
            process_asm(&stmts);
        }
    } else {
        let mut stmts: Vec<AsmStmt> = vec![];
        println!("No file given, reading from stdin");
        let mut buffer = String::new();
        let stdin = io::stdin(); // We get `Stdin` here.
        let mut line_no = 0;
        while let Ok(n) = stdin.read_line(&mut buffer) {
            if n == 0 {
                break;
            }
            stmts.push(parse_line(buffer, "stdin".to_string(), line_no));
            line_no += 1;
            buffer = String::new();
        }
        process_asm(&stmts);
    }

    Ok(())
}

#[derive(Debug, Hash, Eq, PartialEq)]
enum Opcode {
    ADC,
    ADD,
    AND,
    BT,
    BTS,
    CALL,
    CLD,
    CLI,
    CMP,
    CMOVE,
    CMOVG,
    CMOVGE,
    CMOVZ,
    DB,
    DD,
    DEC,
    DIV,
    ENTER,
    EXTRN,
    FILE,
    FORMAT,
    HLT,
    IMUL,
    IN,
    INC,
    INT,
    INCLUDE,
    IRET,
    JAE,
    JB,
    JBE,
    JC,
    JCXZ,
    JE,
    JG,
    JGE,
    JL,
    JLE,
    JMP,
    JNE,
    JNZ,
    JO,
    JZ,
    LEAVE,
    LODSB,
    MOV,
    MOVSX,
    MUL,
    NEG,
    NOP,
    NOT,
    OR,
    OUT,
    PAUSE,
    POP,
    POPA,
    POPFD,
    PUBLIC,
    PUSH,
    PUSHA,
    PUSHD,
    PUSHFD,
    RET,
    SECTION,
    SETNZ,
    SHL,
    SHR,
    SHRD,
    STD,
    STI,
    STOSB,
    SUB,
    TEST,
    USE32,
    XCHG,
    XOR,

    // Note, these are different to normal ops
    EQU,
}

fn is_cmov_opcode(opcode: Option<&Opcode>) -> bool {
    match opcode {
        None
        | Some(Opcode::ADC)
        | Some(Opcode::ADD)
        | Some(Opcode::AND)
        | Some(Opcode::BT)
        | Some(Opcode::BTS)
        | Some(Opcode::CALL)
        | Some(Opcode::CLD)
        | Some(Opcode::CLI)
        | Some(Opcode::CMP)
        | Some(Opcode::DB)
        | Some(Opcode::DD)
        | Some(Opcode::DEC)
        | Some(Opcode::DIV)
        | Some(Opcode::ENTER)
        | Some(Opcode::EXTRN)
        | Some(Opcode::FILE)
        | Some(Opcode::FORMAT)
        | Some(Opcode::HLT)
        | Some(Opcode::IMUL)
        | Some(Opcode::IN)
        | Some(Opcode::INC)
        | Some(Opcode::INT)
        | Some(Opcode::INCLUDE)
        | Some(Opcode::IRET)
        | Some(Opcode::JAE)
        | Some(Opcode::JB)
        | Some(Opcode::JBE)
        | Some(Opcode::JC)
        | Some(Opcode::JCXZ)
        | Some(Opcode::JE)
        | Some(Opcode::JG)
        | Some(Opcode::JGE)
        | Some(Opcode::JL)
        | Some(Opcode::JLE)
        | Some(Opcode::JMP)
        | Some(Opcode::JNE)
        | Some(Opcode::JNZ)
        | Some(Opcode::JO)
        | Some(Opcode::JZ)
        | Some(Opcode::LEAVE)
        | Some(Opcode::LODSB)
        | Some(Opcode::MOV)
        | Some(Opcode::MOVSX)
        | Some(Opcode::MUL)
        | Some(Opcode::NEG)
        | Some(Opcode::NOP)
        | Some(Opcode::NOT)
        | Some(Opcode::OR)
        | Some(Opcode::OUT)
        | Some(Opcode::PAUSE)
        | Some(Opcode::POP)
        | Some(Opcode::POPA)
        | Some(Opcode::POPFD)
        | Some(Opcode::PUBLIC)
        | Some(Opcode::PUSH)
        | Some(Opcode::PUSHA)
        | Some(Opcode::PUSHD)
        | Some(Opcode::PUSHFD)
        | Some(Opcode::RET)
        | Some(Opcode::SECTION)
        | Some(Opcode::SETNZ)
        | Some(Opcode::SHL)
        | Some(Opcode::SHR)
        | Some(Opcode::SHRD)
        | Some(Opcode::STD)
        | Some(Opcode::STI)
        | Some(Opcode::STOSB)
        | Some(Opcode::SUB)
        | Some(Opcode::TEST)
        | Some(Opcode::XCHG)
        | Some(Opcode::XOR) => false,

        Some(Opcode::CMOVE) | Some(Opcode::CMOVZ) | Some(Opcode::CMOVG) | Some(Opcode::CMOVGE) => {
            true
        }
        _ => todo!("is_cmov_opcode() - unhandled opcode {:?}", opcode),
    }
}

fn is_jmp_opcode(opcode: Option<&Opcode>) -> bool {
    match opcode {
        None
        | Some(Opcode::ADC)
        | Some(Opcode::ADD)
        | Some(Opcode::AND)
        | Some(Opcode::BT)
        | Some(Opcode::BTS)
        | Some(Opcode::CALL)
        | Some(Opcode::CLD)
        | Some(Opcode::CLI)
        | Some(Opcode::CMOVE)
        | Some(Opcode::CMOVG)
        | Some(Opcode::CMOVGE)
        | Some(Opcode::CMOVZ)
        | Some(Opcode::CMP)
        | Some(Opcode::DB)
        | Some(Opcode::DD)
        | Some(Opcode::DEC)
        | Some(Opcode::DIV)
        | Some(Opcode::ENTER)
        | Some(Opcode::EXTRN)
        | Some(Opcode::FILE)
        | Some(Opcode::FORMAT)
        | Some(Opcode::HLT)
        | Some(Opcode::IMUL)
        | Some(Opcode::IN)
        | Some(Opcode::INC)
        | Some(Opcode::INT)
        | Some(Opcode::INCLUDE)
        | Some(Opcode::IRET)
        | Some(Opcode::LEAVE)
        | Some(Opcode::LODSB)
        | Some(Opcode::MOV)
        | Some(Opcode::MOVSX)
        | Some(Opcode::MUL)
        | Some(Opcode::NEG)
        | Some(Opcode::NOP)
        | Some(Opcode::NOT)
        | Some(Opcode::OR)
        | Some(Opcode::OUT)
        | Some(Opcode::PAUSE)
        | Some(Opcode::POP)
        | Some(Opcode::POPA)
        | Some(Opcode::POPFD)
        | Some(Opcode::PUBLIC)
        | Some(Opcode::PUSH)
        | Some(Opcode::PUSHA)
        | Some(Opcode::PUSHD)
        | Some(Opcode::PUSHFD)
        | Some(Opcode::RET)
        | Some(Opcode::SECTION)
        | Some(Opcode::SETNZ)
        | Some(Opcode::SHL)
        | Some(Opcode::SHR)
        | Some(Opcode::SHRD)
        | Some(Opcode::STD)
        | Some(Opcode::STI)
        | Some(Opcode::STOSB)
        | Some(Opcode::SUB)
        | Some(Opcode::TEST)
        | Some(Opcode::XCHG)
        | Some(Opcode::XOR) => false,

        Some(Opcode::JMP) | Some(Opcode::JAE) | Some(Opcode::JC) | Some(Opcode::JCXZ)
        | Some(Opcode::JB) | Some(Opcode::JBE) | Some(Opcode::JE) | Some(Opcode::JG)
        | Some(Opcode::JGE) | Some(Opcode::JL) | Some(Opcode::JLE) | Some(Opcode::JNE)
        | Some(Opcode::JNZ) | Some(Opcode::JO) | Some(Opcode::JZ) => true,
        _ => todo!("is_jmp_opcode() - unhandled opcode {:?}", opcode),
    }
}

fn opcode_from_str(opcode: &str) -> Option<Opcode> {
    match opcode {
        // FIXME: this is actually a macro, but we don't support those yet
        "ccall" => Some(Opcode::CALL),

        "adc" => Some(Opcode::ADC),
        "add" => Some(Opcode::ADD),
        "and" => Some(Opcode::AND),
        "bt" => Some(Opcode::BT),
        "bts" => Some(Opcode::BTS),
        "call" => Some(Opcode::CALL),
        "cld" => Some(Opcode::CLD),
        "cli" => Some(Opcode::CLI),
        "cmove" => Some(Opcode::CMOVE),
        "cmovg" => Some(Opcode::CMOVG),
        "cmovge" => Some(Opcode::CMOVGE),
        "cmovz" => Some(Opcode::CMOVZ),
        "cmp" => Some(Opcode::CMP),
        "db" => Some(Opcode::DB),
        "dd" => Some(Opcode::DD),
        "dec" => Some(Opcode::DEC),
        "div" => Some(Opcode::DIV),
        "enter" => Some(Opcode::ENTER),
        "extrn" => Some(Opcode::EXTRN),
        "file" => Some(Opcode::FILE),
        "format" => Some(Opcode::FORMAT),
        "hlt" => Some(Opcode::HLT),
        "imul" => Some(Opcode::IMUL),
        "in" => Some(Opcode::IN),
        "inc" => Some(Opcode::INC),
        "int" => Some(Opcode::INT),
        "include" => Some(Opcode::INCLUDE),
        "iret" => Some(Opcode::IRET),
        "jae" => Some(Opcode::JAE),
        "jb" => Some(Opcode::JB),
        "jbe" => Some(Opcode::JBE),
        "jc" => Some(Opcode::JC),
        "jcxz" => Some(Opcode::JCXZ),
        "je" => Some(Opcode::JE),
        "jg" => Some(Opcode::JG),
        "jge" => Some(Opcode::JGE),
        "jl" => Some(Opcode::JL),
        "jle" => Some(Opcode::JLE),
        "jmp" => Some(Opcode::JMP),
        "jne" => Some(Opcode::JNE),
        "jnz" => Some(Opcode::JNZ),
        "jo" => Some(Opcode::JO),
        "jz" => Some(Opcode::JZ),
        "leave" => Some(Opcode::LEAVE),
        "lodsb" => Some(Opcode::LODSB),
        "mov" => Some(Opcode::MOV),
        "movsx" => Some(Opcode::MOVSX),
        "mul" => Some(Opcode::MUL),
        "neg" => Some(Opcode::NEG),
        "nop" => Some(Opcode::NOP),
        "not" => Some(Opcode::NOT),
        "or" => Some(Opcode::OR),
        "out" => Some(Opcode::OUT),
        "pause" => Some(Opcode::PAUSE),
        "pop" => Some(Opcode::POP),
        "popa" => Some(Opcode::POPA),
        "popfd" => Some(Opcode::POPFD),
        "public" => Some(Opcode::PUBLIC),
        "push" => Some(Opcode::PUSH),
        "pusha" => Some(Opcode::PUSHA),
        "pushd" => Some(Opcode::PUSHD),
        "pushfd" => Some(Opcode::PUSHFD),
        "ret" => Some(Opcode::RET),
        "section" => Some(Opcode::SECTION),
        "setnz" => Some(Opcode::SETNZ),
        "shl" => Some(Opcode::SHL),
        "shr" => Some(Opcode::SHR),
        "shrd" => Some(Opcode::SHRD),
        "sti" => Some(Opcode::STI),
        "std" => Some(Opcode::STD),
        "stosb" => Some(Opcode::STOSB),
        "sub" => Some(Opcode::SUB),
        "test" => Some(Opcode::TEST),
        "use32" => Some(Opcode::USE32),
        "xchg" => Some(Opcode::XCHG),
        "xor" => Some(Opcode::XOR),

        _ => None,
    }
}

impl std::fmt::Display for Opcode {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match &self {
                Opcode::ADC => "adc",
                Opcode::ADD => "add",
                Opcode::AND => "and",
                Opcode::BT => "bt",
                Opcode::BTS => "bts",
                Opcode::CALL => "call",
                Opcode::CLD => "cld",
                Opcode::CLI => "cli",
                Opcode::CMOVZ => "cmovz",
                Opcode::CMOVE => "cmove",
                Opcode::CMOVG => "cmovg",
                Opcode::CMOVGE => "cmovge",
                Opcode::CMP => "cmp",
                Opcode::DB => "db",
                Opcode::DD => "dd",
                Opcode::DEC => "dec",
                Opcode::DIV => "div",
                Opcode::ENTER => "enter",
                Opcode::EXTRN => "extrn",
                Opcode::FILE => "file",
                Opcode::FORMAT => "format",
                Opcode::HLT => "hlt",
                Opcode::IMUL => "imul",
                Opcode::IN => "in",
                Opcode::INC => "inc",
                Opcode::INT => "int",
                Opcode::INCLUDE => "include",
                Opcode::IRET => "iret",
                Opcode::JAE => "jae",
                Opcode::JB => "jb",
                Opcode::JBE => "jbe",
                Opcode::JC => "jc",
                Opcode::JCXZ => "jcxz",
                Opcode::JE => "je",
                Opcode::JG => "jg",
                Opcode::JGE => "jge",
                Opcode::JL => "jl",
                Opcode::JLE => "jle",
                Opcode::JMP => "jmp",
                Opcode::JNE => "jne",
                Opcode::JNZ => "jnz",
                Opcode::JO => "jo",
                Opcode::JZ => "jz",
                Opcode::LEAVE => "leave",
                Opcode::LODSB => "lodsb",
                Opcode::MOV => "mov",
                Opcode::MOVSX => "movsx",
                Opcode::MUL => "mul",
                Opcode::NEG => "neg",
                Opcode::NOP => "nop",
                Opcode::NOT => "not",
                Opcode::OR => "or",
                Opcode::OUT => "out",
                Opcode::PAUSE => "pause",
                Opcode::POP => "pop",
                Opcode::POPA => "popa",
                Opcode::POPFD => "popfd",
                Opcode::PUBLIC => "public",
                Opcode::PUSH => "push",
                Opcode::PUSHA => "pusha",
                Opcode::PUSHD => "pushd",
                Opcode::PUSHFD => "pushfd",
                Opcode::RET => "ret",
                Opcode::SECTION => "section",
                Opcode::SETNZ => "setnz",
                Opcode::SHL => "shl",
                Opcode::SHR => "shr",
                Opcode::SHRD => "shrd",
                Opcode::STD => "std",
                Opcode::STI => "sti",
                Opcode::STOSB => "stosb",
                Opcode::SUB => "sub",
                Opcode::TEST => "test",
                Opcode::XCHG => "xchg",
                Opcode::XOR => "xor",
                _ => todo!("Opcode::fmt() - unhandled Opcode {:?}", self),
            }
        )
    }
}

#[derive(Debug, Eq, PartialEq, Hash)]
struct AsmStmt {
    file_name: String,
    line_no: usize,
    orig_line: String,
    label: Option<String>,
    opcode: Option<Opcode>,
    ops: Option<Vec<String>>,
    comment: Option<String>,
}

impl std::fmt::Display for AsmStmt {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        if let Some(label) = &self.label {
            match &self.opcode {
                Some(_) => {
                    write!(f, "{} ", label)?;
                }
                _ => {
                    write!(f, "{}:", label)?;
                }
            }
        }
        if let Some(opcode) = &self.opcode {
            if let None = &self.label {
                write!(f, "\t")?;
            }
            write!(f, "{}", opcode)?;
            if let Some(ops) = &self.ops {
                let mut first = true;
                for op in ops {
                    if !first {
                        write!(f, ",")?;
                    }
                    write!(f, " {}", op)?;
                    first = false;
                }
            }
        }
        if let Some(comment) = &self.comment {
            write!(f, "; {}", comment)?;
        }
        Ok(())
    }
}

fn parse_line(line: String, file_name: String, line_no: usize) -> AsmStmt {
    let mut ret: AsmStmt = AsmStmt {
        file_name: file_name.clone(),
        line_no: line_no,
        orig_line: line.clone(),
        label: None,
        opcode: None,
        ops: None,
        comment: None,
    };
    let mut splits: VecDeque<&str>;
    if let Some((line, comment)) = line.split_once(";") {
        ret.comment = Some(comment.to_string());
        splits = line.split_whitespace().collect();
    } else {
        splits = line.split_whitespace().collect();
    }
    if splits.is_empty() {
        // No opcode present
    } else if splits.len() == 1 {
        if splits[0].ends_with(":") {
            ret.label = Some((&splits[0][..splits[0].len() - 1]).to_string());
        } else {
            ret.opcode = opcode_from_str(splits[0]);
        }
    } else {
        // if splits[0] is not a valid opcode, then must be a label
        let possible_label = splits.pop_front().unwrap();
        if let Some(opcode) = opcode_from_str(possible_label) {
            ret.opcode = Some(opcode);
        } else {
            ret.label = Some(possible_label.to_string());
            if let Some(opcode_str) = splits.pop_front() {
                if let Some(opcode) = opcode_from_str(opcode_str) {
                    ret.opcode = Some(opcode);
                } else {
                    match opcode_str {
                        "=" | "equ" => {
                            // FIXME this is hacky, could store this somewhere
                            ret.label = None;
                            ret.opcode = Some(Opcode::EQU);
                        }
                        _ => {}
                    }
                    match possible_label {
                        "times" => {
                            // TODO actually repeat the opcode
                            ret.label = None;
                            assert!(
                                !splits.is_empty(),
                                "{}:{} No opcode after times: {}",
                                file_name,
                                line_no,
                                line
                            );
                            ret.opcode = opcode_from_str(splits.pop_front().unwrap());
                        }
                        "lock" => {
                            ret.label = None;
                            assert!(
                                !splits.is_empty(),
                                "{}:{} No opcode after lock: {}",
                                file_name,
                                line_no,
                                line
                            );
                            ret.opcode = opcode_from_str(splits.pop_front().unwrap());
                        }
                        _ => {}
                    }
                    // TODO could deal with macro's here
                    if let None = ret.opcode {
                        assert!(false, "{}:{} unknown opcode: {}", file_name, line_no, line);
                    }
                }
            }
        }
        if !splits.is_empty() {
            if ret.opcode.is_none() {
                println!("{:?}", ret);
                assert!(
                    false,
                    "{}:{} operands exist with no opcode: {}",
                    file_name, line_no, line
                );
            }
            ret.ops = Some(
                splits
                    .iter()
                    .map(|s| s.trim_end_matches(','))
                    .map(|s| s.to_string())
                    .collect::<Vec<String>>(),
            );
        }
    }
    if !line.is_empty() {
        match ret {
            AsmStmt {
                label: None,
                opcode: None,
                ops: None,
                comment: None,
                ..
            } => assert!(
                false,
                "{}:{} couldn't parse line: {}",
                file_name, line_no, line
            ),
            _ => {}
        }
    };
    return ret;
}

use std::collections::HashMap;
#[derive(Debug, Clone, PartialEq)]
enum RegisterState {
    INITIAL,
    UNDEFINED,
    DEFINED,
    STACK,
}

#[derive(Debug, Clone, PartialEq)]
struct State {
    regs: HashMap<String, RegisterState>,
    stack: Vec<(String, RegisterState)>,
    alt_stack: Option<Vec<(String, RegisterState)>>,
}

fn reg_name(reg: &str) -> &str {
    if reg.starts_with("[") && reg.ends_with("]") {
        return reg_name(reg.trim_end_matches(']').trim_start_matches('['));
    }
    match reg {
        "al" => "eax",
        "ah" => "eax",
        "ax" => "eax",
        "bl" => "ebx",
        "bh" => "ebx",
        "bx" => "ebx",
        "cl" => "ecx",
        "ch" => "ecx",
        "cx" => "ecx",
        "dl" => "edx",
        "dh" => "edx",
        "dx" => "edx",
        "si" => "esi",
        "sil" => "esi",
        "di" => "edi",
        "dil" => "edi",
        // TODO sp/spl/bp/bpl
        _ => reg,
    }
}

fn check_reg_is_defined(state: &mut State, reg: &str) -> bool {
    match state.regs.get(reg_name(reg)) {
        Some(RegisterState::DEFINED) => true,
        Some(RegisterState::STACK) => true,
        None => true,
        _ => false,
    }
}

fn check_valid_src_args(stmt: &AsmStmt, state: &mut State, include_first: bool) {
    if let Some(ops) = &stmt.ops {
        for (i, op) in ops.iter().enumerate() {
            if !include_first && i == 0 {
                // ignore the first, it is dst
                continue;
            }
            if !check_reg_is_defined(state, op) {
                println!("{:?}", state);
                println!("{:?}", stmt);
                assert!(
                    false,
                    "{}:{} check_valid_src_args() - use of undefined reg {}",
                    stmt.file_name, stmt.line_no, op
                );
            }
        }
    }
}

fn simulate<'a>(stmt: &'a AsmStmt, state: &mut State) {
    match stmt.opcode {
        None | Some(Opcode::HLT) | Some(Opcode::PAUSE) | Some(Opcode::NOP)
        | Some(Opcode::FORMAT) | Some(Opcode::PUBLIC) | Some(Opcode::EXTRN) => {}
        // TODO should parse include file and check that
        Some(Opcode::INCLUDE) => {}
        // TODO should maybe parse args to db
        Some(Opcode::DB) | Some(Opcode::DD) => {}
        // TODO these potentially could have stuff to check in operands
        Some(Opcode::JMP) | Some(Opcode::JZ) | Some(Opcode::JG) | Some(Opcode::JGE)
        | Some(Opcode::JAE) | Some(Opcode::JB) | Some(Opcode::JBE) | Some(Opcode::JC)
        | Some(Opcode::JCXZ) | Some(Opcode::JE) | Some(Opcode::JNE) | Some(Opcode::JNZ)
        | Some(Opcode::JO) | Some(Opcode::JL) | Some(Opcode::JLE) => {}
        // TODO enter and leave should probably be handled and checked used correctly
        Some(Opcode::ENTER) | Some(Opcode::LEAVE) => {}
        // TODO set flags?
        Some(Opcode::CLI) | Some(Opcode::STI) | Some(Opcode::CLD) | Some(Opcode::STD) => {}
        Some(Opcode::INT) => {
            // FIXME we don't know what regs are set to to be able to work out what int function we are running
        }
        Some(Opcode::RET) => {
            for reg in ["ebx", "esi", "edi"] {
                match state.regs.get(reg) {
                    Some(RegisterState::INITIAL) => {}
                    _ => assert!(
                        false,
                        "{}:{} simulate() - ret: reg {} altered during function",
                        stmt.file_name, stmt.line_no, reg
                    ),
                }
            }
            assert!(
                !state.stack.is_empty(),
                "{}:{} simulate() - ret does not have the initial stack",
                stmt.file_name,
                stmt.line_no
            );
            match state.stack.pop() {
                Some((_, RegisterState::INITIAL)) => {}
                _ => {
                    assert!(
                        state.stack.is_empty(),
                        "{}:{} simulate() - ret has data still on stack",
                        stmt.file_name,
                        stmt.line_no
                    );
                    assert!(
                        false,
                        "{}:{} simulate() - ret does not have the initial stack",
                        stmt.file_name, stmt.line_no
                    );
                }
            }
            assert!(
                state.stack.is_empty(),
                "{}:{} simulate() - ret has data still on stack",
                stmt.file_name,
                stmt.line_no
            );
        }
        Some(Opcode::IRET) => {
            for reg in ["eax", "ecx", "edx", "ebx", "esp", "ebp", "esi", "edi"] {
                match reg {
                    "esp" | "ebp" => continue,
                    _ => (),
                }
                match state.regs.get(reg) {
                    Some(RegisterState::INITIAL) => {}
                    _ => assert!(
                        false,
                        "{}:{} simulate() - iret: reg {} altered during function",
                        stmt.file_name, stmt.line_no, reg
                    ),
                }
            }
            assert!(
                !state.stack.is_empty(),
                "{}:{} simulate() - iret does not have the initial stack",
                stmt.file_name,
                stmt.line_no
            );
            match state.stack.pop() {
                Some((_, RegisterState::INITIAL)) => {}
                _ => {
                    assert!(
                        state.stack.is_empty(),
                        "{}:{} simulate() - iret has data still on stack",
                        stmt.file_name,
                        stmt.line_no
                    );
                    assert!(
                        false,
                        "{}:{} simulate() - iret does not have the initial stack",
                        stmt.file_name, stmt.line_no
                    );
                }
            }
            assert!(
                state.stack.is_empty(),
                "{}:{} simulate() - iret has data still on stack",
                stmt.file_name,
                stmt.line_no
            );
        }
        Some(Opcode::LODSB) => {
            if !check_reg_is_defined(state, "esi") {
                println!("{:?}", state);
                println!("{:?}", stmt);
                assert!(
                    false,
                    "{}:{} simulate() - esi is undefined for stosb",
                    stmt.file_name, stmt.line_no
                );
            }
            state
                .regs
                .insert(String::from("eax"), RegisterState::DEFINED);
        }
        Some(Opcode::STOSB) => {
            for reg in ["edi", "eax"] {
                if !check_reg_is_defined(state, reg) {
                    println!("{:?}", state);
                    println!("{:?}", stmt);
                    assert!(
                        false,
                        "{}:{} simulate() - {} is undefined for stosb",
                        stmt.file_name, stmt.line_no, reg
                    );
                }
            }
        }
        Some(Opcode::XOR) => {
            if let Some(ops) = &stmt.ops {
                if ops.len() >= 2 {
                    let reg = reg_name(&ops[0]);
                    if ops[0] == ops[1] {
                        // Setting ops[0] to 0
                        if let Some(_) = state.regs.get(reg) {
                            state.regs.insert(reg.to_string(), RegisterState::DEFINED);
                        }
                    } else {
                        check_valid_src_args(stmt, state, true);
                        if let Some(_) = state.regs.get(reg) {
                            state.regs.insert(reg.to_string(), RegisterState::DEFINED);
                        }
                    }
                } // else invalid syntax in asm line
            } // else invalid syntax in asm line
        }
        Some(Opcode::SUB) | Some(Opcode::OR) | Some(Opcode::AND) | Some(Opcode::ADD)
        | Some(Opcode::ADC) => {
            if let Some(ops) = &stmt.ops {
                if ops.len() >= 2 {
                    let reg = reg_name(&ops[0]);
                    match reg {
                        "esp" => match stmt.opcode {
                            Some(Opcode::ADD) => {
                                if let Ok(size) = if ops[1].ends_with("*WORD_SIZE") {
                                    ops[1].trim_end_matches("*WORD_SIZE").parse::<usize>()
                                } else if ops[1].eq("WORD_SIZE") {
                                    Ok(1)
                                } else {
                                    ops[1].parse::<usize>()
                                } {
                                    assert!(
                                        state.stack.len() >= size,
                                        "{}:{} stack underflow",
                                        stmt.file_name,
                                        stmt.line_no
                                    );
                                    for _ in 0..size {
                                        state.stack.pop();
                                    }
                                } else {
                                    assert!(
                                        false,
                                        "{}:{} parse error for esp modification - {}",
                                        stmt.file_name, stmt.line_no, ops[1]
                                    );
                                }
                            }
                            Some(Opcode::SUB) => {
                                if let Ok(size) = if ops[1].ends_with("*WORD_SIZE") {
                                    ops[1].trim_end_matches("*WORD_SIZE").parse::<usize>()
                                } else if ops[1].eq("WORD_SIZE") {
                                    Ok(1)
                                } else {
                                    ops[1].parse::<usize>()
                                } {
                                    assert!(
                                        state.stack.len() >= size,
                                        "{}:{} stack underflow",
                                        stmt.file_name,
                                        stmt.line_no
                                    );
                                    for _ in 0..size {
                                        state
                                            .stack
                                            .push((String::from("DATA"), RegisterState::DEFINED))
                                    }
                                } else {
                                    assert!(
                                        false,
                                        "{}:{} parse error for esp modification - {}",
                                        stmt.file_name, stmt.line_no, ops[1]
                                    );
                                }
                            }
                            _ => todo!(),
                        },
                        _ => {
                            check_valid_src_args(stmt, state, true);
                            if let Some(_) = state.regs.get(reg) {
                                state.regs.insert(reg.to_string(), RegisterState::DEFINED);
                            }
                        }
                    }
                } // else invalid syntax in asm line
            } // else invalid syntax in asm line
        }
        Some(Opcode::CMOVE) | Some(Opcode::CMOVZ) | Some(Opcode::CMOVG) | Some(Opcode::CMOVGE)
        | Some(Opcode::MOVSX) | Some(Opcode::MOV) => {
            if let Some(ops) = &stmt.ops {
                if ops.len() >= 2 {
                    let reg = reg_name(&ops[0]);
                    if reg.eq("esp") {
                        let src = reg_name(&ops[1]);
                        match state.regs.get(src) {
                            Some(RegisterState::STACK) => {
                                state.stack = state.alt_stack.as_ref().unwrap().clone();
                            }
                            Some(_) => {
                                // assume new stack
                                state.stack.clear();
                            }
                            Some(RegisterState::UNDEFINED) => {
                                assert!(
                                    false,
                                    "{}:{} FIXME: Can't move from undefined register {} to esp",
                                    stmt.file_name, stmt.line_no, src
                                );
                            }
                            None => {
                                assert!(
                                    false,
                                    "{}:{} FIXME: Can't move from non register to esp",
                                    stmt.file_name, stmt.line_no
                                );
                            }
                        }
                    }
                    check_valid_src_args(stmt, state, false);
                    if let Some(_) = state.regs.get(reg) {
                        let src = reg_name(&ops[1]);
                        if src.eq("esp") && ops[1].ne("[esp]") {
                            if let Some(_) = state.alt_stack {
                                assert!(
                                    false,
                                    "{}:{} FIXME: can't move from esp more than once...",
                                    stmt.file_name, stmt.line_no
                                );
                            }
                            state.regs.insert(reg.to_string(), RegisterState::STACK);
                            state.alt_stack = Some(state.stack.clone());
                        } else {
                            state.regs.insert(reg.to_string(), RegisterState::DEFINED);
                        }
                    }
                } // else invalid syntax in asm file
            } // else invalid syntax in asm file
        }
        Some(Opcode::IN) => {
            // FIXME detect al and deduce eax
            check_valid_src_args(stmt, state, false);
            state
                .regs
                .insert(String::from("eax"), RegisterState::DEFINED);
        }
        Some(Opcode::OUT) => {
            // FIXME detect al and deduce eax
            check_valid_src_args(stmt, state, true);
            if !check_reg_is_defined(state, "eax") {
                println!("{:?}", state);
                println!("{:?}", stmt);
                assert!(
                    false,
                    "{}:{} simulate() - eax/al is undefined for out",
                    stmt.file_name, stmt.line_no
                );
            }
        }
        Some(Opcode::SETNZ) => {
            if let Some(ops) = &stmt.ops {
                if ops.len() == 1 {
                    let reg = reg_name(&ops[0]);
                    if let Some(_) = state.regs.get(reg) {
                        state.regs.insert(reg.to_string(), RegisterState::DEFINED);
                    }
                }
            }
        }
        Some(Opcode::NOT) | Some(Opcode::NEG) | Some(Opcode::INC) | Some(Opcode::DEC) => {
            if let Some(ops) = &stmt.ops {
                if ops.len() == 1 {
                    let reg = reg_name(&ops[0]);
                    check_valid_src_args(stmt, state, true);
                    if let Some(_) = state.regs.get(reg) {
                        state.regs.insert(reg.to_string(), RegisterState::DEFINED);
                    }
                } // else invalid syntax in asm file
            } // else invalid syntax in asm file
        }
        Some(Opcode::MUL) => {
            check_valid_src_args(stmt, state, true);
            if !check_reg_is_defined(state, "eax") {
                println!("{:?}", state);
                println!("{:?}", stmt);
                assert!(
                    false,
                    "{}:{} simulate() - eax is undefined for mul",
                    stmt.file_name, stmt.line_no
                );
            }

            state
                .regs
                .insert(String::from("edx"), RegisterState::DEFINED);
            state
                .regs
                .insert(String::from("eax"), RegisterState::DEFINED);
        }
        Some(Opcode::IMUL) => {
            if let Some(ops) = &stmt.ops {
                match ops.len() {
                    1 => {
                        // same as MUL
                        check_valid_src_args(stmt, state, true);
                        if !check_reg_is_defined(state, "eax") {
                            println!("{:?}", state);
                            println!("{:?}", stmt);
                            assert!(
                                false,
                                "{}:{} simulate() - eax is undefined for mul",
                                stmt.file_name, stmt.line_no
                            );
                        }

                        state
                            .regs
                            .insert(String::from("edx"), RegisterState::DEFINED);
                        state
                            .regs
                            .insert(String::from("eax"), RegisterState::DEFINED);
                    }
                    2 => {
                        let reg = reg_name(&ops[0]);
                        check_valid_src_args(stmt, state, true);
                        if let Some(_) = state.regs.get(reg) {
                            state.regs.insert(reg.to_string(), RegisterState::DEFINED);
                        }
                    }
                    3 => {
                        let reg = reg_name(&ops[0]);
                        check_valid_src_args(stmt, state, false);
                        if let Some(_) = state.regs.get(reg) {
                            state.regs.insert(reg.to_string(), RegisterState::DEFINED);
                        }
                    }
                    _ => {
                        todo!();
                    }
                }
            }
        }
        Some(Opcode::DIV) => {
            check_valid_src_args(stmt, state, true);
            if !check_reg_is_defined(state, "eax") {
                println!("{:?}", state);
                println!("{:?}", stmt);
                assert!(
                    false,
                    "{}:{} simulate() - eax is undefined for div",
                    stmt.file_name, stmt.line_no
                );
            }

            state
                .regs
                .insert(String::from("edx"), RegisterState::DEFINED);
            state
                .regs
                .insert(String::from("eax"), RegisterState::DEFINED);
        }
        Some(Opcode::TEST) | Some(Opcode::CMP) => {
            if let Some(ops) = &stmt.ops {
                if ops.len() >= 2 {
                    check_valid_src_args(stmt, state, true);
                    // TODO set flags
                } // else invalid syntax in asm file
            } // else invalid syntax in asm file
        }
        Some(Opcode::XCHG) | Some(Opcode::BTS) | Some(Opcode::BT) | Some(Opcode::SHL)
        | Some(Opcode::SHR) | Some(Opcode::SHRD) => {
            check_valid_src_args(stmt, state, true);
        }
        Some(Opcode::CALL) => {
            check_valid_src_args(stmt, state, false);
            state
                .regs
                .insert(String::from("ecx"), RegisterState::UNDEFINED);
            state
                .regs
                .insert(String::from("edx"), RegisterState::UNDEFINED);
            // TODO check whether eax is returned in the called function
            state
                .regs
                .insert(String::from("eax"), RegisterState::DEFINED);
        }
        Some(Opcode::PUSHA) => {
            for reg in ["eax", "ecx", "edx", "ebx", "esp", "ebp", "esi", "edi"] {
                match reg {
                    "esp" | "ebp" => continue,
                    _ => (),
                }
                if let Some(regstate) = state.regs.get(reg) {
                    state.stack.push((reg.to_string(), regstate.clone()));
                }
            }
        }
        Some(Opcode::POPA) => {
            for reg in ["eax", "ecx", "edx", "ebx", "esp", "ebp", "esi", "edi"]
                .iter()
                .rev()
            {
                match *reg {
                    "esp" | "ebp" => continue,
                    _ => (),
                }
                if let Some((oldreg, regstate)) = state.stack.pop() {
                    if oldreg.ne(reg) {
                        assert!(
                            false,
                            "{}:{} simulate() - popped reg {} does not match stack with {}",
                            stmt.file_name, stmt.line_no, reg, oldreg
                        );
                    }
                    state.regs.insert(reg.to_string(), regstate.clone());
                } else {
                    assert!(
                        false,
                        "{}:{} simulate() - popa on empty stack",
                        stmt.file_name, stmt.line_no
                    );
                }
            }
        }
        Some(Opcode::PUSHFD) => {
            state
                .stack
                .push((String::from("FLAGS"), RegisterState::DEFINED));
        }
        Some(Opcode::POPFD) => {
            if let Some((oldreg, _)) = state.stack.pop() {
                if oldreg.ne("FLAGS") {
                    assert!(
                        false,
                        "{}:{} simulate() - popfd does not match pushfd, got {}",
                        stmt.file_name, stmt.line_no, oldreg
                    );
                }
            } else {
                assert!(
                    false,
                    "{}:{} simulate() - popfd on empty stack",
                    stmt.file_name, stmt.line_no
                );
            }
        }
        Some(Opcode::PUSH) | Some(Opcode::PUSHD) => {
            if let Some(ops) = &stmt.ops {
                if ops.len() >= 1 {
                    let reg = reg_name(&ops[0]);
                    if let Some(regstate) = state.regs.get(reg) {
                        state.stack.push((reg.to_string(), regstate.clone()));
                    } else {
                        state
                            .stack
                            .push((String::from("DATA"), RegisterState::DEFINED))
                    }
                } // else invalid syntax in asm file
            } // else invalid syntax in asm file
        }
        Some(Opcode::POP) => {
            if let Some(ops) = &stmt.ops {
                if ops.len() >= 1 {
                    let reg = reg_name(&ops[0]);
                    if let Some((oldreg, regstate)) = state.stack.pop() {
                        if let Some(_) = state.regs.get(reg) {
                            if oldreg.ne(reg) {
                                assert!(
                                    false,
                                    "{}:{} simulate() - popped reg {} does not match stack with {}",
                                    stmt.file_name, stmt.line_no, reg, oldreg
                                );
                            }
                            state.regs.insert(reg.to_string(), regstate.clone());
                        } else {
                            assert!(
                                oldreg.eq("DATA"),
                                "{}:{} simulate() - popping data, but got reg {}",
                                stmt.file_name,
                                stmt.line_no,
                                oldreg
                            );
                        }
                    } else {
                        assert!(
                            false,
                            "{}:{} simulate() - pop on empty stack",
                            stmt.file_name, stmt.line_no
                        );
                    }
                } // else invalid syntax in asm file
            } // else invalid syntax in asm file
        }
        _ => {
            todo!(
                "{}:{} simulate() - unhandled opcode {:?}",
                stmt.file_name,
                stmt.line_no,
                stmt.opcode
            )
        }
    }
}

fn process_func(
    stmts: &Vec<AsmStmt>,
    entry: usize,
    func_name: String,
    labels: &HashMap<String, usize>,
) {
    let mut initial_state = State {
        regs: HashMap::new(),
        stack: vec![(String::from("Initial stack"), RegisterState::INITIAL)],
        alt_stack: None,
    };
    initial_state
        .regs
        .insert(String::from("eax"), RegisterState::INITIAL);
    initial_state
        .regs
        .insert(String::from("ebx"), RegisterState::INITIAL);
    initial_state
        .regs
        .insert(String::from("ecx"), RegisterState::INITIAL);
    initial_state
        .regs
        .insert(String::from("edx"), RegisterState::INITIAL);
    initial_state
        .regs
        .insert(String::from("esi"), RegisterState::INITIAL);
    initial_state
        .regs
        .insert(String::from("edi"), RegisterState::INITIAL);

    let mut visited: HashMap<&AsmStmt, Vec<State>> = HashMap::new();
    let mut branches: Vec<(usize, State)> = vec![(entry, initial_state)];

    println!("process_func() - {}", func_name);

    while let Some((ip, mut state)) = branches.pop() {
        if let Some(stmt) = stmts.get(ip) {
            /*
            if let Some(label) = &stmt.label {
                //println!("{}:", label);
            }
            if let Some(_) = &stmt.opcode {
                println!("[{}] {}", ip, stmt);
                println!("stack = {:?}", state.stack);
                println!("regs = {:?}", state.regs);
            }
            */

            if let Some(visited_states) = visited.get(stmt) {
                if let Some(_) = state.alt_stack {
                    // This stops an endless loop in new_task where there is a loop pushing onto alt_stack
                    if let (s, RegisterState::INITIAL) = &state.stack[0] {
                        if s.ne("Initial stack") {
                            println!("Ignoring visited node when alt_stack is being used");
                            println!("[{}] {}", ip, stmt);
                            println!("stack = {:?}", state.stack);
                            println!("alt_stack = {:?}", state.alt_stack);
                            continue;
                        }
                    } else {
                        println!("Ignoring visited node when alt_stack is being used");
                        println!("[{}] {}", ip, stmt);
                        println!("stack = {:?}", state.stack);
                        println!("alt_stack = {:?}", state.alt_stack);
                        continue;
                    }
                }
                if visited_states.contains(&state) {
                    //println!("Seen this state at this point before, skipping");
                    continue;
                }
            } else {
                visited.insert(stmt, vec![]);
            }
            visited.get_mut(stmt).unwrap().push(state.clone());
            if is_cmov_opcode(stmt.opcode.as_ref()) {
                // simulate assumes cmov will do the move
                // this should add a branch to ip+1 with the current state (ie, it doesn't move)
                branches.push((ip + 1, state.clone()));
            }
            simulate(stmt, &mut state);
            if is_jmp_opcode(stmt.opcode.as_ref()) {
                if let Some(ops) = &stmt.ops {
                    let label = &ops[ops.len() - 1];
                    assert!(
                        label.starts_with(".") || label.starts_with("_"),
                        "{}:{} branch to global label {} (should use call?)",
                        stmt.file_name,
                        stmt.line_no,
                        label
                    );
                    let mut full_label = func_name.clone();
                    full_label.push_str(label);
                    if let Some(j_ip) = labels.get(&full_label) {
                        // println!("jmp to {} -> {}", full_label, j_ip);
                        branches.push((*j_ip, state.clone()))
                    }
                }
            }
            // TODO call to private .functions
            match stmt.opcode {
                Some(Opcode::HLT) | Some(Opcode::RET) | Some(Opcode::IRET) | Some(Opcode::JMP)
                | Some(Opcode::DB) | Some(Opcode::DD) => continue,
                _ => branches.push((ip + 1, state)),
            }
        }
    }
}

fn process_asm(stmts: &Vec<AsmStmt>) {
    let mut labels: HashMap<String, usize> = HashMap::new();

    let mut cur_global: String = String::from("");
    for ip in 0..stmts.len() {
        if let Some(label) = &stmts[ip].label {
            if label.starts_with(".") {
                let mut full_label = cur_global.clone();
                full_label.push_str(label);
                labels.insert(full_label, ip);
            } else {
                cur_global = label.to_string();
                labels.insert(label.to_string(), ip);
            }
        }
    }

    for (label, ip) in &labels {
        if !label.contains(".") {
            if let None = stmts[*ip].opcode {
                if ["switch_to_task"].contains(&label.as_str()) {
                    // These are in scheduler, they change the stack about
                    println!("Ignoring hardcoded function {}", label);
                    continue;
                }
                process_func(&stmts, *ip, label.to_string(), &labels);
            }
        }
    }

    println!("Must be all good...");
}
