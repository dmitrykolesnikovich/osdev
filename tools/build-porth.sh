#!/bin/sh

# shellcheck source=tools/common.sh
. "$(dirname "$0")"/common.sh

git clone https://gitlab.com/tsoding/porth "${HOME}/src/porth"
cd "${HOME}/src/porth"
fasm -m 524288 ./bootstrap/porth-linux-x86_64.fasm
chmod +x ./bootstrap/porth-linux-x86_64
./bootstrap/porth-linux-x86_64 com ./porth.porth
