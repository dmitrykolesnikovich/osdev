#!/bin/bash

# shellcheck source=tools/common.sh
. "$(dirname "$0")"/common.sh

if [ -z "${CI+x}" ]; then
    "${TOOLS_DIR}"/build-release.sh
fi

[ -f VERSION ] || {
    echo "No VERSION file. Release not happening"
    exit
}

if [ -n "${CI+x}" ]; then
    git config --global user.email "hugh+osdev-ci@davenport.net.nz"
    git config --global user.name "Gitlab CI Release"
    git remote add api-origin https://oauth2:"${OAUTH2_TOKEN}"@gitlab.com/"${CI_PROJECT_PATH}"
fi

CURL_OPTS=(-q --silent --header 'Content-Type: application/json' --header 'PRIVATE-TOKEN: '"${OAUTH2_TOKEN}"'')
RELEASE_API_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases"
PACKAGE_API_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic"

IFS=""
VERSION="$(cat VERSION)"
if [ -n "${CI+x}" ]; then
    git fetch --tags api-origin
else
    git fetch --tags origin
fi
if [ "$(git tag -l "$VERSION")" ]; then
    echo "Version $VERSION already exists"
else
    [ -f "${BASE_DIR}/release-notes/${VERSION}.md" ] || {
        echo "No release notes present in release-notes/${VERSION}.md. Release not happening"
        exit 1
    }
    git tag -a "$VERSION" -m "Version $VERSION"
    if [ -n "${CI+x}" ]; then
        git push api-origin "$VERSION"
    else
        git push origin "$VERSION"
    fi
    # If we exit due to an error with publishing, then rollback the tag
    trap 'echo Exited with error, cleaning up git; git tag -d "${VERSION}"; git push -d origin "${VERSION}"' EXIT
    DESCRIPTION="$(cat <(sed -e 's/latest release/release/' -e "s@permalink/latest@${VERSION}@" <"${BASE_DIR}"/quick-start.md) <(grep -A3 '^## Demo' "${BASE_DIR}"/README.md | sed -e 's/"/\\"/g' | sed -e "s@demo.gif@release-notes/${VERSION}.gif@") "${BASE_DIR}"/release-notes/"${VERSION}".md | sed ':a;N;$!ba;s/\n/\\n/g;s/\\_/_/g')"
    OVA_URL="${PACKAGE_API_URL}/${OS_NAME}/${VERSION}/${OS_NAME}-${VERSION}.ova"
    IMG_URL="${PACKAGE_API_URL}/${OS_NAME}/${VERSION}/${OS_NAME}-${VERSION}.img"
    MESSAGE='{"name": "Version '"${VERSION}"'", "tag_name": "'"${VERSION}"'", "description": "'"${DESCRIPTION}"'", "assets": { "links": [{ "name" : "Open Virtualization Format", "url": "'"${OVA_URL}"'", "filepath": "/'"${OS_NAME}"'.ova", "link_type": "other" }, { "name" : "Raw disk image", "url": "'"${IMG_URL}"'", "filepath": "/'"${OS_NAME}"'.img", "link_type": "other" }]}}'
    curl "${CURL_OPTS[@]}" --upload-file "${BUILD_DIR}/${OS_NAME}.ova" "${PACKAGE_API_URL}/${OS_NAME}/${VERSION}/${OS_NAME}-${VERSION}.ova" -i
    curl "${CURL_OPTS[@]}" --upload-file "${BUILD_DIR}/${OS_NAME}.img" "${PACKAGE_API_URL}/${OS_NAME}/${VERSION}/${OS_NAME}-${VERSION}.img" -i
    curl "${CURL_OPTS[@]}" "${RELEASE_API_URL}" --data "$MESSAGE"
    curl "${CURL_OPTS[@]}" "${RELEASE_API_URL}" | grep "${VERSION}"
    # FIXME: This was failing due to having a captcha
    # TODO, also test .img
    #   RELEASE_DOWNLOAD_URL="https://gitlab.com/hughdavenport-osdev/osdev/-/releases/${VERSION}/downloads/${OS_NAME}.ova"
    #   curl "${CURL_OPTS[@]}" "${RELEASE_DOWNLOAD_URL}" -L | diff "${BUILD_DIR}/${OS_NAME}.ova" -

    # If we got this far, we want to clear the exit trap defined above which reset the git tag
    trap EXIT
fi
