#!/bin/sh

# shellcheck source=tools/common.sh
. "$(dirname "$0")"/common.sh

set +x
OFFSET_X=2
OFFSET_Y=22
OFFSET_X_RIGHT=2
OFFSET_Y_BOTTOM=26
if [ $# -ne 0 ] && [ "$1" = "serenity" ]; then
    OFFSET_X=0
    OFFSET_Y=0
    OFFSET_X_RIGHT=0
    OFFSET_Y_BOTTOM=0
fi
WIDTH=$((1920 - OFFSET_X - OFFSET_X_RIGHT))
HEIGHT=$((1080 - OFFSET_Y - OFFSET_Y_BOTTOM))
FRAMERATE=30
#PULSE_INPUT_ID=$(pactl list short sources | grep "$(pactl info | grep 'Default Source' | cut -d ':' -f 2 | tr -d ' ')" | cut -f 1)

# TODO better arg parsing
#MONITOR_MODE=1
#PULSE_MONITOR_ID=$(pactl list short sources | grep "$(pactl info | grep 'Default Sink' | cut -d ':' -f 2 | tr -d ' ')" | cut -f 1)

if [ $# -ne 0 ]; then if [ "$1" = "webcam" ] || [ "$1" = "serenity" ]; then
    USE_WEBCAM=1
    WEBCAM_WIDTH=320
    WEBCAM_HEIGHT=180
    WEBCAM_WIDTH=480
    WEBCAM_HEIGHT=270
fi; fi

setup_i3() {
    echo "Setting up workspace"
    i3-msg mark current >/dev/null
    for i in $(seq 1 10); do
        i3-msg workspace "$i" >/dev/null
        i3-msg move workspace to output primary >/dev/null
    done
    i3-msg '[con_mark=current]' focus >/dev/null
    i3-msg unmark current >/dev/null
    i3-msg workspace 8 >/dev/null
    i3-msg move workspace to output right >/dev/null
    i3-msg workspace 8 >/dev/null
    #    if [ -n "${USE_WEBCAM+x}" ]; then
    #        i3-msg exec i3-sensible-terminal >/dev/null
    #        sleep 1
    #        i3-msg floating enable >/dev/null
    #        i3-msg resize set ${WEBCAM_WIDTH} ${WEBCAM_HEIGHT} >/dev/null
    #        i3-msg move absolute position $((1920 - OFFSET_X_RIGHT - WEBCAM_WIDTH)) $((1080 - OFFSET_Y_BOTTOM - WEBCAM_HEIGHT)) >/dev/null
    #        i3-msg focus tiling >/dev/null
    #    fi
    # TODO some way to reverse this, for now, do it manually
}

[ -f video.mkv ] && {
    echo "video.mkv already exists. Remove it and rerun this command"
    exit 1
}
echo "You are about to record a video"
if [ -n "${USE_WEBCAM+x}" ]; then
    echo "You will be filming via the webcam also"
fi

if [ $# -eq 0 ] || [ "$1" != "serenity" ]; then
    setup_i3
fi

echo "Starting to in 5 seconds"
sleep 5

FFMPEG_ARGS=""

FFMPEG_ARGS="${FFMPEG_ARGS} -video_size ${WIDTH}x${HEIGHT} -framerate ${FRAMERATE} -f x11grab -thread_queue_size 512 -i :0.0+${OFFSET_X},${OFFSET_Y}"
if [ -n "${USE_WEBCAM+x}" ]; then
    FFMPEG_ARGS="${FFMPEG_ARGS} -f v4l2 -video_size ${WEBCAM_WIDTH}x${WEBCAM_HEIGHT} -framerate 30 -thread_queue_size 512 -i /dev/video0"
    if [ $# -ne 0 ] && [ "$1" = "serenity" ]; then
        FFMPEG_ARGS="${FFMPEG_ARGS} -filter_complex overlay=main_w-overlay_w-2:main_h-overlay_h-28:format=yuv444"
    else
        FFMPEG_ARGS="${FFMPEG_ARGS} -filter_complex overlay=main_w-overlay_w:main_h-overlay_h:format=yuv444"
    fi
fi

# microphone
# did pulse loopback module source=mic sink=laptop speaker (on headphones-unplugged)
# then have a combine-sink module
# then set scummvm/etc default output to be the combined one
# then record *only* off the monitor of laptop speaker (which should have both mic, and game audio)
# headphones should only have game audio
# pacmd load-module module-combine-sink
# pactl load-module module-loopback source=29 sink=22
FFMPEG_ARGS="${FFMPEG_ARGS} -f pulse -ac 2 -thread_queue_size 512 -i 1"
#FFMPEG_ARGS="${FFMPEG_ARGS} -f pulse -ac 2 -thread_queue_size 512 -i default"
#if [ -n "${MONITOR_MODE+x}" ]; then
#    # this is needed to listen to sounds played on computer
#    FFMPEG_ARGS="${FFMPEG_ARGS} -f pulse -thread_queue_size 512 -i ${PULSE_MONITOR_ID}"
#    # need amerge to merge in mic and monitor
#    # but having it makes it out of sync...
#    FFMPEG_ARGS="${FFMPEG_ARGS} -filter_complex amerge"
#fi

FFMPEG_ARGS="${FFMPEG_ARGS} -c:v libx264rgb -crf 0 -preset ultrafast"

# TODO make a tmux running on the screen at right place
# TODO ^^ should also change hostname to hide that
killall -SIGUSR1 dunst
trap 'killall -SIGUSR2 dunst' INT
trap 'killall -SIGUSR2 dunst' EXIT
# shellcheck disable=SC2086
ffmpeg ${FFMPEG_ARGS} video.mkv
killall -SIGUSR2 dunst
